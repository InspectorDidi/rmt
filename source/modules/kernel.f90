! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief High level controller for executing the arnoldi propagation and setting
!> up/sharing data required for it.

MODULE kernel

    USE rmt_assert, ONLY: assert
    USE precisn, ONLY: wp

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE, SAVE, PRIVATE :: psi_inner_on_grid(:, :, :)

CONTAINS

    SUBROUTINE arnoldi_loop(number_channels, &
                            deltar, &
                            Z_minus_N, &
                            current_field_strength, &
                            timeindex)

        USE initial_conditions,        ONLY: timings_desired
        USE calculation_parameters,    ONLY: nfdm, &
                                             half_fd_order
        USE communications_parameters, ONLY: inner_group_id
        USE initial_conditions,        ONLY: numsols => no_of_field_confs
        USE krylov_method,             ONLY: arnoldi_propagate_inner, &
                                             update_psi_inner_on_grid
        USE local_ham_matrix,          ONLY: r_value_at_r1st
        USE mpi_communications,        ONLY: first_pes_share_cmplx2_from, &
                                             i_am_inner_master, &
                                             i_am_outer_master, &
                                             i_am_in_outer_region, &
                                             i_am_in_inner_region, &
                                             i_am_outer_rank1
        USE propagators,               ONLY: arnoldi_propagate_outer
        USE wall_clock,                ONLY: update_time_start_inner, &
                                             update_outer_time1, &
                                             update_time_end_inner, &
                                             update_outer_time7, &
                                             write_timings_files, &
                                             hel_time
        USE wavefunction,              ONLY: expec_outer, &
                                             expec_inner, &
                                             hr

        INTEGER, INTENT(IN)  :: number_channels
        INTEGER, INTENT(IN)  :: timeindex
        REAL(wp), INTENT(IN) :: deltar
        REAL(wp), INTENT(IN) :: Z_minus_N
        REAL(wp), INTENT(IN) :: current_field_strength(3, numsols)

        REAL(wp) :: time_start, time_end

        time_start = hel_time()

        IF ((i_am_inner_master) .AND. timings_desired) THEN
            CALL update_time_start_inner(-1)
        END IF

        IF (i_am_in_outer_region .AND. timings_desired) THEN
            CALL update_outer_time1(-1)
        END IF

        ! Send psi_inner_on_grid from 1st PE in inner region to 1st PE in outer region
        CALL first_pes_share_cmplx2_from(inner_group_id, &
                                         psi_inner_on_grid, &
                                         nfdm, &
                                         number_channels*numsols, &
                                         i_am_inner_master, &
                                         i_am_outer_master)

        IF ((i_am_inner_master) .AND. timings_desired) THEN
            CALL update_time_end_inner
        END IF

        IF (i_am_in_outer_region .AND. timings_desired) THEN
            CALL update_outer_time7
        END IF

        IF (i_am_in_outer_region) THEN
            ! Call the Arnoldi propagation routine in the outer region
            CALL arnoldi_propagate_outer(nfdm, half_fd_order, &
                                         deltar, &
                                         Z_minus_N, &
                                         current_field_strength, &
                                         psi_inner_on_grid, &
                                         r_value_at_r1st, &
                                         i_am_outer_master, &
                                         hr, &
                                         expec_outer)
        END IF

        IF (i_am_in_inner_region) THEN
            ! Call the Arnoldi propagation routine in the inner region
            CALL arnoldi_propagate_inner(nfdm, &
                                         current_field_strength, &
                                         timeindex, &
                                         i_am_inner_master, &
                                         expec_inner)

            ! Update psi_inner_on_grid using new psi_inner
            CALL update_psi_inner_on_grid(psi_inner_on_grid, nfdm, number_channels, numsols)
        END IF

        ! Write timing data:
        IF (timings_desired) THEN
            time_end = hel_time()
            CALL write_timings_files(time_end - time_start, &
                                     i_am_inner_master, &
                                     i_am_outer_master, &
                                     i_am_outer_rank1)
        END IF

    END SUBROUTINE arnoldi_loop

!-----------------------------------------------------------------------

    SUBROUTINE initialise_psi_inner_on_grid(number_channels)

        USE calculation_parameters, ONLY: nfdm
        USE initial_conditions,     ONLY: numsols => no_of_field_confs
        USE krylov_method,          ONLY: update_psi_inner_on_grid
        USE mpi_communications,     ONLY: i_am_in_inner_region

        INTEGER, INTENT(IN) :: number_channels
        INTEGER             :: err

        ALLOCATE (psi_inner_on_grid(nfdm, number_channels, numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with psi_inner_on_grid')

        psi_inner_on_grid = (0.0_wp, 0.0_wp)

        IF (i_am_in_inner_region) THEN
            CALL update_psi_inner_on_grid(psi_inner_on_grid, nfdm, number_channels, numsols)
        END IF

    END SUBROUTINE initialise_psi_inner_on_grid

!-----------------------------------------------------------------------

    SUBROUTINE deallocate_psi_inner_on_grid

        INTEGER :: err

        DEALLOCATE (psi_inner_on_grid, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with psi_inner_on_grid')

    END SUBROUTINE deallocate_psi_inner_on_grid

END MODULE kernel
