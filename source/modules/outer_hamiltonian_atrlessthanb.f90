! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles outer region Hamiltonian-wavefunction multiplication operations
!> specifically on the inner-region grid.
MODULE outer_hamiltonian_atrlessthanb

    USE precisn, ONLY: wp
    USE grid_parameters, ONLY: channel_id_1st, &
                               channel_id_last
    USE rmt_assert, ONLY: assert

    IMPLICIT NONE

CONTAINS

    !> \brief Multiply the hamiltonian by the wavefunction (inner region grid points)
    !>
    !> The multiplication to be performed is
    !> \f[ H\psi = \left( -\frac{1}{2} \frac{d^2}{dr^2} + \frac{l(l+1)}{2r^2} -
    !> \frac{(Z-N)}{r} \right) \psi  + \sum _{\gamma} (W_E +
    !> W_P + W_D) \psi_{\gamma} \f]
    !> where the sum is performed over all the channels \f$ \gamma \f$.
    !> Each term is computed in turn as required for a given calculation.
    !>
    !>  The value of `field_strength` is used to determine if the field
    !> coupling potentials need to be applied (.false. if field_strength = 0).
    !>
    SUBROUTINE ham_x_vec_outer_b(delR, Z_minus_N, field_strength, psi_outer, &
                                 h_psi_outer, h_psi_b_for_outer, &
                                 number_grid_points, &
                                 r_value_at_first_grid_point, WB_Store)

        USE initial_conditions,     ONLY: numsols => no_of_field_confs
        USE outer_hamiltonian,      ONLY: apply_long_range_potential_matrices, &
                                          apply_centrifugal_and_energy_shift
        USE lrpots,                 ONLY: liuu, lpuu, &
                                          ltuu, lmuu, &
                                          ksq, we_size
        !ARG!
        INTEGER, INTENT(IN)          :: number_grid_points
        REAL(wp), INTENT(IN)         :: delR, Z_minus_N, r_value_at_first_grid_point
        REAL(wp), INTENT(IN)         :: field_strength(3, numsols)
        COMPLEX(wp), INTENT(IN)      :: psi_outer(1:number_grid_points, channel_id_1st:channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(OUT)     :: h_psi_outer(1:number_grid_points, channel_id_1st:channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(IN)      :: h_psi_b_for_outer(1:4, channel_id_1st:channel_id_last, 1:numsols)
        REAL(wp), INTENT(IN)         :: WB_store(number_grid_points, we_size, channel_id_1st:channel_id_last)
        INTEGER                      :: channel_id, little_L, i, parity, total_L, total_ML, isol
        LOGICAL                      :: field_on
        REAL(wp)                     :: r_store(number_grid_points)
        REAL(wp)                     :: z_minus_n_over_r(number_grid_points)

        DO i = 1, number_grid_points
            r_store(i) = r_value_at_first_grid_point + real(i - 1, wp)*delR
            z_minus_n_over_r(i) = Z_minus_N/r_store(i)
        END DO

        ! Check if the field is on or not
        IF (ALL(field_strength == 0.0_wp)) THEN
            field_on = .false.
        ELSE
            field_on = .true.
        END IF

        DO isol = 1, numsols
            !$OMP PARALLEL DO PRIVATE(channel_id, little_L, parity, total_L,total_ML)
            DO channel_id = channel_id_1st, channel_id_last

                little_L = liuu(channel_id)
                parity = lpuu(channel_id)
                total_L = ltuu(channel_id)
                total_ML = lmuu(channel_id)  ! DDAC ADDED THIS

                ! Initialize h_psi_outer = - 1/2 grad^2 psi
                CALL init_w_AtomicHam_x_Vec_outer_b &
                    (psi_outer(:, channel_id, isol), h_psi_outer(:, channel_id, isol), &
                    little_L, h_psi_b_for_outer(:, channel_id, isol), delR, &
                    number_grid_points, r_value_at_first_grid_point)

                CALL apply_centrifugal_and_energy_shift &
                    (number_grid_points, &
                    psi_outer(:, channel_id, isol), &
                    h_psi_outer(:, channel_id, isol), &
                    z_minus_n_over_r, &
                    ksq(channel_id))

                CALL apply_long_range_potential_matrices &
                    (psi_outer(:, :, isol), &
                    h_psi_outer(:,channel_id, isol), &
                    total_ML, &
                    WB_store, &
                    channel_id, &
                    number_grid_points, &
                    field_strength(:, isol), &
                    r_store,&
                    field_on)

            END DO
        END DO

    END SUBROUTINE ham_x_vec_outer_b

!-----------------------------------------------------------------------

    SUBROUTINE init_w_atomicham_x_vec_outer_b(psi, H_psi, L, H_psi_B, delR, &
                                              number_grid_points, r_value_at_first_grid_point)

        USE local_ham_matrix, ONLY: INCR_with_laplacian_b
        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points)
        COMPLEX(wp), INTENT(OUT)   :: H_psi(1:number_grid_points)
        INTEGER, INTENT(IN)        :: L
        COMPLEX(wp), INTENT(IN)    :: H_psi_B(1:4)
        REAL(wp), INTENT(IN)       :: delR, r_value_at_first_grid_point

        INTEGER                    ::  i

        REAL(wp) :: second_deriv_coeff
        REAL(wp) :: kinetic_E_coeff, atomic_ham_factor
        REAL(wp) :: one_over_r_squared(1:number_grid_points)
        REAL(wp) :: r_value

        kinetic_E_coeff = 1.0_wp/(delR*delR)    !! due to finite difference rule for d2/dr2
        atomic_ham_factor = -0.5_wp                     !! since Hamiltonian has -1/2 grad^2

        second_deriv_coeff = kinetic_E_coeff*atomic_ham_factor

        H_psi = (0.0_wp, 0.0_wp)

        DO i = 1, number_grid_points
            r_value = r_value_at_first_grid_point + REAL(i - 1, wp)*delR
            one_over_r_squared(i) = 1.0_wp/(r_value*r_value)
        END DO

        CALL INCR_with_Laplacian_b(psi, H_psi, L, second_deriv_coeff, &
                                   atomic_ham_factor, one_over_r_squared, H_psi_B, &
                                   number_grid_points, r_value_at_first_grid_point)

    END SUBROUTINE init_w_atomicham_x_vec_outer_b

END MODULE outer_hamiltonian_atrlessthanb
