! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles the distribution of the wavefunction data within 
!!each symmetry block.

MODULE distribute_wv_data

    USE precisn
    USE rmt_assert,        ONLY: assert
    USE readhd,            ONLY: max_L_block_size, &
                                 nchmx, &
                                 no_of_LML_blocks
    USE mpi_layer_lblocks, ONLY: Lb_m_comm, &
                                 Lb_m_size, &
                                 Lb_comm
    USE MPI

    IMPLICIT NONE

    INTEGER, SAVE                     :: my_nchan, my_post
    INTEGER, ALLOCATABLE, SAVE        :: wvo_counts(:), wvo_disp(:)
    REAL(wp), ALLOCATABLE, SAVE       :: ib_surfs(:, :, :)

    PRIVATE send_numchannels_to_Lb_masters
    PRIVATE send_locposts_to_Lb_masters
    PRIVATE recv_numchannels_from_master
    PRIVATE recv_locposts_from_master
    PRIVATE alloc_ib_surfamps
    PRIVATE get_displacements_for_wvouter
    PRIVATE bcast_numchannels_in_Lblock
    PRIVATE scatter_ib_surfamps
    PUBLIC dealloc_distribute_wv_data
    PUBLIC setup_and_distribute_wv_data
    PUBLIC ib_surfs, my_nchan, wvo_counts, wvo_disp, my_post

CONTAINS

    SUBROUTINE setup_and_distribute_wv_data(i_am_inner_master, &
                                            i_am_block_master, &
                                            nfdm, &
                                            prog_post)

        LOGICAL, INTENT(IN)  :: i_am_inner_master
        LOGICAL, INTENT(IN)  :: i_am_block_master
        INTEGER, INTENT(IN)  :: nfdm
        INTEGER, INTENT(OUT) :: prog_post

        IF (i_am_inner_master) THEN
            CALL send_numchannels_to_Lb_masters
        ELSE
            IF (i_am_block_master) THEN
                CALL recv_numchannels_from_master
            END IF
        END IF

        IF (i_am_inner_master) THEN
            CALL send_locposts_to_Lb_masters
        ELSE
            IF (i_am_block_master) THEN
                CALL recv_locposts_from_master
            END IF
        END IF

        ! Broadcast number of channels within each L block (to cores that handle
        !    propagation order 0)
        ! Not needed because projection is carried out on block master cores only
        ! If it is to be carried out on all cores then need this call and need ib_surfs to
        !    have dimension num_rows (i.e. would need local_ib_surfs to be set up)
        ! ACB: for projection to happen in parallel, all inner cores need NumChannels

        CALL bcast_numchannels_in_Lblock

        IF (i_am_block_master) THEN
            ! Keep track of cumulative values of nfdm*(no. channels within each symmetry L block)
            CALL get_displacements_for_wvouter(nfdm)
        END IF

        ! Allocation of inside boundary surface amplitudes
        ! ACB: moved outside 'if': all inner region cores need ib_surfs
        CALL alloc_ib_surfamps(nfdm)

        ! Master inner region processor sends relevant portion of the inside boundary
        ! surface amplitudes for each symmetry to all inner region ranks

        CALL scatter_ib_surfamps(nfdm)

        prog_post = my_post

    END SUBROUTINE setup_and_distribute_wv_data

!-----------------------------------------------------------------------

    SUBROUTINE send_numchannels_to_Lb_masters

        USE readhd, ONLY: LML_block_nchan

        IMPLICIT NONE

        INTEGER  :: i, temch, ierr

        my_nchan = LML_block_nchan(1)
        DO i = 2, no_of_LML_blocks
            temch = LML_block_nchan(i)
            CALL MPI_SSEND(temch, 1, MPI_INTEGER, i - 1, 0, Lb_m_comm, ierr)
        END DO

    END SUBROUTINE send_numchannels_to_Lb_masters

!-----------------------------------------------------------------------

    SUBROUTINE send_locposts_to_Lb_masters

        USE readhd, ONLY: LML_block_post

        IMPLICIT NONE

        INTEGER  :: i, tempos, ierr

        my_post = LML_block_post(1) - LML_block_post(0)
        DO i = 2, no_of_LML_blocks
            tempos = LML_block_post(i) - LML_block_post(i - 1)
            CALL MPI_SSEND(tempos, 1, MPI_INTEGER, i - 1, 0, Lb_m_comm, ierr)
        END DO

    END SUBROUTINE send_locposts_to_Lb_masters

!-----------------------------------------------------------------------

    SUBROUTINE recv_numchannels_from_master

        IMPLICIT NONE

        INTEGER :: ierr, status1(MPI_STATUS_SIZE)

        CALL MPI_RECV(my_nchan, 1, MPI_INTEGER, 0, 0, Lb_m_comm, status1, ierr)

    END SUBROUTINE recv_numchannels_from_master

!-----------------------------------------------------------------------

    SUBROUTINE recv_locposts_from_master

        IMPLICIT NONE

        INTEGER :: ierr, status1(MPI_STATUS_SIZE)

        CALL MPI_RECV(my_post, 1, MPI_INTEGER, 0, 0, Lb_m_comm, status1, ierr)

    END SUBROUTINE recv_locposts_from_master

!-----------------------------------------------------------------------

    SUBROUTINE bcast_numchannels_in_Lblock

        IMPLICIT NONE

        INTEGER   :: ierr

        CALL MPI_BCAST(my_nchan, 1, MPI_INTEGER, 0, Lb_comm, ierr)

    END SUBROUTINE bcast_numchannels_in_Lblock

!-----------------------------------------------------------------------

    SUBROUTINE alloc_ib_surfamps (nfdm)

        USE distribute_hd_blocks2, ONLY: rowbeg, rowend
        USE initial_conditions,    ONLY : debug

        !Note 1: IB stands for Inside Boundary. IB_SurfaceAmps are the Eigenvectors
        !obtained from the Splinewaves file outputted from ham95. In RMT they are
        !used to generate an FD wavefunction inside the R-matrix Inner Region.
        !In the molecular case the surface amplitudes are preevaluated for the FD
        !points so they are only transfered from the wamp2 array to ib_surfs.

        INTEGER, INTENT(IN)         :: nfdm
        INTEGER                     :: err

        IF (debug) PRINT *, 'nfdm =', nfdm

        ! ACB : now need ib_surfs on every inner region core, not just block masters

        ALLOCATE (ib_surfs(rowbeg:rowend, nchmx, nfdm), stat = err)
        CALL assert(err .EQ. 0, 'allocation error with ib_surfs')

        ib_surfs(:, :, :) = 0.0_wp

    END SUBROUTINE alloc_ib_surfamps

!-----------------------------------------------------------------------

    SUBROUTINE get_displacements_for_wvouter(nfdm)

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: nfdm
        INTEGER                     :: i, err, ierr

        ALLOCATE (wvo_counts(0:Lb_m_size - 1), wvo_disp(0:Lb_m_size - 1), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with wvo_counts and wvo_disp')

        wvo_counts(:) = 0
        wvo_disp(:) = 0

        CALL MPI_ALLGATHER(nfdm*my_nchan, 1, MPI_INTEGER, wvo_counts, 1, MPI_INTEGER, &
                           Lb_m_comm, ierr)

        wvo_disp(0) = 0

        IF (Lb_m_size > 1) THEN
            DO i = 1, Lb_m_size - 1
                wvo_disp(i) = wvo_counts(i - 1) + wvo_disp(i - 1)
            END DO
        END IF

    END SUBROUTINE get_displacements_for_wvouter

!-----------------------------------------------------------------------

    !> \brief   Distribute wamp2 to inner region ranks
    !> \authors ?, Z Masin, D Clarke, J Benda
    !> \date    2017 - 2019
    !>
    !> Sends the pre-evaluated inside-boundary wave function from inner master to appropriate
    !> inner region ranks. The boundary amplitude storage on master is essentially a matrix with "nchmx"
    !> rows and "nstmx * nfdm * numsym" columns. Only columns corresponding to the interval "rowbeg:rowend"
    !> (from the correct symmetry column interval) are needed by every process. In this subroutine, the master
    !> process sends these narrow collumn intervals (altogether there is "nfdm" column intervals of width
    !> "numrows") to owning destination processes, including itself. Only then, each process transposes
    !> each of its received "nfdm" blocks from shape "nchmx x numrows" to "numrows x nchmx", which is later
    !> used in the inner-to-outer interface.
    !>
    SUBROUTINE scatter_ib_surfamps (nfdm)

        USE distribute_hd_blocks2, ONLY: rowbeg, rowend, numrows
        USE initial_conditions,    ONLY: no_of_pes_to_use_inner
        USE mpi_communications,    ONLY: i_am_inner_master, mpi_comm_region
        USE mpi_layer_lblocks,     ONLY: my_LML_block_id
        USE readhd,                ONLY: no_of_L_blocks, LML_block_Lblk, nstmx, wamp2

        INTEGER, INTENT(IN)  :: nfdm

        INTEGER, ALLOCATABLE      :: typ(:,:), sendtypes(:), sendcounts(:), displacmnt(:), recvcounts(:), recvtypes(:), &
                                     blocks(:), colbeg(:), colend(:)
        INTEGER                   :: i, j, ierr, ones(nfdm), np, LMLblk
        INTEGER(MPI_ADDRESS_KIND) :: zeros(nfdm)

        ones(:) = 1
        zeros(:) = 0
        np = no_of_pes_to_use_inner
        LMLblk = my_LML_block_id

        ALLOCATE (typ(nfdm,np), sendtypes(np), sendcounts(np), displacmnt(np), recvcounts(np), recvtypes(np), &
                  blocks(np), colbeg(np), colend(np), stat = ierr)
        CALL assert(ierr == 0, 'Failed to allocate memory in scatter_ib_surfamps')

        ! everyone gathers offset and block size information from all processes
        CALL MPI_ALLGATHER(LMLblk, 1, MPI_INTEGER, blocks, 1, MPI_INTEGER, mpi_comm_region, ierr)
        CALL MPI_ALLGATHER(rowbeg, 1, MPI_INTEGER, colbeg, 1, MPI_INTEGER, mpi_comm_region, ierr)
        CALL MPI_ALLGATHER(rowend, 1, MPI_INTEGER, colend, 1, MPI_INTEGER, mpi_comm_region, ierr)

        ! master translates LML block indices to L block indices and broadcasts them back (only really needed in atomic mode)
        IF (i_am_inner_master) blocks = LML_block_Lblk(blocks)
        CALL MPI_BCAST(blocks, no_of_pes_to_use_inner, MPI_INTEGER, 0, mpi_comm_region, ierr)

        ! everyone constructs sending types (though only master really needs to do this)
        sendcounts(:) = MERGE(1, 0, i_am_inner_master)  ! only send data from master
        DO i = 1, np
            ! define all sub-matrices of wamp2 to send to a single process
            DO j = 1, nfdm
                CALL MPI_TYPE_CREATE_SUBARRAY(2, (/ nchmx, nfdm * nstmx * no_of_L_blocks                              /),  &
                                                 (/ nchmx, colend(i) - colbeg(i) + 1                                  /),  &
                                                 (/ 0,     ((blocks(i) - 1) * nfdm + (j - 1)) * nstmx + colbeg(i) - 1 /),  &
                                                 MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, typ(j,i), ierr)
                CALL assert(ierr == MPI_SUCCESS, 'Failed to create subarray type in scatter_ib_surfamps.')
                CALL MPI_TYPE_COMMIT(typ(j,i), ierr)
                CALL assert(ierr == MPI_SUCCESS, 'Failed to commit subarray type in scatter_ib_surfamps')
            END DO

            ! pack all of the sub-matrices into a single type, so that one MPI call transfers all data
            CALL MPI_TYPE_CREATE_STRUCT(nfdm, ones, zeros, typ(:,i), sendtypes(i), ierr)
            CALL assert(ierr == MPI_SUCCESS, 'Failed to create struct type in scatter_ib_surfamps.')
            CALL MPI_TYPE_COMMIT(sendtypes(i), ierr)
            CALL assert(ierr == MPI_SUCCESS, 'Failed to commit struct type in scatter_ib_surfamps')
        END DO

        ! everyone defines the data to receive
        displacmnt(:) = 0
        recvcounts(:) = 0
        recvcounts(1) = nchmx * numrows * nfdm  ! only receive from master
        recvtypes(:)  = MPI_DOUBLE_PRECISION


        if (.NOT.ALLOCATED(wamp2)) THEN
            CALL assert(.NOT.i_am_inner_master,'Error!!! wamp2 not allocated on i_am_inner_master!!!')
            !wamp2 only used on block master, so only allocate a single element here so it passes mpi with -check all
            ALLOCATE(wamp2(0,0,0,0))
        ELSE
            CALL assert(i_am_inner_master,'Error!!! wamp2 allocated on not i_am_inner_master!!!')
        END IF

        ! send data to everyone (still with original transposition, which needs to be fixed below!)
        CALL MPI_ALLTOALLW(wamp2, sendcounts, displacmnt, sendtypes, &
                        ib_surfs, recvcounts, displacmnt, recvtypes, mpi_comm_region, ierr)
        CALL assert(ierr == MPI_SUCCESS, 'All-to-all communication failed in scatter_ib_surfamps')

        ! everyone deallocates the sending types
        DO i = 1, no_of_pes_to_use_inner
            DO j = 1, nfdm
                CALL MPI_TYPE_FREE(typ(j,i), ierr)
            END DO
            CALL MPI_TYPE_FREE(sendtypes(i), ierr)
        END DO

        ! wamp2 no longer needed anywhere
        IF (ALLOCATED(wamp2)) THEN
            DEALLOCATE (wamp2, stat = ierr)
        END IF

        ! locally transpose all received blocks
        DO i = 1, nfdm
            ib_surfs(rowbeg:rowend, 1:nchmx, i) = TRANSPOSE(RESHAPE(ib_surfs(rowbeg:rowend, 1:nchmx, i), (/ nchmx, numrows /)))
        END DO

    END SUBROUTINE scatter_ib_surfamps

!-----------------------------------------------------------------------

    SUBROUTINE dealloc_distribute_wv_data

        IMPLICIT NONE

        INTEGER   :: err

        DEALLOCATE (ib_surfs, wvo_counts, wvo_disp, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error for distribute_wv_data')

    END SUBROUTINE dealloc_distribute_wv_data

END MODULE distribute_wv_data
