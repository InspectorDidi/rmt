! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Controls the flow of information from the inner to outer region,
!> specifically the matching of the wavefunction on the inner region grid
!> points. Note that a separate module, outer_to_inner_interface handles the
!> flow of information in the other direction.
MODULE inner_to_outer_interface

    USE precisn,    ONLY: wp
    USE readhd,     ONLY: LML_block_tot_nchan
    USE rmt_assert, ONLY: assert
    USE initial_conditions, ONLY: numsols => no_of_field_confs
    USE MPI

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE    :: psi_at_inner_fd_pts(:, :, :)

    PUBLIC get_psi_at_inner_fd_pts
    PUBLIC alloc_psi_at_inner_fd_pts
    PUBLIC dealloc_psi_at_inner_fd_pts
    PUBLIC get_outer_initial_state_at_b
    PUBLIC send_psi_at_inner_fd_pts
    PUBLIC recv_psi_at_inner_fd_pts

CONTAINS

    SUBROUTINE get_psi_at_inner_fd_pts(nfdm, vecin)

        USE distribute_hd_blocks2, ONLY: rowbeg, rowend
        USE distribute_wv_data,    ONLY: ib_surfs, my_nchan, wvo_counts, wvo_disp
        USE global_data,           ONLY: zero
        USE mpi_layer_lblocks,     ONLY: i_am_block_master, Lb_m_comm, Lb_comm
        USE readhd,                ONLY: max_L_block_size

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: nfdm
        INTEGER                     :: i, ierr, j, jj, isol
        COMPLEX(wp), INTENT(IN)     :: vecin(max_L_block_size, 1:numsols)
        COMPLEX(wp)                 :: wv(nfdm, my_nchan, numsols)
        COMPLEX(wp)                 :: my_wv(nfdm, my_nchan, numsols)

        DO isol = 1, numsols
            DO j = 1, my_nchan
                DO i = 1, nfdm
                    wv(i, j, isol) = zero
                    my_wv(i, j, isol) = zero
                    DO jj = rowbeg, rowend ! now breaking up into sub-blocks
                        my_wv(i, j, isol) = my_wv(i, j, isol) + ib_surfs(jj, j, i)*vecin(jj, isol)
                    END DO
                END DO
            END DO
        END DO

        ! sum contributions from each sub-block onto block master
        IF (my_nchan /= 0) THEN
            CALL MPI_REDUCE(my_wv, wv, nfdm*my_nchan*numsols, MPI_DOUBLE_COMPLEX, MPI_SUM, 0, Lb_comm, ierr)
        END IF

        ! gather contributions all blocks into Psi_At_Inner_FD_Pts for transfer to outer region
        IF (i_am_block_master) THEN
            DO isol = 1, numsols
                CALL MPI_GATHERV(wv(:, :, isol), nfdm*my_nchan, MPI_DOUBLE_COMPLEX, psi_at_inner_fd_pts(:, :, isol), &
                                wvo_counts, wvo_disp, MPI_DOUBLE_COMPLEX, 0, Lb_m_comm, ierr)
            END DO
        END IF

    END SUBROUTINE get_psi_at_inner_fd_pts

!-----------------------------------------------------------------------

    SUBROUTINE alloc_psi_at_inner_fd_pts(nfdm)

        IMPLICIT NONE

        INTEGER, INTENT(IN)    :: nfdm
        INTEGER                :: err

        ALLOCATE (psi_at_inner_fd_pts(nfdm, LML_block_tot_nchan, numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with psi_at_inner_fd_pts')

    END SUBROUTINE alloc_psi_at_inner_fd_pts

!-----------------------------------------------------------------------

    SUBROUTINE dealloc_psi_at_inner_fd_pts

        IMPLICIT NONE

        INTEGER   :: err

        DEALLOCATE (psi_at_inner_fd_pts, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with psi_at_inner_fd_pts')

    END SUBROUTINE dealloc_psi_at_inner_fd_pts

!-----------------------------------------------------------------------
! INITIAL VALUE OF THE OUTER WAVEFUNCTION AT r=b
!----------------------------------------------------------------------- 

    SUBROUTINE get_outer_initial_state_at_b(psi_outer_at_b, psi_inner)

        USE distribute_hd_blocks, ONLY: my_surf_amps
        USE readhd,               ONLY: max_L_block_size

        COMPLEX(wp), INTENT(INOUT) :: psi_outer_at_b(1:LML_block_tot_nchan, 1:numsols)
        COMPLEX(wp), INTENT(IN)    :: psi_inner(1:max_L_block_size, 1:numsols)
        INTEGER                    :: channel_id, isol

        ! On the boundary f^gamma_p(b,t)= sum_k C^gamma_k w^gamma_kp
        ! where gamma labels symmetries, p labels channels, k labels eigenstates in inner basis
        ! C=coefficients of inner region basis functions, w_kp=surface amplitudes

        DO isol = 1, numsols
            DO channel_id = 1, LML_block_tot_nchan
                psi_outer_at_b(channel_id, isol) = DOT_PRODUCT(psi_inner(:, isol), my_surf_amps(:, channel_id))
            END DO
        END DO

    END SUBROUTINE get_outer_initial_state_at_b

!-----------------------------------------------------------------------

    SUBROUTINE send_psi_at_inner_fd_pts(nfdm)

        USE communications_parameters, ONLY: id_of_1st_pe_outer, pe_id_1st
        USE mpi_communications,        ONLY: get_my_pe_id

        INTEGER, INTENT(IN) :: nfdm
        INTEGER             :: tag, ierror, my_rank

        ! Get my rank
        CALL get_my_pe_id(my_rank)

        ! If I am the master PE in the inner region then send data to outer region:
        IF (my_rank == pe_id_1st) THEN

            tag = 33
            CALL MPI_SEND(psi_at_inner_fd_pts, nfdm*LML_block_tot_nchan*numsols, MPI_DOUBLE_COMPLEX, &
                          id_of_1st_pe_outer, tag, MPI_COMM_WORLD, ierror)

        END IF

    END SUBROUTINE send_psi_at_inner_fd_pts

!-----------------------------------------------------------------------

    SUBROUTINE recv_psi_at_inner_fd_pts(nfdm, number_channels, numsols, vecout)

        USE communications_parameters, ONLY: id_of_1st_pe_outer, pe_id_1st
        USE mpi_communications,        ONLY: get_my_pe_id

        INTEGER, INTENT(IN)      :: nfdm, number_channels, numsols
        COMPLEX(wp), INTENT(OUT) :: vecout(nfdm, number_channels, numsols)

        INTEGER :: tag, ierror, My_rank
        INTEGER :: status(MPI_STATUS_SIZE)

        vecout = (0.0_wp, 0.0_wp)

        ! Get my rank
        CALL get_my_pe_id(My_rank)

        ! If I am the master PE in the outer region then recv data from inner region:
        IF (my_rank == id_of_1st_pe_outer) THEN

            tag = 33
            CALL MPI_RECV(vecout, nfdm*number_channels*numsols, MPI_DOUBLE_COMPLEX, &
                          pe_id_1st, tag, MPI_COMM_WORLD, status, ierror)

        END IF

    END SUBROUTINE recv_psi_at_inner_fd_pts

END MODULE inner_to_outer_interface
