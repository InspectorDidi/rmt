! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles periodic write-outs of wavefunction files, calculation parameters and
!> data arrays for calculation restart.

MODULE checkpoint

    USE precisn,            ONLY: wp
    USE mpi_communications, ONLY: i_am_inner_master, &
                                  my_block_id

    IMPLICIT NONE

CONTAINS

    SUBROUTINE checkpoint_writes(stage, &
                                 stage_first, &
                                 pop_index, &
                                 previous_timeindex, &
                                 Z_minus_N, &
                                 r_at_region_bndry, &
                                 max_L_block_size)

        USE wall_clock,         ONLY: hel_time

        INTEGER, INTENT(IN)  :: stage
        INTEGER, INTENT(IN)  :: stage_first
        INTEGER, INTENT(IN)  :: pop_index
        INTEGER, INTENT(IN)  :: previous_timeindex
        INTEGER, INTENT(IN)  :: max_L_block_size
        REAL(wp), INTENT(IN) :: Z_minus_N
        REAL(wp), INTENT(IN) :: r_at_region_bndry

        REAL(wp)             :: checkpoint_start_time
        REAL(wp)             :: checkpoint_end_time

        checkpoint_start_time = hel_time()


        CALL checkpoint_data_files(stage, &
                                   stage_first, &
                                   pop_index, &
                                   my_block_id)

        CALL write_status_file(stage, &
                               previous_timeindex, &
                               Z_minus_N, &
                               r_at_region_bndry, &
                               max_L_block_size)

        checkpoint_end_time = hel_time()

        IF (i_am_inner_master) THEN
            PRINT*
            PRINT *, 'Checkpointing: Time taken (secs): ', checkpoint_end_time - checkpoint_start_time
            PRINT*
        END IF

    END SUBROUTINE checkpoint_writes

!---------------------------------------------------------------------------

    SUBROUTINE write_status_file(previous_stage, &
                                 previous_timeindex, &
                                 Z_minus_N, &
                                 r_at_region_bndry, &
                                 max_L_block_size)

        USE calculation_parameters, ONLY: delta_t
        USE grid_parameters,        ONLY: x_1st, &
                                          x_last, &
                                          channel_id_1st, &
                                          channel_id_last
        USE initial_conditions,     ONLY: use_2colour_field, &
                                          lplusp, &
                                          set_ML_max, &
                                          ML_max, &
                                          ellipticity, &
                                          ellipticity_xuv, &
                                          frequency, &
                                          frequency_xuv, &
                                          periods_of_ramp_on, &
                                          periods_of_ramp_on_xuv, &
                                          periods_of_pulse, &
                                          periods_of_pulse_xuv, &
                                          intensity, &
                                          intensity_xuv, &
                                          no_of_pes_to_use, &
                                          no_of_pes_to_use_inner, &
                                          no_of_pes_to_use_outer, &
                                          propagation_order, &
                                          deltar, &
                                          delay_xuv_in_ir_periods, &
                                          timesteps_per_output, &
                                          absorb_desired, &
                                          absorption_interval, &
                                          sigma_factor, &
                                          start_factor
        USE io_routines,            ONLY: update_status

        INTEGER, INTENT(IN)  :: previous_stage
        INTEGER, INTENT(IN)  :: previous_timeindex
        REAL(wp), INTENT(IN) :: Z_minus_N
        REAL(wp), INTENT(IN) :: r_at_region_bndry
        INTEGER, INTENT(IN)  :: max_L_block_size

        IF (i_am_inner_master) THEN
            CALL update_status(previous_stage, &
                               previous_timeindex, &
                               timesteps_per_output, &
                               no_of_pes_to_use, &
                               no_of_pes_to_use_inner, &
                               no_of_pes_to_use_outer, &
                               Z_minus_N, &
                               lplusp, &
                               set_ML_max, &
                               ML_max, &
                               r_at_region_bndry, &
                               delta_t, &
                               deltar, &
                               x_1st, &
                               x_last, &
                               channel_id_1st, &
                               channel_id_last, &
                               max_L_block_size, &
                               propagation_order, &
                               absorb_desired, &
                               absorption_interval, &
                               sigma_factor, &
                               start_factor, &
                               use_2colour_field, &
                               ellipticity(1), &
                               intensity(1), &
                               frequency(1), &
                               periods_of_ramp_on(1), &
                               periods_of_pulse(1), &
                               ellipticity_xuv(1), &
                               intensity_xuv(1), &
                               frequency_xuv(1), &
                               periods_of_ramp_on_xuv(1), &
                               periods_of_pulse_xuv(1), &
                               delay_xuv_in_ir_periods(1))
        END IF

    END SUBROUTINE write_status_file

!---------------------------------------------------------------------------

    SUBROUTINE checkpoint_data_files(stage, &
                                     stage_first, &
                                     pop_index, &
                                     my_block_id)

        USE grid_parameters,    ONLY: channel_id_1st, &
                                      channel_id_last
        USE initial_conditions, ONLY: keep_checkpoints,&
                                      dipole_output_desired,&
                                      dipole_velocity_output,&
                                      binary_data_files,&
                                      keep_popL
        USE io_routines,        ONLY: write_real_arrays, &
                                      binary_write_arrays,&
                                      get_array_name,&
                                      write_my_wavefunc_inner, &
                                      write_my_wavefunc_outer, &
                                      write_my_wavefunc_inner_checkpt, &
                                      write_my_wavefunc_outer_checkpt
        USE krylov_method,      ONLY: update_psi_inner
        USE propagators,        ONLY: update_psi_outer
        USE mpi_communications, ONLY: i_am_outer_master, &
                                      i_am_in_outer_region, &
                                      i_am_in_inner_region, &
                                      all_processor_barrier
        USE mpi_layer_lblocks,  ONLY: i_am_block_master, &
                                      i_am_gs_master
        USE wavefunction,       ONLY: population_gs_inner, &
                                      population_all_outer_L, &
                                      psi_outer, &
                                      psi_inner, &
                                      data_1st, &
                                      data_last, &
                                      discarded_pop_outer, &
                                      discarded_pop_outer_si, &
                                      time_in_au

        INTEGER, INTENT(IN) :: stage
        INTEGER, INTENT(IN) :: stage_first
        INTEGER, INTENT(IN) :: pop_index
        INTEGER, INTENT(IN) :: my_block_id

        INTEGER             :: index_of_first_data_pt
        INTEGER             :: index_of_last_data_pt
        INTEGER             :: channel_id
        LOGICAL             :: this_is_the_initial_stage

        CHARACTER(LEN=6)    :: array_name_6

        this_is_the_initial_stage = .false.
        IF (stage == stage_first) THEN
            this_is_the_initial_stage = .true.
        END IF

        index_of_first_data_pt = data_1st
        index_of_last_data_pt = pop_index

        IF (i_am_inner_master) THEN

            ! flush memory buffers to the output files
            FLUSH (37) ! pop_inn
            FLUSH (38) ! pop_all
            FLUSH (46) ! EField

            IF (dipole_output_desired) THEN
                FLUSH (58)  ! expec_z_all
                IF (dipole_velocity_output) FLUSH (63) ! expec_v_all
            END IF

        END IF


        IF (i_am_gs_master) THEN
            array_name_6 = 'pop_GS'
            CALL write_real_arrays &
                (time_in_au, population_gs_inner, &
                 array_name_6, &
                 this_is_the_initial_stage, &
                 data_1st, Data_Last, &
                 index_of_first_data_pt, index_of_last_data_pt)

        END IF

        ! Only continue once the inner master finished writing inner populations.
        ! This is needed to be sure that the outer master will not encounter
        ! locked output directory (i.e. directory in the process of creation)
        ! in the code that follows.
        CALL all_processor_barrier

        IF (i_am_outer_master) THEN

            ! flush memory buffers to the output files
            FLUSH (36) ! pop_out

!           array_name_6  = 'popout'
!           CALL write_real_arrays                      &
!               (time_in_au, population_all_outer,      &
!                array_name_6,                          &
!                this_is_the_initial_stage,             &
!                data_1st, data_last,                   &
!                index_of_first_data_pt, index_of_last_data_pt)
!
!           array_name_6  = 'pop_SI'
!           CALL write_real_arrays                         &
!               (time_in_au, population_all_outer_si,      &
!                array_name_6,                             &
!                this_is_the_initial_stage,                &
!                data_1st, data_last,                      &
!                index_of_first_data_pt, index_of_last_data_pt)

!           array_name_6  = 'pp_Ryd'
!           CALL write_real_arrays                         &
!               (time_in_au, population_all_outer_ryd,     &
!                array_name_6,                             &
!                this_is_the_initial_stage,                &
!                data_1st, data_last,                      &
!                index_of_first_data_pt, index_of_last_data_pt)

!           array_name_6  = 'p_r1st'
!           CALL write_real_arrays                    &
!               (time_in_au, population_at_r1st,      &
!                array_name_6,                        &
!                this_is_the_initial_stage,           &
!                data_1st, data_last,                 &
!                index_of_first_data_pt, index_of_last_data_pt)

            IF (keep_popL) THEN
                IF (binary_data_files) THEN
                    ASSOCIATE (nrec => (index_of_last_data_pt - index_of_first_data_pt + 1))
                    ASSOCIATE (nchan=> (channel_id_last-channel_id_1st+1))
                    array_name_6='popchn'
                    CALL binary_write_arrays(time_in_au(index_of_first_data_pt:index_of_last_data_pt),&
                                             population_all_outer_L(index_of_first_data_pt:index_of_last_data_pt, :, :),&
                                             array_name_6,&
                                             nrec,&
                                             nchan)
                    END ASSOCIATE                                         
                    END ASSOCIATE                                         
                                                         
                ELSE                                         
                    DO channel_id = channel_id_1st, channel_id_last
                       
                        CALL get_array_name(channel_id,array_name_6)
                       
                        CALL write_real_arrays &
                            (time_in_au, population_all_outer_L(:, channel_id, :), &
                             array_name_6, &
                             this_is_the_initial_stage, &
                             data_1st, data_last, &
                             index_of_first_data_pt, index_of_last_data_pt)
                    END DO
                END IF
            END IF

!           SELECT CASE (choice_of_wave_propagator)
!           CASE (arnoldi_series)
!           array_name_6  = 'mxevlo'
!           CALL write_real_arrays               &
!               (time_in_au, max_eval,           &
!                array_name_6,                   &
!                this_is_the_initial_stage,      &
!                data_1st, data_last,            &
!                index_of_first_data_pt, index_of_last_data_pt)

!           array_name_6  = 'GSevlo'
!           CALL write_real_arrays               &
!               (time_in_au, min_eval,           &
!                array_name_6,                   &
!                this_is_the_initial_stage,      &
!                data_1st, data_last,            &
!                index_of_first_data_pt, index_of_last_data_pt)

!           END SELECT

        END IF


        ! Each of us prints:

        ! update and checkpoint outer wave function
        ! + optionally store wave function in stage-labeled directory
        IF (i_am_in_outer_region) THEN
            CALL update_psi_outer(psi_outer)
            CALL write_my_wavefunc_outer(psi_outer, my_block_id, discarded_pop_outer, discarded_pop_outer_si)
            IF (keep_checkpoints) THEN
                CALL write_my_wavefunc_outer_checkpt(psi_outer, my_block_id, discarded_pop_outer, discarded_pop_outer_si, stage)
            END IF
        END IF

        ! update and checkpoint inner wave function
        ! + optionally store wave function in stage-labeled directory
        IF (i_am_in_inner_region) THEN
            CALL update_psi_inner(psi_inner)
            CALL write_my_wavefunc_inner(psi_inner, my_block_id, i_am_block_master)
            IF (keep_checkpoints) THEN
                CALL write_my_wavefunc_inner_checkpt(psi_inner, my_block_id, i_am_block_master, stage)
            END IF
        END IF

    END SUBROUTINE checkpoint_data_files

END MODULE checkpoint
