! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Checks whether periodic tasks should be implemented on this time step.
MODULE work_at_intervals

    IMPLICIT NONE

CONTAINS

    LOGICAL FUNCTION times_come_to_do_the_following(time, work_interval)

        INTEGER :: time, work_interval

        times_come_to_do_the_following = .false.
        IF (work_interval > 0) THEN
            IF (MOD(time, work_interval) == 0) THEN
                times_come_to_do_the_following = .true.
            END IF
        END IF

    END FUNCTION times_come_to_do_the_following

END MODULE work_at_intervals

