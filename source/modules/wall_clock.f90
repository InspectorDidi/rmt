! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles timing of calculation, including writing out timing files.

MODULE wall_clock

    USE precisn,            ONLY: wp, longint
    USE io_parameters,      ONLY: version
    USE initial_conditions, ONLY: disk_path

    IMPLICIT NONE

    INTEGER(longint), PRIVATE :: clok_iteration_start

    REAL(wp) :: time_start_inner, time_end_inner
    REAL(wp) :: time_diff_inner, sum_wait_time_inner

    REAL(wp) :: time1, time2, time3, time4, time5, time6, time7
    REAL(wp) :: sum_time_bndries, sum_time_surf, sum_time_arnoldi, sum_time_sendtoinner
    REAL(wp) :: sum_time_firstpe, sum_time_setup, sum_time_wait
    REAL(wp) :: sum_time_bound, sum_time_diag, sum_time_gram, sum_time_trans
    REAL(wp) :: program_start_time

CONTAINS

    REAL(wp) FUNCTION hel_time()

        INTEGER(longint) :: it0, count_rate

        it0 = 0
        count_rate = 1

        CALL SYSTEM_CLOCK(it0, count_rate)
        hel_time = REAL(it0, wp) / REAL(count_rate, wp)

    END FUNCTION hel_time

!-----------------------------------------------------------------------

    SUBROUTINE start_program_clock

        INTEGER(longint) :: clok, count_rate

        program_start_time = hel_time()
        CALL SYSTEM_CLOCK(clok, count_rate)
        clok_iteration_start = clok

    END SUBROUTINE start_program_clock

!-----------------------------------------------------------------------

    SUBROUTINE output_time(iteration_time)

        REAL(wp), INTENT(OUT) :: iteration_time
        INTEGER(longint)      :: clok_iteration_end, count_rate

        CALL SYSTEM_CLOCK(clok_iteration_end, count_rate)
        iteration_time = REAL(clok_iteration_end - clok_iteration_start, wp) / REAL(count_rate, wp)
        clok_iteration_start = clok_iteration_end

    END SUBROUTINE output_time

!-----------------------------------------------------------------------

    SUBROUTINE update_time_start_inner(k1)

        INTEGER, INTENT(IN) :: k1

        time_start_inner = hel_time()

        IF (k1 .EQ. -1) THEN ! reset to 0
            sum_wait_time_inner = 0.0_wp
            sum_time_arnoldi = 0.0_wp
            sum_time_trans = 0.0_wp
            sum_time_gram = 0.0_wp
            sum_time_diag = 0.0_wp
            sum_time_bound = 0.0_wp
        END IF

    END SUBROUTINE update_time_start_inner

!-----------------------------------------------------------------------

    SUBROUTINE update_start_iter

        time1 = hel_time()

    END SUBROUTINE update_start_iter

!-----------------------------------------------------------------------

    SUBROUTINE update_start_arnoldi

        time2 = hel_time()
        sum_time_bound = sum_time_bound - time1 + time2

    END SUBROUTINE update_start_arnoldi

!-----------------------------------------------------------------------

    SUBROUTINE update_start_gram

        time1 = hel_time()
        sum_time_arnoldi = sum_time_arnoldi - time2 + time1

    END SUBROUTINE update_start_gram

!-----------------------------------------------------------------------

    SUBROUTINE update_start_trans

        time2 = hel_time()
        sum_time_gram = sum_time_gram - time1 + time2

    END SUBROUTINE update_start_trans

!-----------------------------------------------------------------------

    SUBROUTINE update_start_diag

        time3 = hel_time()
        sum_time_trans = sum_time_trans + time3 - time2
        time2 = time3

    END SUBROUTINE update_start_diag

!-----------------------------------------------------------------------

    SUBROUTINE update_end_iter

        time1 = hel_time()
        sum_time_diag = sum_time_diag + time1 - time2

    END SUBROUTINE update_end_iter

!-----------------------------------------------------------------------

    SUBROUTINE update_time_end_inner

        time_end_inner = hel_time()
        time_diff_inner = time_end_inner - time_start_inner
        sum_wait_time_inner = sum_wait_time_inner + time_diff_inner
        sum_time_bound = sum_time_bound - time_diff_inner

    END SUBROUTINE update_time_end_inner

!-----------------------------------------------------------------------

    SUBROUTINE open_inner_timings_file

        OPEN (UNIT=53, FILE=disk_path//'timing_inner.'//version)

        WRITE (53, *) 'Iteration ---  Wait  ---   Bound   --- arnoldi  --- gram   --- trans   --- diag'

    END SUBROUTINE open_inner_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE write_timings_inner(iteration_time)

        REAL(wp), INTENT(IN) :: Iteration_Time

        WRITE (53, '(E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4)') &
            iteration_time, sum_wait_time_inner, sum_time_bound, &
            sum_time_arnoldi, sum_time_gram, sum_time_trans, sum_time_diag
        FLUSH (53)

    END SUBROUTINE write_timings_inner

!-----------------------------------------------------------------------

    SUBROUTINE close_inner_timings_file

        CLOSE (53)

    END SUBROUTINE close_inner_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE open_outer0_timings_file

        OPEN (UNIT=55, FILE=disk_path//'timing_outer0.'//version)

        WRITE (55, *) 'Iteration-- Bndries --- setup --- bndries --- surf --- arnoldi ---  send   ---  wait'

    END SUBROUTINE open_outer0_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE write_timings_outer0(iteration_time)

        REAL(wp), INTENT(IN) :: iteration_time

        WRITE (55, '(E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3)') &
            iteration_time, sum_time_firstpe, sum_time_setup, sum_time_bndries, &
            sum_time_surf, sum_time_arnoldi, sum_time_sendtoinner, sum_time_wait
        FLUSH (55)

    END SUBROUTINE write_timings_outer0

!-----------------------------------------------------------------------

    SUBROUTINE close_outer0_timings_file

        CLOSE (55)

    END SUBROUTINE close_outer0_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE open_outer1_timings_file

        OPEN (UNIT=54, FILE=disk_path//'timing_outer1.'//version)

        WRITE (54, *) 'Iteration ---  firstpe ----   setup  ---  bndries --- Arnoldi'

    END SUBROUTINE open_outer1_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE write_timings_outer1(iteration_time)

        REAL(wp), INTENT(IN) :: iteration_time

        WRITE (54, '(E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3)') Iteration_Time, sum_time_firstpe, &
            sum_time_setup, sum_time_bndries, sum_time_arnoldi
        FLUSH (54)

    END SUBROUTINE write_timings_outer1

!-----------------------------------------------------------------------

    SUBROUTINE close_outer1_timings_file

        CLOSE (54)

    END SUBROUTINE close_outer1_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time1(m)

        INTEGER, INTENT(IN) :: m

        ! reset sums of times to zero at start of each iteration
        IF (m .EQ. -1) THEN
            sum_time_bndries = 0.0_wp
            sum_time_surf = 0.0_wp
            sum_time_arnoldi = 0.0_wp
            sum_time_sendtoinner = 0.0_wp
            sum_time_wait = 0.0_wp
        END IF

        time7 = hel_time()

        IF (m .EQ. 0) THEN
            sum_time_setup = time7 - time1
        END IF

        time1 = time7

    END SUBROUTINE update_outer_time1

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time2

        time2 = hel_time()

        sum_time_bndries = sum_time_bndries + time2 - time1

    END SUBROUTINE update_outer_time2

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time3

        time3 = hel_time()

        sum_time_surf = sum_time_surf + time3 - time2

    END SUBROUTINE update_outer_time3

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time3_inter

        time2 = hel_time()

        sum_time_wait = sum_time_wait + time2 - time3

    END SUBROUTINE update_outer_time3_inter

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time4

        time4 = hel_time()

        sum_time_arnoldi = sum_time_arnoldi + time4 - time3

    END SUBROUTINE update_outer_time4

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time5

        time5 = hel_time()

    END SUBROUTINE update_outer_time5

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time6

        time6 = hel_time()

        sum_time_sendtoinner = sum_time_sendtoinner + time6 - time5

    END SUBROUTINE update_outer_time6

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time7

        time7 = hel_time()

        sum_time_firstpe = time7 - time1

        time1 = time7

    END SUBROUTINE update_outer_time7

!-----------------------------------------------------------------------

    SUBROUTINE write_timings_files(iteration_time, &
                                   i_am_inner_master, &
                                   i_am_outer_master, &
                                   i_am_outer_rank1)

        USE mpi_communications, ONLY : all_processor_barrier                                   
        USE calculation_parameters, ONLY : using_H_outer, &
                                           calc_psi_derivs_at_bndry_by

        REAL(wp), INTENT(IN) :: iteration_time
        LOGICAL, INTENT(IN)  :: i_am_inner_master
        LOGICAL, INTENT(IN)  :: i_am_outer_master
        LOGICAL, INTENT(IN)  :: i_am_outer_rank1
        REAL(wp)             :: start_wait

        start_wait = hel_time()
        call all_processor_barrier
        sum_time_wait = MERGE(hel_time() - start_wait, sum_time_wait, &
                              calc_psi_derivs_at_bndry_by == using_H_outer)

        IF (i_am_inner_master) THEN
            CALL write_timings_inner(iteration_time)
        END IF

        IF (i_am_outer_master) THEN
            CALL Write_Timings_Outer0(iteration_time)
        END IF

        IF (i_am_outer_rank1) THEN
            CALL write_timings_outer1(iteration_time)
        END IF

    END SUBROUTINE write_timings_files

END MODULE wall_clock
