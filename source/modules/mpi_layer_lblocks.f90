! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets up the first level of parallelism in the inner region- divides the
!> Hamiltonian matrix into symmetry blocks and then allocates processors to blocks based on
!> the block size. Also sets up second layer of parallelism- sets up communicators
!> within each block, and between the block masters.

MODULE mpi_layer_lblocks

    USE readhd,             ONLY: no_of_LML_blocks
    USE mpi_communications, ONLY: mpi_comm_region
    USE rmt_assert,         ONLY: assert
    USE MPI

    IMPLICIT NONE

    INTEGER, ALLOCATABLE, SAVE  :: Lb_comm_size(:)
    INTEGER, SAVE               :: Lb_color, Lb_comm, Lb_size, Lb_rank
    INTEGER, SAVE               :: Lb_master_color, Lb_master_rank
    INTEGER, SAVE               :: Lb_m_comm, Lb_m_size, Lb_m_rank
    INTEGER, SAVE               :: my_LML_block_id  ! = my_ang_mom_id = Lb_color
    LOGICAL, SAVE               :: i_am_block_master
    LOGICAL, SAVE               :: i_have_gs_L
    LOGICAL, SAVE               :: i_am_gs_master
    INTEGER, SAVE               :: pe_gs_id
    INTEGER, SAVE               :: inner_region_rank

    PRIVATE setup_Lblock_communicator_size
    PRIVATE setup_colors_for_Lblock_comms
    PRIVATE setup_inner_region_rank
 
    PUBLIC setup_inner_region_comms
    PUBLIC createcomm_order_Lblock
    PUBLIC createcomm_order_Lblock_masters
    PUBLIC Lb_m_comm, Lb_comm, Lb_size, Lb_rank, Lb_m_size, Lb_m_rank
    PUBLIC dealloc_setup_Lblock_comm_size
    PUBLIC i_am_block_master
    PUBLIC my_LML_block_id
    PUBLIC inner_region_rank

CONTAINS

!-----------------------------------------------------------------------
!SETUP COMMUNICATORS AASOCIATED WITH EACH L BLOCK   
!-----------------------------------------------------------------------

    SUBROUTINE setup_inner_region_comms(i_am_inner_master, states_per_LML_block, &
                                        my_ang_mom_id)

        LOGICAL, INTENT(IN)    :: i_am_inner_master
        INTEGER, INTENT(IN)    :: states_per_LML_block(-1:no_of_LML_blocks)
        INTEGER, INTENT(INOUT) :: my_ang_mom_id

        CALL setup_inner_region_rank
        CALL Setup_Lblock_Communicator_Size(i_am_inner_master, states_per_LML_block)
        CALL Setup_Colors_For_Lblock_Comms(my_ang_mom_id)

    END SUBROUTINE setup_inner_region_comms

!-----------------------------------------------------------------------

    SUBROUTINE setup_inner_region_rank

        INTEGER :: ierr

        CALL mpi_comm_rank(mpi_comm_region, inner_region_rank, ierr)

    END SUBROUTINE setup_inner_region_rank

!-----------------------------------------------------------------------
! DDAC: NOTE THAT EVERY 'Lblock' BELOW SHOULD BE UNDERSTOOD AS 'LMLblock'
!-----------------------------------------------------------------------

    SUBROUTINE setup_Lblock_communicator_size(i_am_inner_master, states_per_LML_block)

        USE initial_conditions, ONLY : debug

        LOGICAL, INTENT(IN) :: i_am_inner_master
        ! ACB: hugo fix for bug in inner block allocation
        INTEGER, INTENT(IN) :: states_per_LML_block(-1:no_of_LML_blocks)

        INTEGER :: no_of_inner_region_pes
        INTEGER :: err, L, Iterations
        INTEGER :: states_per_sub_block(no_of_LML_blocks)
        INTEGER :: biased_states_per_LML_block(no_of_LML_blocks)
        INTEGER :: max_sub_block_LML(1), no_of_remaining_pes

        ALLOCATE (Lb_comm_size(no_of_LML_blocks), stat=err)
        CALL assert(err .EQ. 0, 'allocation error in Setup_Lblock_Communicator_Size')

        CALL MPI_COMM_SIZE(mpi_comm_region, no_of_inner_region_pes, err)

        IF (no_of_inner_region_pes < no_of_LML_blocks) THEN
            PRINT *, 'no_of_inner_region_pes = ', no_of_inner_region_pes
            PRINT *, 'Number of LML blocks     = ', no_of_LML_blocks
            PRINT *, 'Must have no_of_inner_region_pes >= no_of_LML_blocks'
        END IF

        Lb_comm_size = 1
        no_of_remaining_pes = no_of_inner_region_pes - no_of_LML_blocks

        biased_states_per_LML_block = NINT((DBLE(states_per_LML_block(1:no_of_LML_blocks)))**1.8)
        ! Bias the allocation by pretending we had a different no of states per block.
        ! Perfect parallelism: would want States_Per_Sub_Block = states_per_LML_block**2

        states_per_sub_block = biased_states_per_LML_block

        DO iterations = 1, SUM(states_per_LML_block)

            IF (no_of_remaining_pes < 1) THEN
                EXIT
            END IF

            ! allocate the PE to LML_Block with the most states:

            max_sub_block_LML = MAXLOC(states_per_sub_block, 1)
            L = max_sub_block_LML(1)
            Lb_comm_size(L) = Lb_comm_size(L) + 1
            no_of_remaining_pes = no_of_remaining_pes - 1

            states_per_sub_block(L) = biased_states_per_LML_block(L)/Lb_comm_size(L)

        END DO

        CALL assert(SUM(Lb_comm_size) .EQ. no_of_inner_region_pes, 'Number of processors assigned to each LML block is incorrect.')

        IF (i_am_inner_master .AND. debug) THEN
            PRINT *, 'LML block Communicator size'
            DO L = 1, no_of_LML_blocks              ! DDAC: NOTE THAT L NO LONGER ASSUMES THE VALUES OF THE ANGULAR MOMENTA
                PRINT *, L, Lb_comm_size(L)         ! AND IS SIMPLY AN INDEX
            END DO
        END IF

    END SUBROUTINE setup_Lblock_communicator_size

!-----------------------------------------------------------------------
! SET UP COLORS FOR EACH COMMUNICATOR                         
!-----------------------------------------------------------------------

    SUBROUTINE setup_colors_for_Lblock_comms(my_ang_mom_id)     !DDAC: LMLblock_Comms

        USE initial_conditions, ONLY: gs_finast, debug

        IMPLICIT NONE

        INTEGER :: rsum, i, j
        INTEGER, INTENT(INOUT) :: my_ang_mom_id

        i_have_gs_L = .false.
        i_am_gs_master = .false.

        Lb_color = 0
        rsum = 0

        ! Each core works with a block of given symmetry
        ! Set Lb_color = i, where i is the ID of the block symmetry
        DO i = 1, no_of_LML_blocks
            DO j = 1, Lb_comm_size(i)
                rsum = rsum + 1
                IF ((inner_region_rank + 1) == rsum) THEN
                    Lb_color = i          !Lblock Color

                    IF (debug) PRINT *, 'i =', i, ' j =', j, ' Lb_color =', Lb_color

                    IF (i .EQ. gs_finast) THEN
                        i_have_gs_L = .true.
                        IF (debug) PRINT *, 'Process ', inner_region_rank, 'has GS L'
                    END IF

                END IF
            END DO
        END DO

        my_ang_mom_id = Lb_color
        my_LML_block_id = Lb_color

        CALL createcomm_order_Lblock(inner_region_rank, Lb_color)

    END SUBROUTINE setup_colors_for_Lblock_comms

!-----------------------------------------------------------------------
! SPLIT MPI_COMM_WORLD                               
!-----------------------------------------------------------------------

    SUBROUTINE createcomm_order_Lblock(inner_region_rank, Lb_color)

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: inner_region_rank, Lb_color
        INTEGER :: ierr

        ! Create a communicator to contain all cores working with same symmetry block
        ! (i.e. mpi_split mpi_comm_region)
        CALL MPI_COMM_SPLIT(mpi_comm_region, Lb_color, inner_region_rank, Lb_comm, ierr)
        CALL MPI_COMM_SIZE(Lb_comm, Lb_size, ierr)
        CALL MPI_COMM_RANK(Lb_comm, Lb_rank, ierr)

        CALL createcomm_order_Lblock_masters(Lb_rank, inner_region_rank)

    END SUBROUTINE createcomm_order_Lblock

!-----------------------------------------------------------------------
! GROUP THE MASTER NODES OF EACH BLOCK COMMUNICATOR INTO A 
! MASTER COMMUNICATOR                                          
!-----------------------------------------------------------------------

    SUBROUTINE createcomm_order_Lblock_masters(Lb_rank, inner_region_rank)
        USE initial_conditions, ONLY : debug

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: Lb_rank, inner_region_rank
        INTEGER :: ierr

        i_am_block_master = .false.

        Lb_master_color = 0

        IF (Lb_rank == 0) THEN
            Lb_master_color = no_of_LML_blocks + 1
            Lb_master_rank = inner_region_rank
            i_am_block_master = .true.
            IF (i_have_gs_L) i_am_gs_master = .true.
        END IF

        ! Lb_m_comm contains all LML block masters that handle the same propagation orders
        ! i.e. the mpi_split is performed on ocomm : now on mpi_comm_region as spltype is removed
        CALL MPI_COMM_SPLIT(mpi_comm_region, Lb_master_color, Lb_master_rank, &
                            Lb_m_comm, ierr)
        CALL MPI_COMM_SIZE(Lb_m_comm, Lb_m_size, ierr)
        CALL MPI_COMM_RANK(Lb_m_comm, Lb_m_rank, ierr)

        IF (i_am_block_master .AND. debug) THEN
            PRINT *, 'in_reg_rank', inner_region_rank, 'Lb_m_rank', Lb_m_rank, 'Lb_m_size', Lb_m_size
        END IF

    END SUBROUTINE createcomm_order_Lblock_masters

!-----------------------------------------------------------------------
! DEALLOCATIONS                                             
!-----------------------------------------------------------------------

    SUBROUTINE dealloc_setup_Lblock_comm_size

        IMPLICIT NONE

        INTEGER :: err

        DEALLOCATE (Lb_comm_size, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with setup_Lblock_communicator_size')

    END SUBROUTINE dealloc_setup_Lblock_comm_size

END MODULE mpi_layer_lblocks
