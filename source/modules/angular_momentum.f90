! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles angular momentum algebra, Clebsch Gordan
!! /Racah coefficients and factorials.

MODULE angular_momentum

    USE precisn, ONLY: wp

    IMPLICIT NONE

    REAL(wp), ALLOCATABLE, SAVE :: gammas(:)

    LOGICAL, SAVE               :: gamma = .false.
    INTEGER, SAVE               :: ngam = 500
    REAL(wp), ALLOCATABLE, SAVE :: gam(:)    ! logs of factorials

    INTEGER, SAVE               :: lrang1
    INTEGER, SAVE               :: lrang3
    INTEGER, SAVE               :: isran1
    INTEGER, SAVE               :: isran2
    REAL(wp), ALLOCATABLE, SAVE :: spinw(:, :)
    REAL(wp), ALLOCATABLE, SAVE :: rmetab(:, :, :)

    PRIVATE
    PUBLIC cracah, dracah, ractab, rme
    PUBLIC lrang1, lrang3, isran1, isran2, spinw, rmetab
    PUBLIC cg, dcg
    PUBLIC complex_spherical_harmonic, real_spherical_harmonic

CONTAINS

    SUBROUTINE cracah(i, j, k, l, m, n, rac)
    ! Calculate racah coefficients

        INTEGER, INTENT(IN)   :: i, j, k, l, m, n  ! 2 * angular momenta
        REAL(wp), INTENT(OUT) :: rac
        INTEGER :: j1, j2, j3, j4, j5, j6, j7, icount, numin, numax, kk, ki

        IF (.NOT. gamma) THEN
            CALL factt
            gamma = .true.
        END IF
        j1 = i + j + m
        j2 = k + l + m
        j3 = i + k + n
        j4 = j + l + n
        IF (2*MAX(i, j, m) > j1 .OR. MOD(j1, 2) /= 0 .OR. &
            2*MAX(k, l, m) > j2 .OR. MOD(j2, 2) /= 0 .OR. &
            2*MAX(i, k, n) > j3 .OR. MOD(j3, 2) /= 0 .OR. &
            2*MAX(j, l, n) > j4 .OR. MOD(j4, 2) /= 0) THEN
            rac = 0.0_wp
            RETURN
        END IF

        ! When any of the first four arguments is zero rac=1.
        IF (i == 0 .OR. j == 0 .OR. k == 0 .OR. l == 0) THEN
            rac = 1.0_wp
            RETURN
        END IF
        j1 = j1/2
        j2 = j2/2
        j3 = j3/2
        j4 = j4/2
        j5 = (i + j + k + l)/2
        j6 = (i + l + m + n)/2
        j7 = (j + k + m + n)/2
        numin = MAX(j1, j2, j3, j4) + 1
        numax = MIN(j5, j6, j7) + 1
        rac = 1.0_wp
        icount = 0
        IF (numin /= numax) THEN
            numin = numin + 1
            DO kk = numin, numax
                ki = numax - icount
                rac = 1.0_wp - (rac*REAL(ki*(j5 - ki + 2)*(j6 - ki + 2)* &
                                         (j7 - ki + 2), wp)/REAL((ki - 1 - j1)*(ki - 1 - j2)* &
                                                                 (ki - 1 - j3)*(ki - 1 - j4), wp))
                icount = icount + 1
            END DO
            numin = numin - 1
        END IF
        rac = rac * ((-1.0_wp)**(j5 + numin + 1)) *               &
            EXP((gam(numin+1) - gam(numin-j1) - gam(numin-j2) -   &
            gam(numin-j3) - gam(numin-j4) - gam(j5+2-numin) -     &
            gam(j6+2-numin) - gam(j7+2-numin)) + ((gam(j1+1-i) +  &
            gam(j1+1-j) + gam(j1+1-m) - gam(j1+2) + gam(j2+1-k) + &
            gam(j2+1-l) + gam(j2+1-m) - gam(j2+2) + gam(j3+1-i) + &
          gam(j3+1-k) + gam(j3+1-n) - gam(j3+2) + gam(j4+1-j) + &
          gam(j4+1-l) + gam(j4+1-n) - gam(j4+2)) / 2.0_wp))
        rac = rac*((m + 1)*(n + 1))**0.5_wp
    END SUBROUTINE cracah

!---------------------------------------------------------------------------

    SUBROUTINE factt
    ! Calculates the logs of factorials

        INTEGER          :: i
        REAL(wp)         :: x
        INTEGER          :: status

        ALLOCATE (gam(ngam), stat=status)

        IF (status /= 0) THEN
            PRINT *, 'factt: allocation error'
            STOP
        END IF

        gam(1:2) = 1.0_wp
        x = 2.0_wp
        DO i = 3, 25
            gam(i) = gam(i - 1)*x
            x = x + 1.0_wp
        END DO

        DO i = 1, 25
            gam(i) = LOG(gam(i))
        END DO

        x = 25.0_wp
        DO i = 26, ngam
            gam(i) = gam(i - 1) + LOG(x)
            x = x + 1.0_wp
        END DO

    END SUBROUTINE factt

!---------------------------------------------------------------------------

    SUBROUTINE dracah(i, j, k, l, m, n, rac)
    ! Calculate racah coefficients
    ! Arguments i,j,k,l,m,n should be twice their actual value

        INTEGER, INTENT(IN)    :: i, j, k, l, m, n
        REAL(WP), INTENT(OUT)  :: rac
        INTEGER :: j1, j2, j3, j4, j5, j6, j7, icount, numin, numax, kk, ki

        IF (.NOT. gamma) THEN
            CALL factt
            gamma = .true.
        END IF
        j1 = i + j + m
        j2 = k + l + m
        j3 = i + k + n
        j4 = j + l + n
        IF (2*MAX(i, j, m) > j1 .OR. MOD(j1, 2) /= 0 .OR. &
            2*MAX(k, l, m) > j2 .OR. MOD(j2, 2) /= 0 .OR. &
            2*MAX(i, k, n) > j3 .OR. MOD(j3, 2) /= 0 .OR. &
            2*MAX(j, l, n) > j4 .OR. MOD(j4, 2) /= 0) THEN
            rac = 0.0_wp
            RETURN
        END IF
        j1 = j1/2
        j2 = j2/2
        j3 = j3/2
        j4 = j4/2
        j5 = (i + j + k + l)/2
        j6 = (i + l + m + n)/2
        j7 = (j + k + m + n)/2
        numin = MAX(j1, j2, j3, j4) + 1
        numax = MIN(j5, j6, j7) + 1
        rac = 1.0_wp
        icount = 0
        IF (numin /= numax) THEN
            numin = numin + 1
            DO kk = numin, numax
                ki = numax - icount
                rac = 1.0_wp - (rac*REAL(ki*(j5 - ki + 2)*(j6 - ki + 2)* &
                                         (j7 - ki + 2), wp))/REAL((ki - 1 - j1)*(ki - 1 - j2)* &
                                                                  (ki - 1 - j3)*(ki - 1 - j4), wp)
                icount = icount + 1
            END DO
            numin = numin - 1
        END IF
        rac = rac * ((-1.0_wp)**(j5+numin+1)) *                    &
             EXP((gam(numin+1) - gam(numin-j1) -gam(numin-j2) -    &
             gam(numin-j3) - gam(numin-j4) - gam(j5+2-numin) -     &
             gam(j6+2-numin) - gam(j7+2-numin)) + ((gam(j1+1-i) +  &
             gam(j1+1-j) + gam(j1+1-m) - gam(j1+2) + gam(j2+1-k) + &
             gam(j2+1-l) + gam(j2+1-m) - gam(j2+2) + gam(j3+1-i) + &
             gam(j3+1-k) + gam(j3+1-n) - gam(j3+2) + gam(j4+1-j) + &
             gam(j4+1-l) + gam(j4+1-n) - gam(j4+2)) / 2.0_wp))

    END SUBROUTINE dracah

!---------------------------------------------------------------------------

    SUBROUTINE ractab
    ! Construct a table of racah coefficients for l, s as required by angcc

        REAL(wp)        :: asp(4), srac(4)
        INTEGER         :: max1, max2, status, sgn, ispim1, ispip1, ispim2
        INTEGER         :: maxa, maxb, maxc, ia, ib, ic, icup, iclo, ispi
        REAL(wp)        :: rm, rac

        max1 = MAX(lrang1, lrang3) - 1
        max2 = max1 + max1

        ! Form the table of spin racah coefficients
        ALLOCATE (spinw(4, isran2 + 3), stat=status)
        IF (status /= 0) THEN
            PRINT *, 'ractab: allocation error'
            STOP
        END IF
        sgn = 1
        DO ispi = 2, isran2 + 3
            sgn = -sgn
            ispim1 = ispi - 1
            ispip1 = ispi + 1
            ispim2 = ispi - 2
            CALL dracah(ispim1, ispim2, ispim2, ispim1, 1, 1, srac(1))
            CALL dracah(ispim1, ispim2, ispi, ispim1, 1, 1, srac(2))
            CALL dracah(ispim1, ispi, ispi, ispim1, 1, 1, srac(3))
            CALL dracah(ispim1, ispi, ispi, ispip1, 1, 1, srac(4))
            asp(1) = ispi
            asp(2) = -ispi
            asp(3) = ispi
            asp(4) = ((ispip1 + 1)*ispi)**0.5_wp
            spinw(:, ispi) = asp*srac*sgn
        END DO

        ! Special case of zero target spin
        CALL dracah(0, 1, 1, 0, 1, 1, rac)
        spinw(1:3, 1) = rac
        CALL dracah(0, 1, 1, 2, 1, 1, rac)
        spinw(4, 1) = 3**.5_wp*rac

        ! Construct simple table of rme
        ! for bbcup and bccup maxa=lrang1-1, maxb=3*maxa, maxc=2*maxa
        ! rm2 is INCONSISTENT here
        maxa = lrang1 - 1 ! ildim8
        maxb = 3*maxa   ! ildim8
        maxc = 2*maxa   ! ildim0
        ALLOCATE (rmetab(0:maxc, 0:maxa, 0:maxb), stat=status)
        IF (status /= 0) THEN
            PRINT *, 'ractab: allocation error'
            STOP
        END IF
        rmetab = 0.0_wp
        DO ia = 0, maxa
            DO ib = ia, maxb
                iclo = ABS(ib - ia)
                icup = MIN(ib + ia, maxc)
                DO ic = iclo, icup, 2
                    rm = rme(ia, ib, ic)
                    rmetab(ic, ia, ib) = rm
                    IF (ib /= ia .AND. ib <= maxa) rmetab(ic, ib, ia) = rm
                END DO
            END DO
        END DO

    END SUBROUTINE ractab

!---------------------------------------------------------------------------

    FUNCTION rme(l, lp, k)
    ! Evaluates the reduced matrix element (l//c(k)//lp)  -  see fano
    ! and racah, irreducible tensorial sets, chap. 14, p. 81

        REAL(wp)                :: rme
        INTEGER, INTENT(IN)     :: l, lp
        INTEGER, INTENT(IN)     :: k
        INTEGER                 :: i2g, ig, i1, i2, i3, imax
        REAL(wp)                :: al1, al2, alp1, alp2, ak1, ak2, el1, elp1
        REAL(wp)                :: qusqrt

        IF (k > (l + lp) .OR. k < ABS(l - lp)) THEN
!           IF (0 > 1) WRITE (fo,'(a,i3,a,i3,a,i3,a)') 'l =', l, &
            IF (0 > 1) PRINT *, 'l =', l, &
                ' lp =', lp, ' k =', k, ' rme set zero since angle does &
                &not match'
            rme = 0.0_wp
            RETURN
        END IF

        i2g = l + lp + k
        ig = i2g/2
        IF (i2g /= 2*ig) THEN
            rme = 0.0_wp
            RETURN
        END IF

        SELECT CASE (ig)
        CASE (0)
            rme = 1.0_wp
        CASE (:-1)
!           IF (0 > 1) WRITE (fo,'(a,i3,a,i3,a,i3,a)') 'l =', l, &
            IF (0 > 1) PRINT *, 'l =', l, &
                 ' lp =', lp, ' k =', k, ' rme set zero since angle does &
                 &not match'
            rme = 0.0_wp
        CASE DEFAULT
            i1 = ig - l
            i2 = ig - lp
            i3 = ig - k
            imax = MAX(ig + 1, i2g + 2, 2*i3 + 1, 2*i2 + 1, 2*i1 + 1)
            IF (imax > 500) THEN
                PRINT *, 'program stops in rme imaxf = ', imax
                STOP
            END IF
            al1 = gam(i1 + 1)
            al2 = gam(2*i1 + 1)
            alp1 = gam(i2 + 1)
            alp2 = gam(2*i2 + 1)
            ak1 = gam(i3 + 1)
            ak2 = gam(2*i3 + 1)
            el1 = 2*l + 1
            elp1 = 2*lp + 1
            qusqrt = LOG(el1) + LOG(elp1) + al2 + alp2 + ak2 - gam(i2g + 2)
            rme = EXP(0.5_wp*qusqrt + gam(ig + 1) - al1 - alp1 - ak1)
        END SELECT

    END FUNCTION rme

!> calculates a clebsch-gordan coefficient.
!> Condon and Shortley page 75 formula (3.14.5))
!> like cg below, but all inputs are doubled so that half integers
!> can be used. Not too confindent in this routine it's kind of a placeholder
!> for now.
  function dcg (j1, j2, j3, m1, m2, m3)
     real(wp)                :: dcg
     integer, intent(in)     :: j1, j2, j3
     integer, intent(in)     :: m1, m2, m3
     real(wp)                :: xnum, g
     integer                 :: a, b, c, d, e, numin, numax, j, n

     if (.NOT.gamma) then
        call factt
        gamma=.true.
     end if
     dcg = 0.0_wp
     if (ABS(m1) > j1 .or. ABS(m2) > j2 .or. ABS(m3) > j3) return
     if (m1+m2 /= m3) return
     if (2*MAX(j1,j2,j3) > j1+j2+j3) return
!
     xnum = (gam((-j1+j2+j3+2)/2) - gam((j2+m2+2)/2)) + &
          (gam((j1-j2+j3+2)/2) - gam((j1+m1+2)/2)) +    &
          (gam((j1+j2-j3+2)/2) - gam((j1-m1+2)/2)) +    &
          (gam((j3-m3+2)/2) - gam((j2-m2+2)/2)) +       &
          (LOG(REAL(j3+1,wp)) + gam((j3+m3+2)/2) - gam((j1+j2+j3+4)/2))
  !    print *,((-j1+j2+j3+2)/2),((j2+m2+2)/2),((j1-j2+j3+2)/2),((j1+m1+2)/2), &
  !        ((j1+j2-j3+2)/2),((j1-m1+2)/2),((j3-m3+2)/2),((j2-m2+2)/2), &
  !        (REAL(j3+1,wp)),((j3+m3+2)/2),((j1+j2+j3+4)/2)
     xnum = xnum / 2.0_wp
     xnum = EXP(xnum)
     a = -j1 + m1
     b = j2 - j1 + m3
     c = j2 + j3 + m1
     d = -j1 + j2 + j3
     e = j3 + m3
     numin = MAX(0,b)
     numax = MIN(c,d,e)
     j = j2 + m2

!    print *,'numin',numin,numax
     do n = numin, numax,2
        g = gam((n-a+2)/2) + gam((c-n+2)/2) - (gam((d-n+2)/2) +         &
             gam((e-n+2)/2) + gam((n-b+2)/2) + gam((n+2)/2))
 !       print *,((n-a+2)),((c-n+2)),((d-n+2)),((e-n+2)),((n-b+2)),((n+2))

        g = EXP(g)
        if (MOD((j+n)/2,2) /= 0) g = -g
        dcg = dcg + g
     end do
     dcg = dcg * xnum
   end function dcg

!---------------------------------------------------------------------------

    FUNCTION cg(j1, j2, j3, m1, m2, m3)
    ! Calculates a clebsch-gordan coefficient.
    ! Condon and Shortley page 75 formula (3.14.5))

        REAL(wp)                :: cg
        INTEGER, INTENT(IN)     :: j1, j2, j3
        INTEGER, INTENT(IN)     :: m1, m2, m3
        REAL(wp)                :: xnum, g
        INTEGER                 :: a, b, c, d, e, numin, numax, j, n

        IF (.NOT. gamma) THEN
            CALL factt
            gamma = .true.
        END IF
        cg = 0.0_wp
        IF (ABS(m1) > j1 .OR. ABS(m2) > j2 .OR. ABS(m3) > j3) RETURN
        IF (m1 + m2 /= m3) RETURN
        IF (2*MAX(j1, j2, j3) > j1 + j2 + j3) RETURN

        xnum = (gam(-j1 + j2 + j3 + 1) - gam(j2 + m2 + 1)) + &
               (gam(j1 - j2 + j3 + 1) - gam(j1 + m1 + 1)) + &
               (gam(j1 + j2 - j3 + 1) - gam(j1 - m1 + 1)) + &
               (gam(j3 - m3 + 1) - gam(j2 - m2 + 1)) + &
               (LOG(REAL(2*j3 + 1, wp)) + gam(j3 + m3 + 1) - gam(j1 + j2 + j3 + 2))
        xnum = xnum/2.0_wp
        xnum = EXP(xnum)
        a = -j1 + m1
        b = j2 - j1 + m3
        c = j2 + j3 + m1
        d = -j1 + j2 + j3
        e = j3 + m3
        numin = MAX(0, b)
        numax = MIN(c, d, e)
        j = j2 + m2

        DO n = numin, numax
            g = gam(n - a + 1) + gam(c - n + 1) - (gam(d - n + 1) + &
                                                   gam(e - n + 1) + gam(n - b + 1) + gam(n + 1))
            g = EXP(g)
            IF (MOD(j + n, 2) /= 0) g = -g
            cg = cg + g
        END DO
        cg = cg*xnum

    END FUNCTION cg

!---------------------------------------------------------------------------

!   FUNCTION cg (j1, j2, j3, m1, m2, m3)
    ! Calculates a clebsch-gordan coefficient.
    ! Condon and Shortley page 75 formula (3.14.5))

!       REAL(wp)                :: cg
!       INTEGER, INTENT(IN)     :: j1, j2, j3
!       INTEGER, INTENT(IN)     :: m1, m2, m3
!       REAL(wp)                :: xnum, g
!       INTEGER                 :: a, b, c, d, e, numin, numax, j, n

!       IF (.NOT.allocated(gammas)) CALL shriek (500)
!       cg = 0.0_wp
!       IF (ABS(m1) > j1 .OR. ABS(m2) > j2 .OR. ABS(m3) > j3) RETURN
!       IF (m1+m2 /= m3) RETURN
!       IF (2*MAX(j1,j2,j3) > j1+j2+j3) RETURN

!       xnum = (gammas(-j1+j2+j3+1) / gammas(j2+m2+1)) * &
!            (gammas(j1-j2+j3+1) / gammas(j1+m1+1)) *    &
!            (gammas(j1+j2-j3+1) / gammas(j1-m1+1)) *    &
!            (gammas(j3-m3+1) / gammas(j2-m2+1)) *       &
!            (REAL(2*j3+1,wp) * gammas(j3+m3+1) / gammas(j1+j2+j3+2))
!       xnum = SQRT(xnum)
!       a = -j1 + m1
!       b = j2 - j1 + m3
!       c = j2 + j3 + m1
!       d = -j1 + j2 + j3
!       e = j3 + m3
!       numin = MAX(0,b)
!       numax = MIN(c,d,e)
!       j = j2 + m2

!       DO n = numin, numax
!           g = gammas(n-a+1) * gammas(c-n+1) / (gammas(d-n+1) *         &
!                gammas(e-n+1) * gammas(n-b+1) * gammas(n+1))
!           IF (MOD(j+n,2) /= 0) g = -g
!           cg = cg + g
!       END DO
!       cg = cg * xnum
!   END FUNCTION cg

!---------------------------------------------------------------------------

    SUBROUTINE shriek(nfact)
    ! Evaluates factorials from 1 to nfact-1
    ! gammas(i+1) = factorial i

        INTEGER, INTENT(IN)      :: nfact
        REAL(wp)                 :: a
        INTEGER                  :: i, status

        ALLOCATE (gammas(nfact), stat=status)
        IF (status /= 0) THEN
            PRINT *, 'shriek: allocation error = ', status
            STOP
        END IF
        gammas(1) = 1.0_wp
        IF (nfact < 2) RETURN
        DO i = 2, nfact
            gammas(i) = REAL(i - 1, wp)*gammas(i - 1)
            IF (gammas(i) > HUGE(a)/1000.0_wp) THEN
                PRINT *, 'shriek: factorial limit = ', i
                EXIT
            END IF
        END DO
    END SUBROUTINE shriek


    !> \brief   Associated Legendre polynomial for m >= 0.
    !> \authors G Armstrong, D Clarke, J Benda
    !> \date    2018 - 2019
    !>
    !> Calculate the associated Legendre polynomial \f$ P_l^m(x) \f$ for \f$ m \ge 0 \f$
    !> using the recurrence formula
    !> \f[
    !>      (l - m + 1) P_{l+1}^m(x) = (2l + 1) x P_l^m(x) - (l + m) P_{l-1}^x
    !> \f]
    !> started with the value of \f$ P_m^m \f$ obtained from
    !> \f[
    !>      P_{l+1}^{l+1} = -(2l + 1) \sqrt{1 - x^2} P_l^l \,.
    !> \f]
    !>
    !> \param[in] l          Angular momentum.
    !> \param[in] m          Angular momentum projection (m >= 0).
    !> \param[in] cos_theta  Argument of the polynomial.
    !>
    REAL(wp) FUNCTION associated_legendre (l, m, cos_theta) RESULT (Plm)

        INTEGER,  INTENT(IN)  :: l, m
        REAL(wp), INTENT(IN)  :: cos_theta
        INTEGER               :: k
        REAL(wp)              :: sin_theta, Plm_1, Plm_2

        sin_theta = SQRT(MAX(0._wp, 1 - cos_theta**2))
        Plm_1 = 0
        Plm_2 = 0

        ! P_0^0
        Plm = 1

        ! P_m^m
        DO k = 0, m - 1
            Plm = -(2*k + 1) * sin_theta * Plm
        END DO

        ! P_l^m
        DO k = m, l - 1
            Plm_2 = Plm_1
            Plm_1 = Plm
            Plm = ((2*k + 1) * cos_theta * Plm_1 - (k + m) * Plm_2) / (k - m + 1)
        END DO

    END FUNCTION associated_legendre


    !> \brief  Complex spherical harmonic \f$ Y_l^m \f$
    !> \author J Benda
    !> \date   2019
    !>
    !> Evaluates the complex spherical harmonics.
    !> Includes Condon-Shortley (but not Fano-Racah) phase factor.
    !>
    !> \param l  Angular momentum.
    !> \param m  Angular momentum projection.
    !> \param e  Unit Cartesian direction vector.
    !>
    COMPLEX(wp) FUNCTION complex_spherical_harmonic (l, m, e) RESULT(Ylm)

        USE global_data, ONLY: im

        INTEGER,  INTENT(IN) :: l, m
        REAL(wp), INTENT(IN) :: e(3)

        REAL(wp) :: c, s

        ! calculate real and imaginary part
        CALL spherical_harmonic_components(l, m, e, c, s)

        ! combine them into final result & add Condon-Shortley phase factor
        Ylm = MERGE(c + im*s, c - im*s, m >= 0) * (-1)**m

    END FUNCTION complex_spherical_harmonic


    !> \brief  Real spherical harmonic \f$ Y_{lm} \f$
    !> \author J Benda
    !> \date   2019
    !>
    !> Evaluates the real spherical harmonics.
    !> Includes Condon-Shortley (but not Fano-Racah) phase factor.
    !>
    !> \param l  Angular momentum.
    !> \param m  Angular momentum projection.
    !> \param e  Unit Cartesian direction vector.
    !>
    REAL(wp) FUNCTION real_spherical_harmonic (l, m, e) RESULT(Ylm)

        INTEGER,  INTENT(IN) :: l, m
        REAL(wp), INTENT(IN) :: e(3)

        REAL(wp) :: c, s

        ! calculate real and imaginary part
        CALL spherical_harmonic_components(l, ABS(m), e, c, s)

        ! combine them into final result & add Condon-Shortley phase factor
        Ylm = MERGE(1.0_wp, SQRT(2.0_wp), m == 0) * MERGE(c, s, m >= 0) * (-1)**m

    END FUNCTION real_spherical_harmonic


    !> \brief  Spherical harmonic \f$ Y_{lm} \f$
    !> \author J Benda
    !> \date   2019
    !>
    !> Calculates real and imaginary part of the spherical harmonics.
    !> Does not include Condon-Shortley phase factor.
    !>
    !> \param l  Angular momentum.
    !> \param m  Angular momentum projection.
    !> \param e  Unit Cartesian direction vector.
    !> \param c  Real part of Ylm.
    !> \param s  Imaginary part of Ylm.
    !>
    SUBROUTINE spherical_harmonic_components (l, m, e, c, s)

        USE global_data, ONLY: pi

        INTEGER,  INTENT(IN)  :: l, m
        REAL(wp), INTENT(IN)  :: e(3)
        REAL(wp), INTENT(OUT) :: c, s

        INTEGER, PARAMETER :: csign(0:3) = (/ 1, 0, -1, 0 /)    ! cos(k*pi/2), k = 0,1,2,3
        INTEGER, PARAMETER :: ssign(0:3) = (/ 0, 1, 0, -1 /)    ! sin(k*pi/2), k = 0,1,2,3

        REAL(wp) :: Plm, b, x, y, z
        INTEGER :: k, n

        b = 1; c = 0; s = 0; n = ABS(m); z = e(3)
        x = e(1) / SQRT(MAX(1 - z*z, 1e-10_wp))
        y = e(2) / SQRT(MAX(1 - z*z, 1e-10_wp))

        ! evaluate the squared normalization factor of Ylm
        Plm = (2*l + 1) / (4 * pi)
        DO k = l - n + 1, l + n
            Plm = Plm / k
        END DO

        ! multiply by the associated Legendre polynomial
        Plm = SQRT(Plm) * associated_legendre(l, n, z)

        ! correct the sign of Plm for m < 0
        IF (m < 0) THEN
            Plm = Plm * (-1)**m
        END IF

        ! evaluate the phase factor exp(i m phi)
        DO k = 0, n
            c = c + b * x**k * y**(n-k) * csign(MOD(n - k, 4))
            s = s + b * x**k * y**(n-k) * ssign(MOD(n - k, 4))
            b = b * (n - k) / (k + 1)
        END DO

        ! multiply polynomial by components of the phase factor
        c = Plm * c
        s = Plm * s

    END SUBROUTINE spherical_harmonic_components

END MODULE angular_momentum
