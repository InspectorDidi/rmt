! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles all routines relevant to the arnoldi propagator- setting up and
!> sharing the reduced Hamiltonian, calling routines to step forward in time in
!> both inner and outer regions etc.

MODULE krylov_method

    USE precisn,               ONLY: wp
    USE distribute_hd_blocks2, ONLY: numrows, rowbeg, rowend
    USE readhd,                ONLY: max_L_block_size
    USE rmt_assert,            ONLY: assert
    USE initial_conditions,    ONLY: numsols => no_of_field_confs

    IMPLICIT NONE

    INTEGER, SAVE               :: ord
    REAL(wp), ALLOCATABLE       :: tot_beta(:)
    COMPLEX(wp), ALLOCATABLE    :: qvec(:, :, :), wfa(:, :), wfar2(:, :)
    COMPLEX(wp), ALLOCATABLE    :: kvec(:, :, :)
    COMPLEX(wp), ALLOCATABLE    :: redH(:, :, :), redH2(:, :, :), exphvec(:, :)
    COMPLEX(wp), SAVE           :: t_step, t_step2
    CHARACTER(LEN=3), SAVE      :: krymeth
    CHARACTER(LEN=4), SAVE      :: etech

    ! QC_ham serves the purpose of reducing output messages only
    COMPLEX(wp), ALLOCATABLE    :: powers_of_H_array_in(:, :, :), QC_ham(:, :)
    COMPLEX(wp), ALLOCATABLE    :: i_to_power(:)
    REAL(wp), ALLOCATABLE       :: minus1_to_power(:)
    COMPLEX(wp), ALLOCATABLE    :: stored_c_vals(:, :, :, :)

    PRIVATE transform_vals_Q_to_C
    PRIVATE reset_powers_of_H_array_in
    PRIVATE reset_stored_c_vals
    PRIVATE get_C_timederiv

    PUBLIC setup_arnoldi_data
    PUBLIC arnoldi_propagate_inner
    PUBLIC update_psi_inner_on_grid
    PUBLIC update_psi_inner
    PUBLIC init_krylov_module
    PUBLIC deallocate_krylov_module

CONTAINS

    SUBROUTINE init_krylov_module(propagation_order, psi_inner)

        USE serial_matrix_algebra, ONLY: init_serial_matrix_algebra

        INTEGER, INTENT(IN)     :: propagation_order
        COMPLEX(wp), INTENT(IN) :: psi_inner(:, :)

        CALL init_serial_matrix_algebra(propagation_order)
        CALL update_wfa(psi_inner)
        CALL allocate_powers_of_H_array_in(propagation_order)
        CALL init_powers_of_i_in(propagation_order)

    END SUBROUTINE init_krylov_module

!-----------------------------------------------------------------------

    SUBROUTINE deallocate_krylov_module

        USE serial_matrix_algebra, ONLY: dealloc_serial_matrix_algebra

        CALL dealloc_serial_matrix_algebra
        CALL deallocate_powers_of_H_array_in
        CALL deallocate_powers_of_i_in
        CALL deallocate_arnoldi_data

    END SUBROUTINE deallocate_krylov_module

!-----------------------------------------------------------------------

    SUBROUTINE setup_arnoldi_data(krydim, krytech, exptech, t, krylov_hdt_desired)

        USE live_communications, ONLY: setup_block_links

        IMPLICIT NONE

        INTEGER, INTENT(IN)          :: krydim
        INTEGER                      :: err
        REAL(wp), INTENT(IN)         :: t
        CHARACTER(LEN=3), INTENT(IN) :: krytech
        CHARACTER(LEN=4), INTENT(IN) :: exptech
        LOGICAL, INTENT(IN)          :: krylov_hdt_desired

        krymeth = krytech         !arnoldi or lanczos
        ord = krydim              !dimension of krylov subspace
        etech = exptech           !probably redundant

        ALLOCATE (wfa(max_L_block_size, numsols), qvec(numrows, krydim + 2, numsols), wfar2(max_L_block_size, numsols), &
                  kvec(numrows, numsols, 0:ord), QC_ham(ord + 1, ord + 1), tot_beta(numsols), stat=err)

        CALL assert(err .EQ. 0, 'allocation error in Setup_Arnoldi_Data')

        wfar2 = (0.0_wp, 0.0_wp)
        kvec = (0.0_wp, 0.0_wp)
        qvec(:, :, :) = (0.0_wp, 0.0_wp)

!       t_step =(1.0_wp,0.0_wp)*t   !t_step = dt
        IF (krylov_hdt_desired) THEN
            ! Krylov subspace spans (H dt), (H dt)^2, ..., (H dt)^n
            t_step = (1.0_wp, 0.0_wp)     !t_step  = 1
            t_step2 = (1.0_wp, 0.0_wp)*t   !t_step2 = dt
        ELSE
            ! Krylov subspace spans H, H^2, ..., H^n
            t_step = (1.0_wp, 0.0_wp)*t   !t_step  = dt
            t_step2 = (1.0_wp, 0.0_wp)     !t_step2 = 1
        END IF

        ! ACB: All cores in inner region now need to know what the Del_Rcv_** are, so all cores call Setup_Block_Links
        !      was previously just block masters

        ! DDAC: Careful! Not all inner region cores have access to data required for this subroutine.
        !       Only block masters.

        CALL setup_block_links

    END SUBROUTINE setup_arnoldi_data

!-----------------------------------------------------------------------

    SUBROUTINE arnoldi_propagate_inner(nfdm, field, istep, i_am_inner_master, &
                                       expec_inner)

        USE initial_conditions, ONLY: dipole_output_desired

        IMPLICIT NONE

        REAL(wp), INTENT(IN)     :: field(3, numsols)
        INTEGER, INTENT(IN)      :: istep, nfdm
        LOGICAL, INTENT(IN)      :: i_am_inner_master
        COMPLEX(wp), INTENT(OUT) :: expec_inner(1:numsols,1:3,1:2)

        IF (dipole_output_desired) THEN
            CALL hhg_inner(wfa(rowbeg:rowend, 1:numsols), expec_inner) ! calculate expec_Z_inner for old wf
        END IF

        CALL inhomogeneous_arnoldi_algorithm(field, ord, wfa, &
                                             istep, wfar2, i_am_inner_master, nfdm)

        wfa(1:max_L_block_size, 1:numsols) = wfar2(1:max_L_block_size, 1:numsols)   !updated wave function coefficients

    END SUBROUTINE arnoldi_propagate_inner

!-----------------------------------------------------------------------

    SUBROUTINE inhomogeneous_arnoldi_algorithm(field, m, v0, istep, &
                                               w2, i_am_inner_master, nfdm)

        USE calculation_parameters,   ONLY: calc_psi_derivs_at_bndry_by, &
                                            using_h_inner
        USE initial_conditions,       ONLY: timings_desired
        USE inner_to_outer_interface, ONLY: get_psi_at_inner_fd_pts, &
                                            send_psi_at_inner_fd_pts
        USE live_communications,      ONLY: parallel_matrix_vector_multiply, &
                                            parallel_matrix_vector_multiply_no_field, &
                                            gram_schmidt_orthog_lanczos, &
                                            sum_and_distribute_total_beta, &
                                            gather_c_time_derivs
        USE outer_to_inner_interface, ONLY: get_fderivs_and_project
        USE serial_matrix_algebra,    ONLY: augment_reduced_hamiltonian, &
                                            exponentiate_reduced_ham_diag, &
                                            exponentiate_reduced_ham_pade
        USE wall_clock,               ONLY: update_start_iter, &
                                            update_end_iter,&
                                            update_start_arnoldi,&
                                            update_start_gram,&
                                            update_start_diag,&
                                            update_start_trans

        IMPLICIT NONE

        INTEGER, INTENT(IN)        :: m, istep, nfdm
        REAL(wp), INTENT(IN)       :: field(3, numsols)
        COMPLEX(wp), INTENT(IN)    :: v0(max_L_block_size, numsols)
        COMPLEX(wp), INTENT(OUT)   :: w2(max_L_block_size, numsols)
        LOGICAL, INTENT(IN)        :: i_am_inner_master

        INTEGER                    :: j, k1, mx, isol
        REAL(wp)                   :: p1
        COMPLEX(wp)                :: pvec(numrows, numsols),  &
                                      qvecj(numrows, numsols), &
                                      w(numrows), &
                                      v(numrows, numsols), c_vals(numrows), &
                                      C_time_deriv_vec(numrows), &
                                      C_time_deriv_vec_Lblock(max_L_block_size)


        ! PARAMETERS AND POINTERS FOR WORKSPACE (PADE ROUTINE)
        CALL reset_stored_C_vals(ord)

        DO k1 = 0, ord               !loop over outer region Fderivs

            IF (timings_desired) THEN
                CALL update_start_iter
            END IF

            IF (k1 == 0) THEN

                CALL sum_and_distribute_total_beta(v0, rowbeg, rowend, tot_beta)

                DO isol = 1, numsols
                    p1 = 1.0_wp/tot_beta(isol)                  !normalisation factor
                    qvec(1:numrows, 1, isol) = p1 * v0(rowbeg:rowend, isol)
                END DO

            ELSE IF (k1 > 0) THEN

                CALL get_fderivs_and_project(k1, v)
                CALL sum_and_distribute_total_beta(v, 1, numrows, tot_beta)

                DO isol = 1, numsols
                    p1 = 1.0_wp/tot_beta(isol)                  !normalisation factor
                    qvec(1:numrows, 1, isol) = p1 * v(1:numrows, isol)
                END DO

                ! NOTE - w is needed for the Psi_Derivs at the boundary
                ! Normalisation brought forward so that other processes are not
                ! needlessly waiting for process Inner_Master, which may be dealing
                ! with smaller matrices...

                ! Additional work to do if use H_inner to get the time derivatives of
                ! the wavefunction at the fictitious FD points in the inner region
                IF (calc_psi_derivs_at_bndry_by .EQ. using_H_inner) THEN

                    ! Calculate d^{k1}C/dt^{k1} and use to calculate d^{k1}Psi/dt^{k1} at
                    ! the two grid points b-dr and b-2*dr just inside the R-matrix boundary
                    ! Step 1. Calculate d^{k1}C/dt^{k1} on this core
                    CALL get_C_timederiv(k1, w, C_time_deriv_vec)

                    ! Step 2. Gather all parts of d^{k1}C/dt^{k1} for this sym L block
                    ! Gather will be done onto master PE for this sym L block
                    CALL gather_C_time_derivs(C_time_deriv_vec, C_time_deriv_vec_Lblock)

                    ! Step 3. Update wfa to contain this vector
                    CALL update_wfa(C_time_deriv_vec_Lblock)
                    ! Step 4. Calculate Psi at the FD points
                    CALL get_psi_at_inner_fd_pts(nfdm, wfa)
                    ! Step 5. Send Psi at the inner FD points to the outer region
                    CALL send_psi_at_inner_fd_pts(nfdm)

                END IF

            END IF

            mx = m + k1 + 1

            CALL allocate_reduced_ham_and_vec(mx)

            IF (timings_desired) THEN
                CALL update_start_arnoldi
            END IF

            !-----------------------------------------------------
            ! ARNOLDI/LANCZOS LOOP (ALGORITHM) STARTS HERE
            !-----------------------------------------------------

            IF (krymeth == 'arn') THEN

                CALL assert(.false., 'Arnoldi method has not been fully implemented yet')

            ELSE IF (krymeth == 'lan') THEN

                DO j = 1, m + 1

                    qvecj = qvec(1:numrows, j, 1:numsols)

                    IF (ANY(field /= 0.0_wp)) THEN
                        CALL parallel_matrix_vector_multiply(field, qvecj, pvec, t_step2)
                    ELSE
                        CALL parallel_matrix_vector_multiply_no_field(qvecj, pvec, t_step2)
                    END IF

                    IF (timings_desired) THEN
                        CALL update_start_gram
                    END IF

                    CALL gram_schmidt_orthog_lanczos(j, mx, qvecj, pvec, qvec, redH)

                    IF (timings_desired) THEN
                        CALL update_start_trans
                    END IF

                    qvec(1:numrows, j + 1, 1:numsols) = pvec ! pvec has been gram-schmidt-orthogonalised

                    ! Additional work to do if use H_inner to get the time derivatives of
                    ! the wavefunction at the fictitious FD points in the inner region
                    IF (calc_psi_derivs_at_bndry_by .EQ. using_H_inner) THEN

                        ! Reverse transform Q,     H Q,     H^2 Q,     H^3 Q, ...,     H^m Q and times by (-i)^n
                        !           to get  C, (-iH) C, (-iH)^2 C, (-iH)^3 C, ..., (-iH)^m C (or F or F deriv)
                        ! These are needed to calculate C, dC/dt, d^2C/dt^2, ..., d^mC/dt^m
                        ! and hence to calculate Psi at grid points b-dr and b-2*dr
!                       CALL transform_vals_Q_to_C(redH(1:ord+1,1:ord+1), pvecout, tot_beta, c_vals, j)

                        DO isol = 1, numsols
                            QC_ham = redH(1:ord + 1, 1:ord + 1, isol) ! FIXME for numsols > 1
                            CALL transform_vals_Q_to_C(QC_ham, qvecj(:, isol), tot_beta(isol), c_vals, j)  ! FIXME for numsols > 1
                            ! Store the (-iH)^n C (or F deriv) values
                            stored_c_vals(:, j, k1, isol) = c_vals
                        END DO

                    END IF

                    IF (timings_desired) THEN
                        CALL update_start_diag
                    END IF

                END DO

            ELSE

                IF (timings_desired) THEN
                    CALL update_start_diag
                END IF

            END IF

            !-----------------------------------------------------
            !AUGMENT REDUCED H AND MULTIPLY BY -i
            !-----------------------------------------------------

            DO isol = 1, numsols
                CALL augment_reduced_hamiltonian(k1, m, mx, redH(:, :, isol), redH2(:, :, isol))

                IF (k1 == 0) THEN
                    CALL exponentiate_reduced_ham_diag(istep, mx, t_step, redH2(:, :, isol), exphvec(:, isol), i_am_inner_master)
                ELSE ! k1 > 0
                    CALL exponentiate_reduced_ham_pade(m, mx, t_step, redH2(:, :, isol), exphvec(:, isol))
                END IF
            END DO

            CALL kryspace_to_Nspace(m, tot_beta, exphvec(1:m, 1:numsols), kvec(1:numrows, 1:numsols, k1))
            CALL deallocate_reduced_ham_and_vec

            IF (timings_desired) THEN
                CALL update_end_iter
            END IF

        END DO         !loop over outer Fderivs

        CALL sum_orders(w2)

    END SUBROUTINE inhomogeneous_arnoldi_algorithm

!-----------------------------------------------------------------------

    SUBROUTINE hhg_inner(vecin, val)

        USE global_data, ONLY: one
        USE initial_conditions,  ONLY: dipole_velocity_output,      &
                                       dipole_dimensions_desired,   &
                                       length_guage_id,             &
                                       velocity_guage_id
        USE live_communications, ONLY: parallel_matrix_vector_multiply,&
                                       psi_Z_psi_multiplication_inner

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN) :: vecin(rowbeg:rowend, 1:numsols) ! psi_inner
        COMPLEX(wp), INTENT(OUT):: val(numsols,1:3,1:2) ! expec_inner
        COMPLEX(wp)             :: psi_inner(numrows,numsols,3,2)
        REAL(wp)  :: factors_axis(3, numsols)
        INTEGER :: Dimension_Counter

        DO Dimension_Counter=1,3
            IF (dipole_dimensions_desired(Dimension_Counter)) THEN

                factors_axis = 0
                factors_axis(Dimension_Counter, :) = -1

                CALL parallel_matrix_vector_multiply(factors_axis, vecin(:,:),             &
                                                     psi_inner(:,:,Dimension_Counter,length_guage_id), one, &
                                                     dipole_velocity_output, psi_inner(:,:,Dimension_Counter,velocity_guage_id))

                CALL psi_Z_psi_multiplication_inner(vecin, psi_inner(:,:,Dimension_Counter,length_guage_id),&
                                                    val(:,Dimension_Counter,length_guage_id))

                IF (dipole_velocity_output) THEN
                    CALL psi_Z_psi_multiplication_inner(vecin(:,:),                               &
                                                        psi_inner(:,:,Dimension_Counter,velocity_guage_id),   &
                                                        val(:,Dimension_Counter,velocity_guage_id))
                ELSE
                    val(:,Dimension_Counter,velocity_guage_id) = (0.0_wp, 0.0_wp)
                END IF
            ELSE
                val(:,Dimension_Counter,length_guage_id)= (0.0_wp, 0.0_wp)
                val(:,Dimension_Counter,velocity_guage_id)= (0.0_wp, 0.0_wp)
            END IF
        END DO

    END SUBROUTINE hhg_inner

!-----------------------------------------------------------------------
! SUM KVECS AND SEND TO RELEVANT PROCESSES
!-----------------------------------------------------------------------

    SUBROUTINE sum_orders(vecout)

        USE live_communications, ONLY: order0_pes_sum_vecs

        IMPLICIT NONE

        COMPLEX(wp), INTENT(OUT)    :: vecout(max_L_block_size, numsols)
        COMPLEX(wp)                 :: vec1(numrows, numsols)

        ! vecout initialised in subroutine to be called
        vec1(1:numrows, 1:numsols) = SUM(kvec(1:numrows, 1:numsols, 0:ord), dim = 3)

        CALL order0_pes_sum_vecs(vec1, vecout)

    END SUBROUTINE sum_orders

!-----------------------------------------------------------------------

    SUBROUTINE kryspace_To_Nspace(m, prefac, vecin, vecout)

        USE global_data,        ONLY: zero

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: m
        REAL(wp), INTENT(IN)        :: prefac(numsols)
        COMPLEX(wp), INTENT(IN)     :: vecin(m, numsols)
        COMPLEX(wp), INTENT(OUT)    :: vecout(numrows, numsols)
        COMPLEX(wp)                 :: hij
        INTEGER                     :: isol

        IF (numrows == 0) RETURN

        DO isol = 1, numsols
            hij = prefac(isol)
            CALL ZGEMV('n', numrows, m, hij, qvec(:, :, isol), numrows, vecin(:, isol), 1, zero, vecout(:, isol), 1)
        END DO

    END SUBROUTINE kryspace_To_Nspace

!-----------------------------------------------------------------------

    SUBROUTINE allocate_reduced_ham_and_vec(mx)

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: mx
        INTEGER                     :: err

        ALLOCATE (redH(mx, mx, numsols), redH2(mx, mx, numsols), exphvec(mx, numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with redH')

        redH = (0.0_wp, 0.0_wp)
        redH2 = (0.0_wp, 0.0_wp)

    END SUBROUTINE allocate_reduced_ham_and_vec

!-----------------------------------------------------------------------

    SUBROUTINE deallocate_reduced_ham_and_vec

        IMPLICIT NONE

        INTEGER  :: err

        DEALLOCATE (redH, redH2, exphvec, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with redH')

    END SUBROUTINE deallocate_reduced_ham_and_vec

!-----------------------------------------------------------------------

    SUBROUTINE deallocate_arnoldi_data

        IMPLICIT NONE

        INTEGER :: err

        DEALLOCATE (qvec, wfa, wfar2, kvec, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with Arnoldi data')

    END SUBROUTINE deallocate_arnoldi_data

!-----------------------------------------------------------------------

    SUBROUTINE update_psi_inner_on_grid(wv, nfdm, number_channels, numsols)

        USE inner_to_outer_interface, ONLY: get_psi_at_inner_fd_pts, &
                                            psi_at_inner_fd_pts

        INTEGER, INTENT(IN)      :: nfdm, number_channels, numsols
        COMPLEX(wp), INTENT(OUT) :: wv(nfdm, number_channels, numsols)

        CALL get_psi_at_inner_fd_pts(nfdm, wfa)

        wv = psi_at_inner_fd_pts

    END SUBROUTINE update_psi_inner_on_grid

!-----------------------------------------------------------------------

    SUBROUTINE update_wfa(psi_inner)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN) :: psi_inner(max_L_block_size, numsols)

        wfa = psi_inner

    END SUBROUTINE update_wfa

!-----------------------------------------------------------------------

    SUBROUTINE update_psi_inner(psi_inner)

        COMPLEX(wp), INTENT(OUT) :: psi_inner(max_L_block_size, numsols)

        psi_inner = wfa

    END SUBROUTINE update_psi_inner

!-----------------------------------------------------------------------

    SUBROUTINE transform_vals_Q_to_C(kry_h, q_vals, psi_norm, c_vals, order)

        COMPLEX(wp), INTENT(IN)    :: kry_h(1:ord + 1, 1:ord + 1)
        COMPLEX(wp), INTENT(IN)    :: q_vals(numrows)
        REAL(wp), INTENT(IN)       :: psi_norm
        COMPLEX(wp), INTENT(OUT)   :: c_vals(numrows)
        INTEGER, INTENT(IN)        :: order

        INTEGER :: diagonal_index, row_index, col_index, j

        ! Need to transform  H*X(:,:,m) into  H^m C
        ! Set up a matrix of values of H^j X(i)
        ! The matrix will be upper left triangular
        ! If we save these values at each order then they can be re-used for calculating
        ! the higher order H^n X values
        ! Use relation H^j X(i) = h(i+1,i)H^(j-1)X(i+1) + h(i,i)H^(j-1)X(i) + h(i-1,i)H^(j-1)X(i-1)
        ! Fill the array diagonally
        !     ie for order m, save X(m), H X(m-1), H^2 X(m-2), ...
        ! The first row contains X(1), H X(1), H^2 X(1), ... so is directly
        ! related to what we need to pass to the outer region:
        ! C(:,:,m) =  (-Im)^m * H^m C = (-Im)^m * H^m X(1) * Psi_Size --- WAVE EQN

        !$OMP PARALLEL DO PRIVATE(j, row_index, col_index, diagonal_index)
        DO j = 1, numrows

            powers_of_H_array_in(order, 1, j) = q_vals(j)
            DO diagonal_index = 2, order - 1
                row_index = order - diagonal_index + 1
                col_index = diagonal_index
                powers_of_H_array_in(row_index, col_index, j) = &
                    kry_h(row_index + 1, row_index)*powers_of_H_array_in(row_index + 1, col_index - 1, j) + &
                    kry_h(row_index, row_index)*powers_of_H_array_in(row_index, col_index - 1, j) + &
                    kry_h(row_index - 1, row_index)*powers_of_H_array_in(row_index - 1, col_index - 1, j)
            END DO
            ! And now do first row
            IF (order .GT. 1) THEN
                row_index = 1
                col_index = order
                powers_of_H_array_in(row_index, col_index, j) = &
                    kry_h(row_index + 1, row_index)*powers_of_H_array_in(row_index + 1, col_index - 1, j) + &
                    kry_h(row_index, row_index)*powers_of_H_array_in(row_index, col_index - 1, j)
            END IF

            c_vals(j) = minus1_to_power(order - 1)*i_to_power(order - 1)*Psi_Norm* &
                        powers_of_H_array_in(1, order, j)

        END DO

    END SUBROUTINE transform_vals_Q_to_C

!-----------------------------------------------------------------------

    SUBROUTINE allocate_powers_of_H_array_in(propagation_order)

        INTEGER, INTENT(IN) :: propagation_order
        INTEGER             :: err

        ALLOCATE (powers_of_H_array_in(1:propagation_order + 1, 1:propagation_order + 1, numrows), &
                  stored_c_vals(numrows, 1:propagation_order + 1, 0:propagation_order, 1:numsols), &
                  stat=err)
        CALL assert(err .EQ. 0, 'allocation error with powers_of_H_array_in')

    END SUBROUTINE allocate_powers_of_H_array_in

!---------------------------------------------------------------------

    SUBROUTINE init_powers_of_i_in(propagation_order)

        USE global_data, ONLY: im

        INTEGER, INTENT(IN) :: propagation_order
        INTEGER             :: err, m

        ALLOCATE (i_to_power(0:propagation_order), minus1_to_power(0:propagation_order), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with i_to_power')

        DO m = 0, propagation_order
            i_to_power(m) = im**m
            minus1_to_power(m) = (-1.0_wp)**m
        END DO

    END SUBROUTINE init_powers_of_i_in

!---------------------------------------------------------------------

    SUBROUTINE deallocate_powers_of_h_array_in

        INTEGER :: err

        DEALLOCATE (powers_of_H_array_in, stored_c_vals, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with powers_of_H_array_in')

    END SUBROUTINE deallocate_powers_of_H_array_in

!---------------------------------------------------------------------

    SUBROUTINE deallocate_Powers_of_i_in

        INTEGER :: err

        DEALLOCATE (i_to_power, minus1_to_power, stat=err)
        CALL assert(err .EQ. 0, 'DEALLOCATION error i_to_power inner region')

    END SUBROUTINE deallocate_Powers_of_i_in

!---------------------------------------------------------------------

    SUBROUTINE reset_powers_of_H_array_in

        USE global_data, ONLY: zero

        powers_of_H_array_in = zero

    END SUBROUTINE reset_powers_of_H_array_in

!---------------------------------------------------------------------

    SUBROUTINE reset_stored_c_vals(propagation_order)

        USE global_data, ONLY: zero

        INTEGER, INTENT(IN) :: propagation_order
        INTEGER :: i, j, k, isol

        !$OMP PARALLEL DO PRIVATE(i, j, k, isol) COLLAPSE(4)
        DO isol = 1, numsols
            DO k = 0, propagation_order
                DO j = 1, propagation_order + 1
                    DO i = 1, numrows
                        stored_c_vals(i, j, k, isol) = zero
                    END DO
                END DO
            END DO
        END DO

    END SUBROUTINE reset_stored_c_vals

!---------------------------------------------------------------------

    SUBROUTINE get_C_timederiv(k1, F_deriv_vec, C_time_deriv)

        INTEGER, INTENT(IN)      :: k1
        COMPLEX(wp), INTENT(IN)  :: F_deriv_vec(numrows)
        COMPLEX(wp), INTENT(OUT) :: C_time_deriv(numrows)

        INTEGER :: i, j, isol

        ! k1=1 calculate dC/dt = -iHC + F
        ! k1=2 calculate d^2C/dt^2 = (-iH)^2 C + (-iH)(F) + dF/dt
        ! k1=3 calculate d^3C/dt^3 = (-iH)^3 C + (-iH)^2 (F) + (-iH)(dF/dt) + d^2F/dt^2
        ! and so on
        ! The last quantity in each of these equations is held in F_deriv_vec
        ! The other quantities are read from stored_c_vals array

        ! NB This will only work if all inner region processors handle all propagation
        !    orders.  This will not work if there is a split over even and odd orders.
        !    However, a split over even and odd orders makes no sense given that the
        !    inner and outer regions are coupled to each other at every order of the
        !    propagation.

        !$OMP PARALLEL DO PRIVATE(i, j, isol)
        DO isol = 1, numsols
            DO j = 1, numrows
                C_time_deriv(j) = F_deriv_vec(j)
                DO i = 1, k1
                    C_time_deriv(j) = C_time_deriv(j) + stored_c_vals(j, i + 1, k1 - i, isol)
                END DO
            END DO
        END DO

    END SUBROUTINE get_C_timederiv

END MODULE krylov_method
