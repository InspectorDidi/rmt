! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets up long-range potential matrices W_E, W_P and W_D for outer-region.
!> sets up matrices: WE,WD and WP
!> written by MAL Oct 2007
!> modified for CP fields by DDAC March 2017
!> modified for reduced storage by ACB Aug 2018

MODULE lrpots

    USE precisn,            ONLY: wp
    USE rmt_assert,         ONLY: assert
    USE angular_momentum,   ONLY: cg, dracah
    USE initial_conditions, ONLY: dipole_velocity_output, &
                                  RmatrixI_format_id, &
                                  RmatrixII_format_id, &
                                  dipole_format_id, &
                                  molecular_target, &
                                  LS_coupling_id,   &
                                  jK_coupling_id,   &
                                  coupling_id
    USE readhd,             ONLY: ntarg, &
                                  L_block_npty, &
                                  LML_block_npty, &
                                  L_block_lrgl, &
                                  LML_block_lrgl, &
                                  LML_block_ml, &
                                  ltarg, &
                                  L_block_l2p, &
                                  LML_block_l2p, &
                                  m2p, &
                                  LML_block_nspn, &
                                  no_of_L_blocks, &
                                  no_of_LML_blocks, &
                                  L_block_tot_nchan, &
                                  LML_block_tot_nchan, &
                                  L_block_nconat, &
                                  LML_block_nconat, &
                                  crlv, &
                                  LML_block_cf, &
                                  lamax, &
                                  etarg, &
                                  crlv_v, &
                                  wp_read_store, &
                                  wd_read_store, &
                                  wd_read_store_v, &
                                  LML_wp_read_store, &
                                  LML_wd_read_store, &
                                  LML_wd_read_store_v, &
                                  L_block_nchan, &
                                  ichl,          &
                                  mat_size_wp,   &
                                  mat_size_wd

    IMPLICIT NONE

    REAL(wp), ALLOCATABLE, SAVE     :: cfuu(:, :, :), etuu(:)
    COMPLEX(wp), ALLOCATABLE, SAVE  :: wp2pot(:, :, :), wdpot(:, :, :)

    COMPLEX(wp), ALLOCATABLE, SAVE  :: wp1pot(:, :, :), wp2pot_v(:, :, :), wdpot_v(:, :, :)
    INTEGER, ALLOCATABLE, SAVE      :: liuu(:), miuu(:), ltuu(:), lmuu(:), lpuu(:), lsuu(:), icuu(:)
    COMPLEX(wp), ALLOCATABLE, SAVE  :: ksq(:)
    INTEGER, SAVE                   :: we_size, wd_size, wp_size

    PRIVATE

    PUBLIC liuu, miuu, ltuu, lmuu, cfuu, lpuu, lsuu, etuu, icuu, ksq, &
           init_lrpots,  deallocate_lrpots, wp2pot_v, wp2pot, wdpot_v, wdpot, wp1pot, &
           we_size, wd_size, wp_size

CONTAINS

    !> \brief Set up long-range potentials (WE, WD, WP)
    !>
    !> The routines `setupwd_*` and `setuppmatco*` are called twice: once to
    !> determine the size of the matrices, and once to allocate and populate them
    !>
    !> In general the WD and WP arrays could couple any channel to any other, so
    !> the matrices should be of dimension `LML_block_tot_nchan` * `LML_block_tot_nchan`. For systems
    !> with many channels, this becomes prohibitively large. Hence these are
    !> stored in a banded-structure. In practice, only a subset of channels is
    !> coupled, and the resulting array is a banded diagonal. Thus we store the
    !> matrix element coupling channel `ii` to channel `jj` in element (ii - jj,
    !> jj) of the matrix. The dimension is thus `MAX( coupled (ii - jj)) ,
    !> L_block_tot_nchan)`, which is in general much smaller than the full matrix.
    !>
    SUBROUTINE init_lrpots

        IF (.NOT. molecular_target) THEN
            CALL makecfuu(.TRUE., we_size)                     !determines size of WE
            CALL makecfuu(.FALSE., we_size)                    !sets up coefficients for WE

            IF (dipole_format_id.EQ.RmatrixII_format_id) THEN
                CALL setupwd_len_atomic(.TRUE., wd_size)           !determines size of WD
                CALL setupwd_len_atomic(.FALSE., wd_size)          !sets up coefficients for WD
                CALL setuppmatco_len_atomic(.TRUE., wp_size)       !determines size of WP
                CALL setuppmatco_len_atomic(.FALSE., wp_size)      !sets up coefficients for WP
                IF (dipole_velocity_output) THEN
                    CALL setupwd_vel_atomic(.FALSE., wd_size)       !sets up coefficients for WD (velocity gauge)
                    CALL setuppmatco_vel_atomic(.FALSE., wp_size)   !sets up coefficients for WP (velocity gauge)
                END IF
            ELSE
               CALL setup_wp_and_wd_for_RMatrixI
               CALL setuppmatco_len_atomic(.false., mat_size_wp) !Check wp matrix
            END IF
        ELSE
            CALL makecfuu(.TRUE., we_size)                     !determines size of WE
            CALL makecfuu(.FALSE., we_size)                    !sets up coefficients for WE
            CALL setupwd_len_molecular(.TRUE., wd_size)        !determines size of WD
            CALL setupwd_len_molecular(.FALSE., wd_size)       !sets up coefficients for WD
            CALL setuppmatco_len_molecular(.TRUE., wp_size)    !determines size of WP
            CALL setuppmatco_len_molecular(.FALSE., wp_size)   !sets up coefficients for WP
        END IF

    END SUBROUTINE init_lrpots

    !>\brief allocates WP and WD matricies for when RMatrixI inputs are used
    !>
    !> The WP and WD matrix data has already been read from readhd. and stored
    !> in LML_wd_read_store and LML_wp_read_store.  This subroutine allocates
    !> and wdpot and wp2pot
    SUBROUTINE setup_wp_and_wd_for_RMatrixI
        INTEGER :: err

        wd_size=mat_size_wd
        wp_size=mat_size_wp

        !TODO - velocity guage here
        ALLOCATE (wp2pot(1, -mat_size_wp:mat_size_wp, LML_block_tot_nchan), stat=err)
        ALLOCATE (wdpot(1, -mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat=err)

        wdpot(1,:,:)=LML_wd_read_store
        wp2pot(1,:,:)=LML_wp_read_store

        IF (dipole_velocity_output) THEN
            ALLOCATE(wdpot_v(1, -mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat=err)
            wdpot_v(1,:,:)=LML_wd_read_store_v


            ALLOCATE (wp1pot(1, -mat_size_wp:mat_size_wp, LML_block_tot_nchan), &
                      wp2pot_v(1, -mat_size_wp:mat_size_wp, LML_block_tot_nchan), stat=err)
            wp2pot_v=0.0_wp
            wp1pot=0.0_wp
        END IF

        wdpot = (-1.0_wp, 0.0_wp)*wdpot

    End SUBROUTINE

    !> \brief determine size/allocate and set up coefficients for WD
    !>
    !> The WD matrix describes the laser coupling with the residual ion. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    SUBROUTINE setupwd_len_atomic(get_size, mat_size)

        ! DDAC: SIGNS CONSISTENT WITH ORIGINAL RMT FOR delta_ML = 0 CASE

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER  :: i, j, ist, jst, nctf, ncti, ichf, ichi
        REAL(wp) :: w6j
        INTEGER  :: chcount1, chcount2, targ1, targ2
        INTEGER  :: li, lj
        INTEGER  :: lcfgf, lcfgi
        INTEGER  :: blf, bli, ii, jj, ptyi, ptyf
        INTEGER  :: mli, mlf, delta_ml
        INTEGER  :: a2, a3, a4, a5, a6, err
        REAL(wp) :: c, a, aa, sign1, tmom

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wdpot(1, -mat_size:mat_size, LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wdmat ')
            wdpot = (0.0_wp, 0.0_wp)
        END IF

        IF (dipole_format_id .EQ. RmatrixII_format_id) THEN
            ! WARNING: ISSUE WITH WHICH IS INITIAL AND FINAL TARG STATE
            ii = 0
            c = 0
            aa = 0
            w6j = 0
            tmom = 0
            DO i = 1, no_of_LML_blocks
                bli = LML_block_lrgl(i)
                mli = LML_block_ml(i)
                ptyi = LML_block_npty(i)
                chcount1 = 0
                DO ist = 1, ntarg
                    DO ncti = 1, LML_block_nconat(ist, i)
                        chcount1 = chcount1 + 1
                        ichi = chcount1
                        targ1 = ist
                        lcfgi = ltarg(targ1)
                        li = LML_block_l2p(ichi, i)
                        ii = ii + 1
                        jj = 0

                        DO j = 1, no_of_LML_blocks
                            blf = LML_block_lrgl(j)
                            mlf = LML_block_ml(j)
                            ptyf = LML_block_npty(j)
                            chcount2 = 0

                            DO jst = 1, ntarg
                                DO nctf = 1, LML_block_nconat(jst, j)
                                    chcount2 = chcount2 + 1
                                    ichf = chcount2
                                    targ2 = jst
                                    lcfgf = ltarg(targ2)
                                    lj = LML_block_l2p(ichf, j)
                                    jj = jj + 1

                                    IF (li == lj .AND. ABS(crlv(targ1, targ2)) &
                                        .NE. 0.0_wp &
                                        .AND. ABS(mlf - mli) .LE. 1 &
                                        .AND. ABS(blf - bli) .LE. 1) THEN

                                        a = SQRT(2._wp*REAL(blf, wp) + 1._wp)
                                        sign1 = 1.0_wp

                                        IF (MOD(lcfgi + lcfgf + 1, 2) == 1) THEN
                                            sign1 = -1.0_wp
                                        END IF

                                        c = a*sign1
                                        a2 = lcfgf + lcfgf
                                        a3 = bli + bli
                                        a4 = li + li
                                        a5 = lcfgi + lcfgi
                                        a6 = blf + blf
                                        tmom = crlv(targ1, targ2)     ! length target moments
                                        CALL dracah(2, a2, a3, a4, a5, a6, w6j)

                                        delta_ml = mlf - mli

                                        IF (delta_ml == 1) THEN
                                            aa = -cg(1, blf, bli, -1, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot(1, ii - jj, jj) = c*aa*w6j*tmom
                                            END IF
                                        END IF

                                        IF (delta_ml == -1) THEN
                                            aa = -cg(1, blf, bli, 1, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot(1, ii - jj, jj) = c*aa*w6j*tmom
                                            END IF
                                        END IF

                                        IF (delta_ml == 0) THEN
                                            aa = -cg(1, blf, bli, 0, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot(1, ii - jj, jj) = c*aa*w6j*tmom
                                            END IF
                                        END IF

                                    END IF

                                END DO
                            END DO
                        END DO
                    END DO
                END DO
            END DO

        ELSE
            wdpot(1, :, :) = LML_wd_read_store(:, :)
        END IF


        ! Length gauge only - multiply by i
        ! Fano Racah phase convention
!       wdpot = (0.0_wp,1.0_wp)*wdpot  ! DDAC: Field comps. elsewhere

        IF (.NOT. get_size) wdpot = (-1.0_wp, 0.0_wp)*wdpot


    END SUBROUTINE setupwd_len_atomic

!-------------------------------------------------------------------------------------

    !> \brief determine size/allocate and set up coefficients for WD
    !>
    !> The WD matrix describes the laser coupling with the residual ion. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    SUBROUTINE setupwd_len_molecular(get_size, mat_size)

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER :: i, j, ichf, ichi
        INTEGER :: targ1, targ2
        INTEGER :: li, lj, mi, mj
        INTEGER :: ii, jj
        INTEGER :: err
        REAL(wp), DIMENSION(3) :: tmom

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wdpot(3, -mat_size:mat_size, LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wdpot')
            wdpot = (0.0_wp, 0.0_wp)
        END IF
        ii = 0

        DO i = 1, no_of_L_blocks
            DO ichi = 1, L_block_nchan(i)
                li = L_block_l2p(ichi, i)
                mi = m2p(ichi, i)
                targ1 = ichl(ichi, i)
                ii = ii + 1
                jj = 0

                DO j = 1, no_of_L_blocks
                    DO ichf = 1, L_block_nchan(j)
                        lj = L_block_l2p(ichf, j)
                        mj = m2p(ichf, j)
                        targ2 = ichl(ichf, j)
                        jj = jj + 1
                        tmom(1:3) = crlv(3*(targ1 - 1) + 1:3*(targ1 - 1) + 3, targ2)

                        ! todo implement selection rule on the total spins in i and j symmetry

                        IF (li == lj .AND. mi == mj .AND. any(tmom /= 0)) THEN
                            IF (get_size) THEN
                                mat_size = MAX( ABS(ii-jj), mat_size)
                            ELSE
                                wdpot(1, ii - jj, jj) = tmom(1)
                                wdpot(2, ii - jj, jj) = tmom(2)
                                wdpot(3, ii - jj, jj) = tmom(3)
                            END IF
                        END IF

                    END DO
                END DO
            END DO
        END DO

    END SUBROUTINE setupwd_len_molecular

!-------------------------------------------------------------------------------------
! SET UP WD matrix   FOR LENGTH GAUGE
!-------------------------------------------------------------------------------------

    SUBROUTINE setupwd_vel_atomic(get_size, mat_size)

    ! DDAC: SIGNS CONSISTENT WITH ORIGINAL RMT FOR delta_ML = 0 CASE

    ! This subroutine is required only to compute the expectation value of
    ! dipole velocity in the outer region. For this purpose, the
    ! electric-field array is replaced with factors_axis (see
    ! subroutine hhg_inner in module krylov_method) that
    ! selects which dipole blocks should be included in the calculations.
    ! At present, only the data for the M_{L}-conserving transitions are
    ! retained, as necessary to calculate the expectation value of the
    ! z-component of dipole velocity. Although factors_axis does not
    ! represent a field, its elements are treated like those of the electric-field
    ! array, and are multiplied by the appropriate phase factors in
    ! field_sph_component. The phase factors appearing in this subroutine
    ! compensate for these multiplications, such as to produce the correct
    ! harmonic phases.



        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER  :: i, j, ist, jst, nctf, ncti, ichf, ichi
        REAL(wp) :: w6j
        INTEGER  :: chcount1, chcount2, targ1, targ2
        INTEGER  :: li, lj
        INTEGER  :: lcfgf, lcfgi
        INTEGER  :: blf, bli, ii, jj, ptyi, ptyf
        INTEGER  :: mli, mlf, delta_ml
        INTEGER  :: a2, a3, a4, a5, a6, err
        REAL(wp) :: c, a, aa, sign1, tmom

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wdpot_v(1, -mat_size:mat_size, LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wdmat ')
            wdpot_v = (0.0_wp, 0.0_wp)
        END IF

        IF (dipole_format_id .EQ. RmatrixII_format_id) THEN
            ! WARNING: CARE NEEDED WITH WHICH IS INITIAL AND FINAL TARG STATE
            ii = 0
            c = 0
            aa = 0
            w6j = 0
            tmom = 0
            DO i = 1, no_of_LML_blocks
                bli = LML_block_lrgl(i)
                mli = LML_block_ml(i)
                ptyi = LML_block_npty(i)
                chcount1 = 0
                DO ist = 1, ntarg
                    DO ncti = 1, LML_block_nconat(ist, i)
                        chcount1 = chcount1 + 1
                        ichi = chcount1
                        targ1 = ist
                        lcfgi = ltarg(targ1)
                        li = LML_block_l2p(ichi, i)
                        ii = ii + 1
                        jj = 0

                        DO j = 1, no_of_LML_blocks
                            blf = LML_block_lrgl(j)
                            mlf = LML_block_ml(j)
                            ptyf = LML_block_npty(j)
                            chcount2 = 0

                            DO jst = 1, ntarg
                                DO nctf = 1, LML_block_nconat(jst, j)
                                    chcount2 = chcount2 + 1
                                    ichf = chcount2
                                    targ2 = jst
                                    lcfgf = ltarg(targ2)
                                    lj = LML_block_l2p(ichf, j)
                                    jj = jj + 1

                                    IF (li == lj .AND. ABS(crlv_v(targ1, targ2)) &
                                        .NE. 0.0_wp &
                                        .AND. ABS(mlf - mli) .LE. 1 &
                                        .AND. ABS(blf - bli) .LE. 1) THEN

                                        a = SQRT(2._wp*REAL(blf, wp) + 1._wp)
                                        sign1 = 1.0_wp

                                        IF (MOD(lcfgi + lcfgf + 1, 2) == 1) THEN
                                            sign1 = -1.0_wp
                                        END IF

                                        c = a*sign1
                                        a2 = lcfgf + lcfgf
                                        a3 = bli + bli
                                        a4 = li + li
                                        a5 = lcfgi + lcfgi
                                        a6 = blf + blf
                                        tmom = crlv_v(targ1, targ2)     ! Velocity target moments
                                        CALL dracah(2, a2, a3, a4, a5, a6, w6j)

                                        delta_ml = mlf - mli

                                        IF (delta_ml == 1) THEN
                                            aa = -cg(1, blf, bli, -1, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                ! DDAC: Factor -i since p_{z} = -id/dz
                                                wdpot_v(1, ii - jj, jj) = (0.0_wp,-1.0_wp)*c*aa*w6j*tmom
                                            END IF
                                        END IF

                                        IF (delta_ml == -1) THEN
                                            aa = -cg(1, blf, bli, 1, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot_v(1, ii - jj, jj) = (0.0_wp,-1.0_wp)*c*aa*w6j*tmom
                                            END IF
                                        END IF

                                        IF (delta_ml == 0) THEN
                                            aa = -cg(1, blf, bli, 0, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot_v(1, ii - jj, jj) = (0.0_wp,-1.0_wp)*c*aa*w6j*tmom
                                            END IF
                                        END IF

                                    END IF


                                END DO
                            END DO
                        END DO
                    END DO
                END DO
            END DO

        ELSE
            wdpot_v(1, :, :) = -wd_read_store(:, :)  !GSJA added factor of -1 needed due to multiplication by -1 below
        END IF



    END SUBROUTINE setupwd_vel_atomic

!-------------------------------------------------------------------------------------

    !> \brief determine size/allocate and set up coefficients for WP
    !>
    !> The WP matrix describes the laser coupling with the outer electron. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    !>
    !> (based on R-matrix book, pg 399)
    SUBROUTINE setuppmatco_len_atomic(get_size, mat_size)

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER     :: ii, jj, i, j, ist, jst, ncti, nctf
        INTEGER     :: bli, ptyi, blf, ptyf, ichi, jchf
        INTEGER     :: chcount1, chcount2, targ1, targ2
        INTEGER     :: lcfgi, li, lcfgf, lj, err
        INTEGER     :: bli2, blf2, li2, lj2, lcfgi2
        INTEGER     :: mli, mlf, delta_ml
        REAL(wp)    :: sign1, b, c, d, w6j
        COMPLEX(wp) :: pmtrxel1, pmtrxel2
        REAL(wp)    :: al, bl
        COMPLEX(wp) :: sign2
        COMPLEX(wp), ALLOCATABLE :: coeff_check(:, :)

        LOGICAL :: All_Passed

        IF (get_size) THEN
            mat_size = 0
        ELSE
            IF (dipole_format_id .eq. RmatrixII_format_id) THEN
                ALLOCATE (wp2pot(1, -mat_size:mat_size, LML_block_tot_nchan), stat=err)
                CALL assert(err .EQ. 0, 'allocation error in wpmat ')
                wp2pot = (0.0_wp, 0.0_wp)
            END IF
        END IF


        ! If we are using RMatrixI dipole data, then the wp matricies should have already
        ! been read in and are stored in wp_read_store . Here we only need to assign
        ! the data to the wp2pot matrix. We still run the RMT code to create the matrix,
        ! but rather than write the pmtrexl1 to the wp2pot array, we store it in coeff_check.
        ! We then perform a comparison of wp2pot and coeff_check at the end.
        IF (dipole_format_id .eq. RmatrixI_format_id) THEN
            ALLOCATE (coeff_check(-mat_size:mat_size, LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in coeff_check ')
        END IF

        ! Begin calculation of wp2pot(if RMatrixII dipole format) or coeff_check(if RMatrixI dipole format)
        ii = 0
        DO i = 1, no_of_LML_blocks
            bli = LML_block_lrgl(i)
            ptyi = LML_block_npty(i)
            mli = LML_block_ml(i)
            chcount1 = 0

            DO ist = 1, ntarg
                DO ncti = 1, LML_block_nconat(ist, i)
                    chcount1 = chcount1 + 1
                    ichi = chcount1
                    targ1 = ist
                    lcfgi = ltarg(targ1)
                    li = LML_block_l2p(ichi, i)
                    ii = ii + 1
                    jj = 0

                    DO j = 1, no_of_LML_blocks
                        blf = LML_block_lrgl(j)
                        ptyf = LML_block_npty(j)
                        mlf = LML_block_ml(j)
                        chcount2 = 0

                        DO jst = 1, ntarg
                            DO nctf = 1, LML_block_nconat(jst, j)
                                chcount2 = chcount2 + 1
                                jchf = chcount2
                                targ2 = jst
                                lcfgf = ltarg(targ2)
                                lj = LML_block_l2p(jchf, j)
                                jj = jj + 1

                                IF (targ1 == targ2 .AND. (ptyi /= ptyf)) THEN
                                    pmtrxel1 = (0._wp, 0.0_wp)
                                    pmtrxel2 = (0._wp, 0.0_wp)
                                    sign1 = 1.0_wp    !DDAC: consistent with original RMT

                                    ! warning with signs
                                    IF (MOD(blf + bli, 2) == 1) THEN
                                        sign1 = -1.0_wp    !DDAC: consistent with original RMT
                                    END IF

                                    ! WARNING ON SIGNS
!                                    sign2 = (0.0_wp,1.0_wp)   !length gauge

                                    sign2 = (1.0_wp, 0.0_wp)

                                    b = SQRT((2._wp*REAL(lj, wp) + 1._wp)*(2._wp*REAL(bli, wp) + 1._wp))
!                                   c = cg(bli,1,blf,ml,0,ml)
                                    d = cg(li, 1, lj, 0, 0, 0)
                                    li2 = li + li
                                    lj2 = lj + lj
                                    bli2 = bli + bli
                                    blf2 = blf + blf
                                    lcfgi2 = lcfgi + lcfgi
                                    CALL dracah(2, lj2, bli2, lcfgi2, li2, blf2, w6j)

                                    IF (li == (lj - 1)) THEN
                                        !!!!!!!!WARNING ON SIGN!!!!!!!!!!!!
                                        al = REAL(lj, wp)/SQRT((2._wp*REAL(lj, wp) - 1._wp)* &
                                                               (2._wp*REAL(lj, wp) + 1._wp))
                                        !!!!!!!!!!!!!!!!!!!!!
                                        bl = REAL(lj, wp)

                                    ELSE
                                        IF (li == (lj + 1)) THEN
                                            ! Warning on sign change for al in length gauge
                                            al = -(REAL(lj, wp) + 1._wp)/SQRT((2._wp*REAL(lj, wp) + 1._wp)* &
                                                                              (2._wp*REAL(lj, wp) + 3._wp))
                                            bl = -REAL(lj, wp) - 1._wp
                                        END IF
                                    END IF

                                    delta_ml = mlf - mli

!                                   IF (ABS(d) < 0.1e-11_wp) THEN
!                                       pmtrxel1 = (0.0_wp,0.0_wp)
!                                       pmtrxel2 = (0.0_wp,0.0_wp)
!                                   ELSE

                                    IF (delta_ml == 1) THEN
                                        IF (ABS(d) < 0.1e-11_wp) THEN
                                            pmtrxel1 = (0.0_wp, 0.0_wp)
                                            pmtrxel2 = (0.0_wp, 0.0_wp)
                                        ELSE
                                            c = cg(bli, 1, blf, mli, 1, mlf)
                                            pmtrxel1 = -(sign1*sign2*b*c*w6j*al)/d  ! DDAC: Length gauge
                                            pmtrxel2 = (sign1*sign2*b*c*w6j*al*bl)/d  ! DDAC: Velocity gauge
                                        END IF
                                    END IF

                                    IF (delta_ml == -1) THEN
                                        IF (ABS(d) < 0.1e-11_wp) THEN
                                            pmtrxel1 = (0.0_wp, 0.0_wp)
                                            pmtrxel2 = (0.0_wp, 0.0_wp)
                                        ELSE
                                            c = cg(bli, 1, blf, mli, -1, mlf)
                                            pmtrxel1 = -(sign1*sign2*b*c*w6j*al)/d  !DDAC: Length gauge
                                            pmtrxel2 = (sign1*sign2*b*c*w6j*al*bl)/d  ! DDAC: Velocity gauge
                                        END IF
                                    END IF

                                    IF (delta_ml == 0) THEN
                                        IF (ABS(d) < 0.1e-11_wp) THEN
                                            pmtrxel1 = (0.0_wp, 0.0_wp)
                                            pmtrxel2 = (0.0_wp, 0.0_wp)
                                        ELSE
                                            c = cg(bli, 1, blf, mli, 0, mlf)
                                            pmtrxel1 = (sign1*sign2*b*c*w6j*al)/d  ! DDAC: Length gauge
                                            pmtrxel2 = (sign1*sign2*b*c*w6j*al*bl)/d  ! DDAC: Velocity gauge
                                        END IF
                                    END IF

!                                   END IF

!                                   IF (ABS(d) < 0.1e-11_wp) THEN
!                                       pmtrxel1 = (0.0_wp,0.0_wp)
!                                       pmtrxel2 = (0.0_wp,0.0_wp)
!                                   ELSE
!                                       pmtrxel1 = (sign1*sign2*b*c*w6j*al)/d    ! length gauge
!                                       pmtrxel2 = (sign1*sign2*b*c*w6j*al*bl)/d
!                                   END IF

                                    IF (dipole_format_id .EQ. RmatrixII_format_id) THEN
                                        IF (pmtrxel1 /= (0.0_wp, 0.0_wp)) THEN
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wp2pot(1, ii-jj, jj) = pmtrxel1
                                            END IF
                                        END IF
                                    ELSE
                                       if (abs(pmtrxel1).gt.0.00000000001) then
                                       if (abs(ii-jj).gt.abs(mat_size)) then
                                          print *,'about to crash',ii,jj,pmtrxel1,mat_size
                                       end if
                                       coeff_check(ii-jj, jj) = pmtrxel1
                                       END IF
                                    END IF

!                               ELSE

!                                   IF (dipole_format_id .EQ. RmatrixII_format_id) THEN
!                                       wp2pot(1, ii-jj, jj) = (0.0_wp, 0.0_wp)
!                                   ELSE
!                                       coeff_check(ii, jj) = 0.0_wp
!                                   END IF

                                END IF

                            END DO
                        END DO
                    END DO
                END DO
            END DO
        END DO

        ! If we use RMatrixI dipole format, we Check that RMatrixI wp wp2pots agree
        ! with RMT calculated wp2pots
        IF (dipole_format_id .EQ. RmatrixI_format_id) THEN

            All_Passed=.true.

            IF (coupling_id.EQ.LS_coupling_id) THEN
            DO ii = -mat_size, mat_size
                DO jj = 1, LML_block_tot_nchan

                    ! if the two matrices don't agree then -
                    IF (ABS(ABS(coeff_check(ii, jj) - wp2pot(1, ii, jj))) .GT. 0.00001_wp) THEN
!                       PRINT diagnostic info
                        PRINT *, 'error in wp2pot!', ii, jj
                        PRINT *, 'coeff_check', ABS(coeff_check(ii, jj)),ABS(wp2pot(1, ii, jj))
                        ! and exit RMT.
                        All_Passed=.false.
                    END IF

                END DO
            END DO
            END IF

            CALL assert(All_Passed, 'Stopping')

            DEALLOCATE (coeff_check)

        END IF

    END SUBROUTINE setuppmatco_len_atomic

!-------------------------------------------------------------------------------------


    !> \brief determine size/allocate and set up coefficients for WP
    !>
    !> The WP matrix describes the laser coupling with the outer electron. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    !>
    !> Needed for molecular case:
    !> Must be diagonal in target space and contains coupling cfs for three real spherical harmonics.
    SUBROUTINE setuppmatco_len_molecular(get_size, mat_size)

        USE global_data, ONLY: pi

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        COMPLEX(wp) :: pmtrxel(3)
        INTEGER :: ii, jj, i, j
        INTEGER :: ichi, ichj
        INTEGER :: targ1, targ2
        INTEGER :: li, lj, mi, mj, err

        ! norm removes the normalization factor sqrt(3/(4*pi)) for the real sph. harmonic, e.g. S_{1,1} = sqrt(3/(4*pi))*x/r but we need only x/r.
        ! and the minus sign is for electron charge
        REAL(wp), parameter :: norm = -SQRT(4.0_wp*pi/3.0_wp)

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wp2pot(3, -mat_size:mat_size, LML_block_tot_nchan), &
                      stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wpmat ')
            wp2pot = (0.0_wp, 0.0_wp)
        END IF

        ii = 0
        DO i = 1, no_of_L_blocks
            DO ichi = 1, L_block_nchan(i)
                li = LML_block_l2p(ichi, i)
                mi = m2p(ichi, i)
                targ1 = ichl(ichi, i)
                ii = ii + 1
                jj = 0

                DO j = 1, no_of_L_blocks
                    DO ichj = 1, L_block_nchan(j)
                        lj = LML_block_l2p(ichj, j)
                        mj = m2p(ichj, j)
                        targ2 = ichl(ichj, j)
                        jj = jj + 1

                        ! todo implement selection rule on the total spins in i and j symmetry


                        pmtrxel(1) = norm*get_coupling(li, lj, mi, mj, 1, 1)
                        pmtrxel(2) = norm*get_coupling(li, lj, mi, mj, 1, -1)
                        pmtrxel(3) = norm*get_coupling(li, lj, mi, mj, 1, 0)

                        IF (targ1 == targ2) THEN
                            IF (ANY(ABS(pmtrxel) > 1e-12_wp)) THEN
                                IF (get_size) THEN
                                    mat_size = MAX( ABS(ii-jj), mat_size)
                                ELSE
                                    ! integral over three real spherical harmonics with the dipole spherical harmonic Y(1,mi+mj)
                                    wp2pot(1, ii - jj, jj) = pmtrxel(1) !   wp2pot(1,:,:) corresponds to Y(1,+1), i.e. ~x
                                    wp2pot(2, ii - jj, jj) = pmtrxel(2) !   wp2pot(2,:,:)     "          Y(1,-1), i.e. ~y
                                    wp2pot(3, ii - jj, jj) = pmtrxel(3) !   wp2pot(3,:,:)     "          Y(1, 0), i.e. ~z
                                END IF
                            END IF
                        END IF

                    END DO
                END DO
            END DO
        END DO

        ! finally, drop negligible couplings
        IF (.NOT. get_size) THEN
            WHERE (ABS(wp2pot) < 1e-12_wp) wp2pot = 0
        END IF

    END SUBROUTINE setuppmatco_len_molecular

!-------------------------------------------------------------------------------------

    FUNCTION get_coupling(li, lj, mi, mj, L, M)

        USE readhd, ONLY: n_rg, rg, lm_rg

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: li, lj, mi, mj, L, M
        REAL(wp) :: get_coupling

        INTEGER :: test(6), i, j
        LOGICAL :: match

        get_coupling = 0.0_wp

        test(1:6) = (/li, mi, lj, mj, L, M/)

        DO i = 1, n_rg
            match = .true.

            DO j = 1, 6
                IF (test(j) .NE. lm_rg(j, i)) THEN
                    match = .false.
                    EXIT
                END IF
            END DO

            IF (match) THEN
                get_coupling = rg(i)
                EXIT
            END IF

        END DO !i

    END FUNCTION get_coupling

!-------------------------------------------------------------------------------------
! SET UP PMAT AND COEFF (based on R-matrix book, pg 399) VELOCITY GAUGE
!-------------------------------------------------------------------------------------

    SUBROUTINE setuppmatco_vel_atomic(get_size, mat_size)


    ! This subroutine is required only to compute the expectation value of
    ! dipole velocity in the outer region. For this purpose, the
    ! electric-field array is replaced with factors_axis (see
    ! subroutine hhg_inner in module krylov_method) that
    ! selects which dipole blocks should be included in the calculations.
    ! Although factors_axis does not represent a field, 
    ! its elements are treated like those of the electric-field
    ! array, and are multiplied by the appropriate phase factors in
    ! field_sph_component. The phase factors appearing in this subroutine
    ! compensate for these multiplications, such as to produce the correct
    ! harmonic phases.



        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER     :: ii, jj, i, j, ist, jst, ncti, nctf
        INTEGER     :: bli, ptyi, blf, ptyf, ichi, jchf
        INTEGER     :: chcount1, chcount2, targ1, targ2
        INTEGER     :: lcfgi, li, lcfgf, lj, err
        INTEGER     :: bli2, blf2, li2, lj2, lcfgi2
        INTEGER     :: mli, mlf, delta_ml
        REAL(wp)    :: sign1, b, c, d, w6j
        COMPLEX(wp) :: pmtrxel1, pmtrxel2
        REAL(wp)    :: al, bl
        COMPLEX(wp) :: sign2

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wp1pot(1, -mat_size:mat_size, LML_block_tot_nchan), &
                      wp2pot_v(1, -mat_size:mat_size, LML_block_tot_nchan), stat=err)

            CALL assert(err .EQ. 0, 'allocation error in wp1pot and/or wp2pot_v ')
            wp1pot = (0.0_wp, 0.0_wp)
            wp2pot_v = (0.0_wp, 0.0_wp)
        END IF



        !wp1pot = (0.0_wp, 0.0_wp)
        !wp2pot_v = (0.0_wp, 0.0_wp)
        ii = 0
        DO i = 1, no_of_LML_blocks
            bli = LML_block_lrgl(i)
            ptyi = LML_block_npty(i)
            mli = LML_block_ml(i)
            chcount1 = 0

            DO ist = 1, ntarg
                DO ncti = 1, LML_block_nconat(ist, i)
                    chcount1 = chcount1 + 1
                    ichi = chcount1
                    targ1 = ist
                    lcfgi = ltarg(targ1)
                    li = LML_block_l2p(ichi, i)
                    ii = ii + 1
                    jj = 0

                    DO j = 1, no_of_LML_blocks
                        blf = LML_block_lrgl(j)
                        ptyf = LML_block_npty(j)
                        mlf = LML_block_ml(j)
                        chcount2 = 0

                        DO jst = 1, ntarg
                            DO nctf = 1, LML_block_nconat(jst, j)
                                chcount2 = chcount2 + 1
                                jchf = chcount2
                                targ2 = jst
                                lcfgf = ltarg(targ2)
                                lj = LML_block_l2p(jchf, j)
                                jj = jj + 1

                                IF (targ1 == targ2 .AND. (ptyi /= ptyf)) THEN
                                    pmtrxel1 = (0._wp, 0.0_wp)
                                    pmtrxel2 = (0._wp, 0.0_wp)
                                    sign1 = -1.0_wp

                                    ! warning with signs
                                    IF (MOD(blf + bli, 2) == 1) THEN
                                        sign1 = 1.0_wp
                                    END IF

                                    ! WARNING ON SIGNS
                                    sign2 = (0.0_wp, 1.0_wp)


                                    b = SQRT((2._wp*REAL(lj, wp) + 1._wp)*(2._wp*REAL(bli, wp) + 1._wp))
                                    !c = cg(bli, 1, blf, ml, 0, ml)
                                    d = cg(li, 1, lj, 0, 0, 0)
                                    li2 = li + li
                                    lj2 = lj + lj
                                    bli2 = bli + bli
                                    blf2 = blf + blf
                                    lcfgi2 = lcfgi + lcfgi
                                    CALL dracah(2, lj2, bli2, lcfgi2, li2, blf2, w6j)

                                    IF (li == (lj - 1)) THEN
                                        !!!!!!!!WARNING ON SIGN!!!!!!!!!!!!
                                        al = REAL(lj, wp)/SQRT((2._wp*REAL(lj, wp) - 1._wp)* &
                                                               (2._wp*REAL(lj, wp) + 1._wp))
                                        !!!!!!!!!!!!!!!!!!!!!
                                        bl = REAL(lj, wp)
                                    ELSE
                                        IF (li == (lj + 1)) THEN
                                            !warning on sign change for al in length gauge
                                            al = -(REAL(lj, wp) + 1._wp)/SQRT((2._wp*REAL(lj, wp) + 1._wp)* &
                                                                              (2._wp*REAL(lj, wp) + 3._wp))
                                            bl = -REAL(lj, wp) - 1._wp
                                        END IF
                                    END IF


                                    delta_ml = mlf - mli


                                    IF (ABS(delta_ml) .LE. 1) THEN

                                        IF (ABS(d) < 0.1e-11_wp) THEN

                                            pmtrxel1 = (0.0_wp, 0.0_wp)
                                            pmtrxel2 = (0.0_wp, 0.0_wp)

                                        ELSE

                                            c = cg(bli, 1, blf, mli, delta_ml, mlf)*(-1.0_wp)**(delta_ml)
                                            pmtrxel1 = (sign2*sign1*b*c*w6j*al*bl)/d  ! Unique to velocity gauge
                                            pmtrxel2 = (sign2*sign1*b*c*w6j*al)/d!

                                        END IF

                                    END IF

                                        IF (pmtrxel2 /= (0.0_wp, 0.0_wp)) THEN
                                            wp1pot(1, ii-jj, jj) = pmtrxel2
                                        END IF
                                        IF (pmtrxel1 /= (0.0_wp, 0.0_wp)) THEN
                                            wp2pot_v(1, ii-jj, jj) = pmtrxel1
                                        END iF

                                END IF

                            END DO
                        END DO
                    END DO
                END DO
            END DO
        END DO

    END SUBROUTINE setuppmatco_vel_atomic

    !> \brief determine size/allocate and set up coefficients for WE
    !>
    !> The WE matrix describes the coupling between the outer electron and residual ion. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix cfuu, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    SUBROUTINE makecfuu(get_size,mat_size)

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER :: ii, jj, i, j, ist, jst
        INTEGER :: chcount1, chcount2, ncti, nctf
        INTEGER :: lj, ichi, ichf, err, lambda

        IF (get_size) THEN
            mat_size = 0
            ALLOCATE (liuu(LML_block_tot_nchan), &
                  etuu(LML_block_tot_nchan), ksq(LML_block_tot_nchan), ltuu(LML_block_tot_nchan), &
                  lmuu(LML_block_tot_nchan), miuu(LML_block_tot_nchan), icuu(LML_block_tot_nchan), &
                  lpuu(LML_block_tot_nchan), lsuu(LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wemat ')
        ELSE
            ALLOCATE (cfuu(-mat_size:mat_size, LML_block_tot_nchan, lamax), stat=err)
            cfuu = 0._wp
            CALL assert(err .EQ. 0, 'allocation error in wemat ')
        END IF

        ii = 0
        DO i = 1, no_of_LML_blocks
            chcount1 = 0

            DO ist = 1, ntarg
                DO ncti = 1, LML_block_nconat(ist, i)
                    chcount1 = chcount1 + 1
                    ichi = chcount1
                    ii = ii + 1
                    etuu(ii) = etarg(ist) - etarg(1)
                    ksq(ii) = -2.0_wp*(etuu(ii))
                    liuu(ii) = LML_block_l2p(ichi, i)
                    miuu(ii) = m2p(ichi, i)

                    IF (molecular_target) THEN
                        icuu(ii) = ichl(ichi, i)
                    ELSE
                        DO j = 1, ii
                            IF (ABS(etuu(ii) - etuu(j)) < 1e-8_wp) THEN
                                icuu(ii) = j
                                EXIT
                            END IF
                        END DO
                    END IF

                    ltuu(ii) = LML_block_lrgl(i)

                    lmuu(ii) = LML_block_ml(i)

                    lpuu(ii) = LML_block_npty(i)
                    lsuu(ii) = LML_block_nspn(i)

                    jj = 0
                    DO j = 1, no_of_LML_blocks
                        chcount2 = 0

                        DO jst = 1, ntarg
                            DO nctf = 1, LML_block_nconat(jst, j)
                                chcount2 = chcount2 + 1
                                ichf = chcount2
                                lj = LML_block_l2p(ichf, j)
                                jj = jj + 1

                                IF (i == j) THEN
                                    IF (get_size) THEN
                                        mat_size = MAX( ABS(ii-jj), mat_size)
                                    ELSE
                                        DO lambda = 1, lamax
                                            cfuu(ii - jj, jj, lambda) = LML_block_cf(ichi, ichf, lambda, j)
                                        END DO
                                    END IF
                                END IF

                            END DO
                        END DO
                    END DO
                END DO
            END DO
        END DO


    END SUBROUTINE makecfuu

!-------------------------------------------------------------------------------------

    SUBROUTINE deallocate_lrpots

        INTEGER :: err

        DEALLOCATE (wdpot, wp2pot, cfuu, liuu, etuu, ksq, ltuu, lmuu, lpuu, lsuu, stat=err)
        IF (err /= 0) THEN
            PRINT *, 'deallocation error lrpots'
        END IF

        IF (dipole_velocity_output) THEN
            DEALLOCATE (wdpot_v, wp1pot, wp2pot_v, stat=err)
            IF (err /= 0) THEN
                PRINT *, 'deallocation error lrpots_v'
            END IF
        END IF

    END SUBROUTINE deallocate_lrpots

END MODULE lrpots
