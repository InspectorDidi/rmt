\page run_rmt Running RMT

Test Suite
=========

To test your version of RMT you may wish to run some calculations and compare
with expected outputs.  A test suite is provided with sample outputs under
`tests`. The exemplar calculations are subdivided into atomic and molecular
categories, with the atomic calculations further divided into small (can be run
on fewer than 96 cores) and large (requires more than 196 cores) directories.

Atomic Tests
------------

Each atomic test calculation directory is structured identically. Taking the
`tests/atomic_tests/small_tests/helium` directory as an example, the directory
contains subdirectories `inputs`-- containing the necessary input files: 

    H, d, d00, Splinedata, Splinewaves, input.conf

and the output files under the directory `rmt_output`: 

    pop_all, pop_inn, pop_out, expec_z_all, expec_v_all 


Molecular Tests 
---------------

The molecular test calculation directories contain the input files not only for
the RMT calculations, but also for the UKRmol+ calculations (which generate the
input for RMT in the directory) in directory `UKRmol+`. The relevant inputs for
the RMT calculation are contained in `inputs`,  where files analagous to the
atomic case reside: inputs (`input.conf, molecular_data`) and outputs
(`rmt_output`).  The molecular calculations all run on just 10 cores.

Running a Test Calculation
--------------------------

To run a test calculation, simply change into the chosen test directory and use a 
parallel process launcher to run your version of RMT. For example:

    cd rmt/tests/atomic_tests/small/argon
    mpiexec -n 24 <path_to_repository>/rmt/source/build/bin/rmt.x > log.out


Here `-n 24` means we are using 24 cores for our RMT calculation, and is the sum of the
`no_of_pes_to_use_inner` and `no_of_pes_to_use_outer` variables.

The directory `rmt_output` contains the expected RMT output for the test
calculation.  To check your version of RMT you may wish to compare your output
data with this standard output. Note that it should be expected that the output
files contained in the `rmt_output` directories should be reproduced identically
on all systems with all compilers. It is sufficient to check that the numbers do
not vary substantially. For reference, the data provided was determined using
the Intel 17 compiler on [ARCHER](www.archer.ac.uk). Furthermore, only some of
the output files produced by the RMT calculation are present in the `rmt_output`
directories, and thus the utility script `compare_rmt_runs` will show a warning
when executed against the test calculations.

Running an RMT Calculation
===========================


Prerequisites
------------

Before running an RMT calculation you should have:

1. A copy of the executable `rmt.x` (see \ref quick_start "README" for compilation instructions).

   Your working directory should contain:

2. The appropriate input files - `H, Splinewaves, Splinedata` and either `d, d00` or `D***`
    for an atomic calculation or `molecular_data` for a molecular calculation.

3. The file `input.conf` populated with the correct parameters (see \ref inputs "/source/modules/initial_conditions").

4. The empty directories `ground, state, data` to house some of the RMT output.

Execution
---------

Once you have all of the prerequisities listed above you will be ready to run an
RMT calculation.  To do this, simply call a parallel process launcher from the
directory containing the input files.  For example:


    mpiexec -n 24 <path_to_repository>/rmt/source/bin/rmt.x > log.out


Here `-n 24` specifies that we want to run over 24 cores - your choice of
`number_of_pes_inner` and `number_of_pes_outer` should add up to this number. 

You can of course symbolically link or copy the executable into your working directory.

Postprocessing
--------------

Upon successful completion of a calculation each processor will print `Reached
finalise` either to the screen or to a chosen output file and your working
directory will contain the following:


    CurrentPosition                             log.out (piped output file)
    d                                           pop_all.<version_number>
    d00                                         pop_inn.<version_number>
    data                                        pop_out.<version_number>
    EField.<version_number>                     rmt.x
    expec_v_all.<version_number> (optional)     Splinewaves
    expec_z_all.<version_number> (optional)     Splinedata
    ground                                      state
    H                                           timing_inner.<version_number>
    hstat.<version_number>                      timing_outer0.<version_number>
    input.conf                                  timing_outer1.<version_number>


The directories `ground` and ` state` will contain the `psi_inner` and
`psi_outer` wavefunction files. `data` will contain the `pop` channel population
files.  Instructions for manipulating the output of RMT calculations can be
found \ref extracting_obs "in /utilities/pylib/README".

The expectation (`expec_*`) and population (`pop_*`) files contain values associated
with start of each iteration. The electric field file (`EField.*`) contains
values of the electric field at the middle of each iteration.
