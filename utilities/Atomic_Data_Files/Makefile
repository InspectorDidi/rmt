#############################################################################
#
#                               ADAS8#3 Makefile
#                               ----------------
#
#	This makefile:
#			* Compiles necessary R-matrix codes
#			* Produces "proc" file for adas8#3 script
#			* Downloads codes from the web
#
#	Necessary modifications:
#			* Define where your binary files should go
#			* Specify if you want parallel of just serial
#			  binaries.
#			* Include (or write) an appropriate .def file.
#			* Define a wget username/password (in def file)
#
#	Usage:
#			* "make" will build all codes
#
#                       * "make directories"
#                       * "make update_serial"
#                       * "make update_utility"                   
#                       * "make update_parallel"
#                       * "make clean"
#
#	Updates:
#			V1.0 Allan Whiteford, 26/09/05
#			     First Release
#                     
#                       V1.1 MCW, SDL, CPB     1/6/10
#
#                       V1.2    Martin O'Mullane, Mike Witthoeft,
#                               Stuart Loch, Connor Ballance   (14/10/11)
#                             - Introduce REPOScode and REPOSparam
#                               which can be set to CB or NRB repositories
#                               for finer control over retrieving the code
#                               and parameter files.
#                             - radiation damping for the general case
#                 
#                       V1.7  -  accelerated parallel code 
#                                extra utility codes hfilter,dfilter
#                                CPB and SDL  Apr 2012
#
#############################################################################


# Codes to make by default, options are any combination of
# serial, parallel and utility. This can be overridden at the
# command line by, e.g., "make parallel"

CODETYPE = serial utility parallel

# Include here machine dependent definitions and choose which repository
# from which to draw the code and dimensions.

include currentsite


BIN = $(BASE)/bin


# Where you want the automatically generated procfiles to go.
# Note that the main adas8#3 script expects the procfile to be
# in ~/.adas8#3proc unless you specify otherwise with --proc=
# you can specify /dev/null if you don't want a procfile

SPROCFILE = $(BIN)/serial_procfile
PPROCFILE = $(BIN)/parallel_procfile

# Below are paths to the various source codes as well as where their dimensions are
# stored. Note that the dimension file can be called anything (almost) and will be
# copied to the appropriate filename (e.g. PARAM) at compile time.
# Also present are URLs for downloading the latest versions of the code.

# Autostructure

autos_src = $(BASE)/src/autos/asdeck.f
autos_web = $(REPOSas)/autos2/asdeck.f
autos_header = $(BASE)/include/autos.h
autos_header_web = $(REPOSas)/autos2/PARAM

# Inner region exchange codes

#s_stg1f_src = $(BASE)/src/serial/inner/stg1rfixed.f

s_stg1_src = $(BASE)/src/serial/inner/stg1r.f
s_stg1_header = $(BASE)/include/s_inner.h
s_stg1_web = $(REPOScode)/ser/xlsbp/stg1r.f
s_stg1_header_web = $(REPOSparam)/include/s_inner.h

s_BSplineStg1_src = $(BASE)/src/serial/inner/BSplineStg1.f90
s_BSplineStg1_web = $(REPOScode)/ser/xlsbp/BSplineStg1.f90

s_MakeSplineWavesParallel_src = $(BASE)/src/serial/inner/MakeSplineWavesParallel.f90
s_MakeSplineWavesParallel_web = $(REPOScode)/par/xlsbp_dip/MakeSplineWavesParallel.f90

s_SetMore_src = $(BASE)/src/serial/inner/SetMore.f90
s_SetMore_web = $(REPOScode)/par/xlsbp_dip/SetMore.f90

s_stg2_src = $(BASE)/src/serial/inner/stg2r.f
s_stg2_header = $(BASE)/include/s_inner.h
s_stg2_web = $(REPOScode)/ser/xlsbp/stg2r.f

s_stg3_src = $(BASE)/src/serial/inner/stg3r.f
s_stg3_header = $(BASE)/include/s_inner.h
s_stg3_web = $(REPOScode)/ser/xlsbp/stg3r.f

s_stgjk_src = $(BASE)/src/serial/inner/stgjk.f
s_stgjk_header = $(BASE)/include/s_inner.h
s_stgjk_web = $(REPOScode)/ser/xlsbp/stgjk.f

s_stglib_src = $(BASE)/src/serial/inner/stglib.f
s_stglib_header = $(BASE)/include/s_inner.h
s_stglib_web = $(REPOScode)/ser/xlsbp/stglib.f

# Inner region non-exchange codes

s_stg1nx_src = $(BASE)/src/serial/nx/stg1nx.f
s_stg1nx_header = $(BASE)/include/s_nonx.h
s_stg1nx_web = $(REPOScode)/ser/nxls/stg1nx.f
s_stg1nx_header_web = $(REPOSparam)/include/s_nonx.h

s_stg2nx_src = $(BASE)/src/serial/nx/stg2nx.f
s_stg2nx_header = $(BASE)/include/s_nonx.h
s_stg2nx_web = $(REPOScode)/ser/nxls/stg2nx.f

s_stg3nx_src = $(BASE)/src/serial/nx/stg3nx.f
s_stg3nx_header = $(BASE)/include/s_nonx.h
s_stg3nx_web = $(REPOScode)/ser/nxls/stg3nx.f

# Outer region serial codes

s_stgb_src = $(BASE)/src/serial/outer/stgb.f
s_stgb_header = $(BASE)/include/s_b.h
s_stgb_web = $(REPOScode)/ser/asy/stgb.f
s_stgb_header_web = $(REPOSparam)/include/s_f.h

s_stgf_src = $(BASE)/src/serial/outer/stgf.f
s_stgf_header = $(BASE)/include/s_f.h
s_stgf_web = $(REPOScode)/ser/asy/stgf.f
s_stgf_header_web = $(REPOSparam)/include/s_f.h    # yes it's the same as stgb

s_stgicf_src = $(BASE)/src/serial/outer/stgicf.f
s_stgicf_header = $(BASE)/include/s_icf.h
s_stgicf_web = $(REPOScode)/ser/asy/stgicf.f
s_stgicf_header_web = $(REPOSparam)/include/s_icf.h

s_stgfdamp_src = $(BASE)/src/serial/outer/stgfdamp.f
s_stgfdamp_header = $(BASE)/include/s_fdamp.h
s_stgfdamp_web = $(REPOScode)/ser/asy-damp/stgfdamp.f
s_stgfdamp_header_web = $(REPOSparam)/include/s_fdamp.h

s_stgicfdamp_src = $(BASE)/src/serial/outer/stgicfdamp.f
s_stgicfdamp_header = $(BASE)/include/s_icfdamp.h
s_stgicfdamp_web = $(REPOScode)/ser/asy-damp/stgicfdamp.f
s_stgicfdamp_header_web = $(REPOSparam)/include/s_icfdamp.h

# Miscellanous utility programs

adasexj_src = $(BASE)/src/misc/adasexj.f
adasex_src = $(BASE)/src/misc/adasex.f
omadd_src = $(BASE)/src/misc/omadd.f
om2omu_src = $(BASE)/src/misc/om2omu.f
omr2omc_src = $(BASE)/src/misc/omr2omc.f
arrange_src = $(BASE)/src/misc/arrange.f
width_src = $(BASE)/src/misc/width.f
dfilter_src = $(BASE)/src/misc/dfilter.f
hfilter_src = $(BASE)/src/misc/hfilter.f

adasexj_web = $(REPOScode)/misc/adasexj.f
adasex_web = $(REPOScode)/misc/adasex.f
omadd_web = $(REPOScode)/misc/omadd.f
om2omu_web = $(REPOScode)/misc/om2omu.f
arrange_web = $(REPOScode)/misc/arrange.f
width_web = $(REPOScode)/misc/width.f
dfilter_web = $(REPOScode)/misc/dfilter.f
hfilter_web = $(REPOScode)/misc/hfilter.f


# Parallel inner region codes

p_stg1_src = $(BASE)/src/parallel/inner/pstg1r.f
p_stg1_header = $(BASE)/include/p_inner.h
p_stg1_web = $(REPOScode)/par/xlsbp/pstg1r.f
p_stg1_header_web = $(REPOSparam)/include/p_inner.h

p_stg2_src = $(BASE)/src/parallel/inner/pstg2r.f
p_stg2_header = $(BASE)/include/p_inner.h
p_stg2_web = $(REPOScode)/par/xlsbp/pstg2r.f

p_stg3_src = $(BASE)/src/parallel/inner/pstg3r.f
p_stg3_header = $(BASE)/include/p_inner.h
p_stg3_web = $(REPOScode)/par/xlsbp/pstg3r.f

p_stg3_proto_src = $(BASE)/src/parallel/inner/pstg3r_proto.f
p_stg3_proto_header = $(BASE)/include/p_inner.h
p_stg3_proto_web = $(REPOSproto)/prototype/pstg3r_proto.f

pd_stg2_src = $(BASE)/src/parallel/inner/pstg2r_dip.f
pd_stg2_header = $(BASE)/include/p_inner.h
pd_stg2_web = $(REPOScode)/par/xlsbp_dip/pstg2r_dip.f

pd_stg3_src = $(BASE)/src/parallel/inner/pstg3r_dip.f
pd_stg3_header = $(BASE)/include/p_inner.h
pd_stg3_web = $(REPOScode)/par/xlsbp_dip/pstg3r_dip.f

pd_stgd_src = $(BASE)/src/parallel/inner/pstgd_dip.f
pd_stgd_header = $(BASE)/include/p_inner.h
pd_stgd_web = $(REPOScode)/par/xlsbp_dip/pstgd_dip.f

p_stglib_src = $(BASE)/src/parallel/inner/stglib.f
p_stglib_header = $(BASE)/include/p_inner.h
p_stglib_web = $(REPOScode)/par/xlsbp/stglib.f

# Parallel inner region NX codes

p_stg2nx_src = $(BASE)/src/parallel/nx/pstg2nx.f
p_stg2nx_header = $(BASE)/include/p_nx.h
p_stg2nx_web = $(REPOScode)/par/nxls/pstg2nx.f
p_stg2nx_header_web = $(REPOSparam)/include/p_nx.h

p_stg3nx_src = $(BASE)/src/parallel/nx/pstg3nx.f
p_stg3nx_header = $(BASE)/include/p_nx.h
p_stg3nx_web = $(REPOScode)/par/nxls/pstg3nx.f

# Parallel outer region codes

p_stgb_src = $(BASE)/src/parallel/outer/pstgb.f
p_stgb_header = $(BASE)/include/p_f.h
p_stgb_web = $(REPOScode)/par/asy-damp/pstgb.f
p_stgb_header_web = $(REPOSparam)/include/p_f.h

p_stgf_src = $(BASE)/src/parallel/outer/pstgf.f
p_stgf_header = $(BASE)/include/p_f.h
p_stgf_web = $(REPOScode)/par/asy/pstgf.f
p_stgf_header_web = $(REPOSparam)/include/p_f.h

p_stgicf_src = $(BASE)/src/parallel/outer/pstgicf.f
p_stgicf_header = $(BASE)/include/p_icf.h
p_stgicf_web = $(REPOScode)/par/asy/pstgicf.f
p_stgicf_header_web = $(REPOSparam)/include/p_icf.h

p_stgfdamp_src = $(BASE)/src/parallel/outer/pstgfdamp.f
p_stgfdamp_header = $(BASE)/include/p_fdamp.h
p_stgfdamp_web = $(REPOScode)/par/asy-damp/pstgfdamp.f
p_stgfdamp_header_web = $(REPOSparam)/include/p_fdamp.h

p_stgicfdamp_src = $(BASE)/src/parallel/outer/pstgicfdamp.f
p_stgicfdamp_header = $(BASE)/include/p_icfdamp.h
p_stgicfdamp_web = $(REPOScode)/par/asy-damp/pstgicfdamp.f
p_stgicfdamp_header_web = $(REPOSparam)/include/p_icfdamp.h

#======================================================================
#
# Changing settings below here is dangerous.
#
#======================================================================

# Header file names the codes themself, you'll only need to alter
# this if the include statements in the codes are changed, these
# are correct from the web versions as of 02/09/05

autos_param = PARAM
s_stg1_param = PARAM
s_stg2_param = PARAM
s_stg3_param = PARAM
s_stgjk_param = PARAM
s_stglib_param = PARAM
s_stg1nx_param = PARAM
s_stg2nx_param = PARAM
s_stg3nx_param = PARAM
s_stgf_param = PARAM
s_stgicf_param = PARAM
s_stgfdamp_param = PARAM
s_stgicfdamp_param = PARAM
s_stgb_param = PARAM
p_stg1_param = PARAM
p_stg2_param = PARAM
p_stg3_param = PARAM
p_stg3_proto_param = PARAM
pd_stg1_param = PARAM
pd_stg2_param = PARAM
pd_stg3_param = PARAM
pd_stgd_param = PARAM
p_stglib_param = PARAM
p_stg2nx_param = PARAM
p_stg3nx_param = PARAM
p_stgf_param = PARAM
p_stgicf_param = PARAM
p_stgb_param = PARAM
p_stgfdamp_param = PARAM
p_stgicfdamp_param = PARAM

# Binary paths, better to edit BIN rather than change these

autos = $(BIN)/autos.x
s_stg1 = $(BIN)/stg1r.x
s_BSplineStg1 = $(BIN)/BSplineStg1.x
s_MakeSplineWavesParallel = $(BIN)/MakeSplineWavesParallel.x
s_SetMore = $(BIN)/SetMore.x
s_stg2 = $(BIN)/stg2r.x
s_stg3 = $(BIN)/stg3r.x
s_stgjk = $(BIN)/stgjk.x
s_stglib = $(BIN)/stglib.o
s_stg1nx = $(BIN)/stg1nx.x
s_stg2nx = $(BIN)/stg2nx.x
s_stg3nx = $(BIN)/stg3nx.x
s_stgf = $(BIN)/stgf.x
s_stgicf = $(BIN)/stgicf.x
s_stgfdamp = $(BIN)/stgfdamp.x
s_stgicfdamp = $(BIN)/stgicfdamp.x
s_stgb = $(BIN)/stgb.x
adasexj = $(BIN)/adasexj.x
adasex = $(BIN)/adasex.x
omadd = $(BIN)/omadd.x
om2omu = $(BIN)/om2omu.x
omr2omc = $(BIN)/omr2omc.x
arrange = $(BIN)/arrange.x
width = $(BIN)/width.x
dfilter = $(BIN)/dfilter.x
hfilter = $(BIN)/hfilter.x
p_stg1 = $(BIN)/pstg1r.x
p_stg1 = $(BIN)/pstg1r.x
p_stg2 = $(BIN)/pstg2r.x
p_stg3 = $(BIN)/pstg3r.x
p_stg3_proto = $(BIN)/pstg3r_proto.x
pd_stg2 = $(BIN)/pstg2r_dip.x
pd_stg3 = $(BIN)/pstg3r_dip.x
pd_stgd  = $(BIN)/pstgd_dip.x
p_stglib = $(BIN)/pstglib.o
p_stg2nx = $(BIN)/pstg2nx.x
p_stg3nx = $(BIN)/pstg3nx.x
p_stgf =  $(BIN)/pstgf.x
p_stgicf =  $(BIN)/pstgicf.x
p_stgb = $(BIN)/pstgb.x
p_stgfdamp =  $(BIN)/pstgfdamp.x
p_stgicfdamp =  $(BIN)/pstgicfdamp.x

# Rest of makefile follows, you should have stopped reading after the last warning
# changing anything below this line is not recommended

all: $(CODETYPE)

directories:
	$(MKDIR_P) $(BIN)
	$(MKDIR_P) $(BASE)/adf00
	$(MKDIR_P) $(BASE)/include
	$(MKDIR_P) $(BASE)/sites
	$(MKDIR_P) $(BASE)/src/autos
	$(MKDIR_P) $(BASE)/src/misc
	$(MKDIR_P) $(BASE)/src/serial/inner
	$(MKDIR_P) $(BASE)/src/serial/outer
	$(MKDIR_P) $(BASE)/src/serial/nx
	$(MKDIR_P) $(BASE)/src/parallel/inner
	$(MKDIR_P) $(BASE)/src/parallel/outer
	$(MKDIR_P) $(BASE)/src/parallel/nx

parallel: $(p_stg1) $(p_stg2) $(pd_stg2) $(p_stg3) $(pd_stg3) $(p_stg3_proto) $(pd_stgd) $(p_stg2nx) $(p_stg3nx) $(p_stgf) $(p_stgicf) $(p_stgb) $(p_stgfdamp) $(p_stgicfdamp) $(PPROCFILE) tidy

serial: $(autos) $(s_stg1) $(s_BSplineStg1) $(s_MakeSplineWavesParallel) $(s_SetMore) $(s_stg1f) $(s_stg2) $(s_stg3) $(s_stg3RMT) $(s_stg1nx) $(s_stg2nx) $(s_stg3nx) $(s_stgf) $(s_stgicf) $(s_stgfdamp) $(s_stgicfdamp) $(s_stgb) $(SPROCFILE) $(s_stgjk)  tidy

utility: $(adasexj) $(adasex) $(omadd) $(autos) $(arrange) $(width) $(dfilter) $(hfilter) tidy

update_serial:
	$(WGETas) $(autos_web) -O $(autos_src)
	$(WGETcode) $(s_stg1_web) -O $(s_stg1_src)
	$(WGETcode) $(s_BSplineStg1_web) -O $(s_BSplineStg1_src)
	$(WGETcode) $(s_MakeSplineWavesParallel_web) -O $(s_MakeSplineWavesParallel_src)
	$(WGETcode) $(s_SetMore_web) -O $(s_SetMore_src)
	$(WGETcode) $(s_stg2_web) -O $(s_stg2_src)
	$(WGETcode) $(s_stg3_web) -O $(s_stg3_src)
	$(WGETcode) $(s_stgjk_web) -O $(s_stgjk_src)
	$(WGETcode) $(s_stglib_web) -O $(s_stglib_src)
	$(WGETcode) $(s_stgf_web) -O $(s_stgf_src)
	$(WGETcode) $(s_stgicf_web) -O $(s_stgicf_src)
	$(WGETcode) $(s_stgfdamp_web) -O $(s_stgfdamp_src)
	$(WGETcode) $(s_stgicfdamp_web) -O $(s_stgicfdamp_src)
	$(WGETcode) $(s_stg1nx_web) -O $(s_stg1nx_src)
	$(WGETcode) $(s_stg2nx_web) -O $(s_stg2nx_src)
	$(WGETcode) $(s_stg3nx_web) -O $(s_stg3nx_src)
	$(WGETcode) $(s_stgb_web) -O $(s_stgb_src)
	$(WGETparam) $(autos_header_web)        -O $(autos_header)
	$(WGETparam) $(s_stg1_header_web)       -O $(s_stg1_header)
	$(WGETparam) $(s_stg1nx_header_web)     -O $(s_stg1nx_header)
	$(WGETparam) $(s_stgb_header_web)       -O $(s_stgb_header)
	$(WGETparam) $(s_stgf_header_web)       -O $(s_stgf_header)
	$(WGETparam) $(s_stgicf_header_web)     -O $(s_stgicf_header) 
	$(WGETparam) $(s_stgfdamp_header_web)   -O $(s_stgfdamp_header)
	$(WGETparam) $(s_stgicfdamp_header_web) -O $(s_stgicfdamp_header)
update_utility:
	$(WGETcode) $(dfilter_web) -O $(dfilter_src)
	$(WGETcode) $(hfilter_web) -O $(hfilter_src)
	$(WGETcode) $(arrange_web) -O $(arrange_src)
	$(WGETcode) $(width_web) -O $(width_src)
	$(WGETcode) $(adasexj_web) -O $(adasexj_src)
	$(WGETcode) $(adasex_web) -O $(adasex_src)
	$(WGETcode) $(omadd_web) -O $(omadd_src)
	$(WGETadf00cb) $(REPOSadf00) -x $(BASE)/adf00
#	$(WGETcode) $(om2omu_web) -O $(om2omu_src)
update_parallel:
	$(WGETcode) $(p_stg1_web) -O $(p_stg1_src)
	$(WGETcode) $(p_stg2_web) -O $(p_stg2_src)
	$(WGETcode) $(p_stg3_web) -O $(p_stg3_src)
	$(WGETcode) $(pd_stg2_web) -O $(pd_stg2_src)
	$(WGETcode) $(pd_stg3_web) -O $(pd_stg3_src)
	$(WGETcode) $(p_stg3_proto_web) -O $(p_stg3_proto_src)
	$(WGETcode) $(pd_stgd_web) -O $(pd_stgd_src)
	$(WGETcode) $(p_stg2nx_web) -O $(p_stg2nx_src)
	$(WGETcode) $(p_stg3nx_web) -O $(p_stg3nx_src)
#       $(WGETcode) $(p_stgjk_web) -O $(p_stgjk_src)
	$(WGETcode) $(p_stglib_web) -O $(p_stglib_src)
	$(WGETcode) $(p_stgf_web) -O $(p_stgf_src)
	$(WGETcode) $(p_stgb_web) -O $(p_stgb_src)
	$(WGETcode) $(p_stgicf_web) -O $(p_stgicf_src)
	$(WGETcode) $(p_stgfdamp_web) -O $(p_stgfdamp_src)
	$(WGETcode) $(p_stgicfdamp_web) -O $(p_stgicfdamp_src)
	$(WGETparam) $(p_stg1_header_web)       -O $(p_stg1_header)
	$(WGETparam) $(p_stg2nx_header_web)     -O $(p_stg2nx_header)
	$(WGETparam) $(p_stgf_header_web)       -O $(p_stgf_header)
	$(WGETparam) $(p_stgicf_header_web)     -O $(p_stgicf_header)
	$(WGETparam) $(p_stgfdamp_header_web)   -O $(p_stgfdamp_header)
	$(WGETparam) $(p_stgicfdamp_header_web) -O $(p_stgicfdamp_header)
clean: 
	rm  $(BIN)/*.x
	rm  $(BIN)/*.o

$(SPROCFILE):
	@$(ECHO) "GENERAL" > $(SPROCFILE)
	@$(ECHO) "chunk_size = 20000" >> $(SPROCFILE)
	@$(ECHO) "chunk_size_high_ex = 1000" >> $(SPROCFILE)
	@$(ECHO) "chunk_size_nex = 1000" >> $(SPROCFILE)
	@$(ECHO) "internal_split = 10" >> $(SPROCFILE)
	@$(ECHO) "adf00_path = $(BASE)/adf00" >> $(SPROCFILE)
	@$(ECHO) "matrix_type = K" >> $(SPROCFILE)
	@$(ECHO) "" >> $(SPROCFILE)
	@$(ECHO) "EXECUTABLE INFORMATION" >> $(SPROCFILE)	
	@$(ECHO) "auto            1 $(autos)" >> $(SPROCFILE)
	@$(ECHO) "stg1_ex         1 $(s_stg1)" >> $(SPROCFILE)
	@$(ECHO) "BSplineStg1     1 $(s_BSplineStg1)" >> $(SPROCFILE)
	@$(ECHO) "MakeSplineWavesParallel         1 $(s_MakeSplineWavesParallel)" >> $(SPROCFILE)
	@$(ECHO) "SetMore         1 $(s_SetMore)" >> $(SPROCFILE)
	@$(ECHO) "stg2_ex         1 $(s_stg2)" >> $(SPROCFILE)
	@$(ECHO) "stg3_ex         1 $(s_stg3)" >> $(SPROCFILE)
	@$(ECHO) "stg1_dip        1 $(s_stg1)" >> $(SPROCFILE)
	@$(ECHO) "stg2_dip        1 $(s_stg2)" >> $(SPROCFILE)
	@$(ECHO) "stg3_dip        1 $(s_stg3)" >> $(SPROCFILE)
	@$(ECHO) "stg1_tcc        1 $(s_stg1)" >> $(SPROCFILE)
	@$(ECHO) "stg2_tcc        1 $(s_stg2)" >> $(SPROCFILE)
	@$(ECHO) "stgjk_tcc       1 $(s_stgjk)" >> $(SPROCFILE)
	@$(ECHO) "stgf_ex         1 $(s_stgf)" >> $(SPROCFILE)
	@$(ECHO) "stgicf_ex       1 $(s_stgicf)" >> $(SPROCFILE)
	@$(ECHO) "stgfdamp_ex     1 $(s_stgfdamp)" >> $(SPROCFILE)
	@$(ECHO) "stgicfdamp_ex   1 $(s_stgicfdamp)" >> $(SPROCFILE)
	@$(ECHO) "stgb            1 $(s_stgb)" >> $(SPROCFILE)
	@$(ECHO) "stg1_nx         1 $(s_stg1)" >> $(SPROCFILE)
	@$(ECHO) "stg2_nx         1 $(s_stg2)" >> $(SPROCFILE)
	@$(ECHO) "stg3_nx         1 $(s_stg3)" >> $(SPROCFILE)
	@$(ECHO) "stgf_nx         1 $(s_stgf)" >> $(SPROCFILE)
	@$(ECHO) "stgicf_nx       1 $(s_stgicf)" >> $(SPROCFILE)
	@$(ECHO) "omadd           1 $(omadd)" >> $(SPROCFILE)
	@$(ECHO) "om2omu          1 $(om2omu)" >> $(SPROCFILE)
	@$(ECHO) "omr2omc         1 $(omr2omc)" >> $(SPROCFILE)
	@$(ECHO) "adasexj         1 $(adasexj)" >> $(SPROCFILE)
	@$(ECHO) "adasex          1 $(adasex)" >> $(SPROCFILE)
	@$(ECHO) "arrange         1 $(arrange)" >> $(SPROCFILE)
	@$(ECHO) "dfilter         1 $(dfilter)" >> $(SPROCFILE)
	@$(ECHO) "hfilter         1 $(hfilter)" >> $(SPROCFILE)
	@$(ECHO) "width           1 $(width)  " >> $(SPROCFILE)


$(PPROCFILE):
	@$(ECHO) "GENERAL" > $(PPROCFILE)
	@$(ECHO) "chunk_size = 20000" >> $(PPROCFILE)
	@$(ECHO) "chunk_size_high_ex = 1000" >> $(PPROCFILE)
	@$(ECHO) "chunk_size_nex = 1000" >> $(PPROCFILE)
	@$(ECHO) "internal_split = 10" >> $(PPROCFILE)
	@$(ECHO) "adf00_path = $(BASE)/adf00" >> $(PPROCFILE)
	@$(ECHO) "matrix_type = K" >> $(PPROCFILE)
	@$(ECHO) "" >> $(PPROCFILE)
	@$(ECHO) "EXECUTABLE INFORMATION" >> $(PPROCFILE)
	@$(ECHO) "auto            1 $(autos)" >> $(PPROCFILE)
	@$(ECHO) "stg1_ex         $(NCPU) $(MPICMD) $(p_stg1)" >> $(PPROCFILE)
	@$(ECHO) "BSplineStg1     1 $(s_BSplineStg1)" >> $(PPROCFILE)
	@$(ECHO) "MakeSplineWavesParallel         1 $(s_MakeSplineWavesParallel)" >> $(PPROCFILE)
	@$(ECHO) "SetMore         1 $(s_SetMore)" >> $(PPROCFILE)
	@$(ECHO) "stg2_ex         $(NCPU) $(MPICMD) $(p_stg2)" >> $(PPROCFILE)
	@$(ECHO) "stg3_ex         $(NCPU) $(MPICMD) $(p_stg3)" >> $(PPROCFILE)
	@$(ECHO) "stg3_proto_ex   $(NCPU) $(MPICMD) $(p_stg3_proto)" >> $(PPROCFILE)
#	@$(ECHO) "stgd_ex         $(NCPU) $(MPICMD) $(p_stgd)" >> $(PPROCFILE)
	@$(ECHO) "stg1_dip        $(NCPU) $(MPICMD) $(p_stg1)" >> $(PPROCFILE)
	@$(ECHO) "stg2_dip        $(NCPU) $(MPICMD) $(pd_stg2)" >> $(PPROCFILE)
	@$(ECHO) "rmt_stg2_dip    1 $(pd_stg2)" >> $(PPROCFILE)
	@$(ECHO) "stg3_dip        $(NCPU) $(MPICMD) $(pd_stg3)" >> $(PPROCFILE)
	@$(ECHO) "stgd_dip        $(NCPU) $(MPICMD) $(pd_stgd)" >> $(PPROCFILE)
	@$(ECHO) "stgd_dip_rmt    $(pd_stgd)" >> $(PPROCFILE)
	@$(ECHO) "stg1_tcc        1 $(s_stg1)" >> $(PPROCFILE)
	@$(ECHO) "stg2_tcc        1 $(s_stg2)" >> $(PPROCFILE)
	@$(ECHO) "stgjk_tcc       1 $(s_stgjk)" >> $(PPROCFILE)
	@$(ECHO) "stgf_ex         $(NCPU) $(MPICMD) $(p_stgf)" >> $(PPROCFILE)
	@$(ECHO) "stgicf_ex       $(NCPU) $(MPICMD) $(p_stgicf)" >> $(PPROCFILE)
	@$(ECHO) "stgfdamp_ex     $(NCPU) $(MPICMD) $(p_stgfdamp)" >> $(PPROCFILE)
	@$(ECHO) "stgicfdamp_ex   $(NCPU) $(MPICMD) $(p_stgicfdamp)" >> $(PPROCFILE)
	@$(ECHO) "stgb            1 $(s_stgb)" >> $(PPROCFILE)
	@$(ECHO) "stg1_nx         1 $(s_stg1)" >> $(PPROCFILE)
	@$(ECHO) "stg2_nx         $(NCPU) $(MPICMD) $(p_stg2)" >> $(PPROCFILE)
	@$(ECHO) "stg3_nx         $(NCPU) $(MPICMD) $(p_stg3)" >> $(PPROCFILE)
	@$(ECHO) "stgf_nx         $(NCPU) $(MPICMD) $(p_stgf)" >> $(PPROCFILE)
	@$(ECHO) "stgicf_nx       $(NCPU) $(MPICMD) $(p_stgicf)" >> $(PPROCFILE)
	@$(ECHO) "omadd           1 $(omadd)" >> $(PPROCFILE)
	@$(ECHO) "om2omu          1 $(om2omu)" >> $(PPROCFILE)
	@$(ECHO) "omr2omc         1 $(omr2omc)" >> $(PPROCFILE)
	@$(ECHO) "adasexj         1 $(adasexj)" >> $(PPROCFILE)
	@$(ECHO) "adasex          1 $(adasex)" >> $(PPROCFILE)
	@$(ECHO) "arrange         1 $(arrange)" >> $(PPROCFILE)
	@$(ECHO) "dfilter         1 $(dfilter)" >> $(PPROCFILE)
	@$(ECHO) "hfilter         1 $(hfilter)" >> $(PPROCFILE)
	@$(ECHO) "width           1 $(width)" >> $(PPROCFILE)



$(s_stg1): $(s_stg1_src) $(s_stg1_header)
	@$(ECHO) 'Compiling Inner region stage 1...'
	@$(CP) $(s_stg1_header) $(s_stg1_param)
	@$(SFF) $(SFFLAGS) $(s_stg1_src) -o $(s_stg1) $(SLFLAGS)
	@$(RM) $(s_stg1_param)

$(s_BSplineStg1): $(s_BSplineStg1_src)
	@$(ECHO) 'Compiling Inner region Bspline stage 1...'
	@$(SFF) $(SFFLAGS) $(s_BSplineStg1_src) -o $(s_BSplineStg1) $(SLFLAGS)

$(s_MakeSplineWavesParallel): $(s_MakeSplineWavesParallel_src)
	@$(ECHO) 'Compiling Inner region MakeSplineWavesParallel ...'
	@$(SFF) $(SFFLAGS) $(s_MakeSplineWavesParallel_src) -o $(s_MakeSplineWavesParallel) $(SLFLAGS)

$(s_SetMore): $(s_SetMore_src)
	@$(ECHO) 'Compiling Inner region SetMore utility ...'
	@$(SFF) $(SFFLAGS) $(s_SetMore_src) -o $(s_SetMore) $(SLFLAGS)
	
$(s_stglib): $(s_stglib_src) $(s_stglib_header)
	@$(ECHO) 'Compiling Inner region stage lib...'
	@$(CP) $(s_stglib_header) $(s_stglib_param)
	@$(SFF) $(SFFLAGS) -c $(s_stglib_src) -o $(s_stglib) $(SLFLAGS)
	@$(RM) $(s_stglib_param) *.o

$(s_stg2): $(s_stg2_src) $(s_stglib) $(s_stg2_header)
	@$(ECHO) 'Compiling Inner region stage 2...'
	@$(CP) $(s_stg2_header) $(s_stg2_param)
	@$(SFF) $(SFFLAGS) $(s_stg2_src) $(s_stglib) -o $(s_stg2) $(SLFLAGS)
	@$(RM) $(s_stg2_param) *.o

$(s_stg3): $(s_stg3_src) $(s_stglib) $(s_stg3_header)
	@$(ECHO) 'Compiling Inner region stage 3...'
	@$(CP) $(s_stg3_header) $(s_stg3_param)
	@$(SFF) $(SFFLAGS) $(s_stg3_src) $(s_stglib) -o $(s_stg3) $(SLFLAGS)
	@$(RM) $(s_stg3_param) *.o

$(s_stgjk): $(s_stgjk_src) $(s_stglib) $(s_stgjk_header)
	@$(ECHO) 'Compiling Inner region stage JK...'
	@$(CP) $(s_stgjk_header) $(s_stgjk_param)
	@$(SFF) $(SFFLAGS) $(s_stgjk_src) $(s_stglib) -o $(s_stgjk) $(SLFLAGS)
	@$(RM) $(s_stgjk_param) *.o

$(s_stg1nx): $(s_stg1nx_src) $(s_stg1nx_header)
	@$(ECHO) 'Compiling Inner region NX stage 1...'
	@$(CP) $(s_stg1nx_header) $(s_stg1nx_param)
	@$(SFF) $(SFFLAGS) $(s_stg1nx_src) -o $(s_stg1nx) $(SLFLAGS)
	@$(RM) $(s_stg1nx_param)

$(s_stg2nx): $(s_stg2nx_src) $(s_stg2nx_header)
	@$(ECHO) 'Compiling Inner region NX stage 2...'
	@$(CP) $(s_stg2nx_header) $(s_stg2nx_param)
	@$(SFF) $(SFFLAGS) $(s_stg2nx_src) -o $(s_stg2nx) $(SLFLAGS)
	@$(RM) $(s_stg2nx_param)

$(s_stg3nx): $(s_stg3nx_src) $(s_stg3nx_header)
	@$(ECHO) 'Compiling Inner region NX stage 3...'
	@$(CP) $(s_stg3nx_header) $(s_stg3nx_param)
	@$(SFF) $(SFFLAGS) $(s_stg3nx_src) -o $(s_stg3nx) $(SLFLAGS)
	@$(RM) $(s_stg3nx_param)

$(s_stgf): $(s_stgf_src) $(s_stgf_header)
	@$(ECHO) 'Compiling outer region stage F...'
	@$(CP) $(s_stgf_header) $(s_stgf_param)
	@$(SFF) $(SFFLAGS) $(s_stgf_src) -o $(s_stgf) $(SLFLAGS)
	@$(RM) $(s_stgf_param)

$(s_stgicf): $(s_stgicf_src) $(s_stgicf_header)
	@$(ECHO) 'Compiling outer region stage ICF...'
	@$(CP) $(s_stgicf_header) $(s_stgicf_param)
	@$(SFF) $(SFFLAGS) $(s_stgicf_src) -o $(s_stgicf) $(SLFLAGS)
	@$(RM) $(s_stgicf_param)

$(s_stgfdamp): $(s_stgfdamp_src) $(s_stgfdamp_header)
	@$(ECHO) 'Compiling outer region stage Fdamp...'
	@$(CP) $(s_stgfdamp_header) $(s_stgfdamp_param)
	@$(SFF) $(SFFLAGS) $(s_stgfdamp_src) -o $(s_stgfdamp) $(SLFLAGS)
	@$(RM) $(s_stgfdamp_param)

$(s_stgicfdamp): $(s_stgicfdamp_src) $(s_stgicfdamp_header)
	@$(ECHO) 'Compiling outer region stage ICFdamp...'
	@$(CP) $(s_stgicfdamp_header) $(s_stgicfdamp_param)
	@$(SFF) $(SFFLAGS) $(s_stgicfdamp_src) -o $(s_stgicfdamp) $(SLFLAGS)
	@$(RM) $(s_stgicfdamp_param)

$(s_stgb): $(s_stgb_src) $(s_stgb_header)
	@$(ECHO) 'Compiling Inner region stage b...'
	@$(CP) $(s_stgb_header) $(s_stgb_param)
	@$(SFF) $(SFFLAGS) $(s_stgb_src) -o $(s_stgb) $(SLFLAGS)
	@$(RM) $(s_stgb_param)

$(adasexj): $(adasexj_src)
	@$(ECHO) 'Compiling utility code adasexj...'
	@$(SFF) $(SFFLAGS) $(adasexj_src) -o $(adasexj)  $(SLFLAGS)

$(adasex): $(adasex_src)
	@$(ECHO) 'Compiling utility code adasex...'
	@$(SFF) $(SFFLAGS) $(adasex_src) -o $(adasex)  $(SLFLAGS)

$(omadd): $(omadd_src)
	@$(ECHO) 'Compiling utility code omadd...'
	@$(SFF) $(SFFLAGS) $(omadd_src) -o $(omadd)  $(SLFLAGS)

$(om2omu): $(om2omu_src)
	@$(ECHO) 'Compiling utility code om2omu...'
	@$(SFF) $(SFFLAGS) $(om2omu_src) -o $(om2omu)  $(SLFLAGS)

$(omr2omc): $(omr2omc_src)
	@$(ECHO) 'Compiling utility code omr2omc...'
	@$(SFF) $(SFFLAGS) $(omr2omc_src) -o $(omr2omc)  $(SLFLAGS)

$(arrange): $(arrange_src)
	@$(ECHO) 'Compiling utility code arrange...'
	@$(SFF) $(SFFLAGS) $(arrange_src) -o $(arrange)  $(SLFLAGS)

$(width): $(width_src)
	@$(ECHO) 'Compiling utility code width...'
	@$(SFF) $(SFFLAGS) $(width_src) -o $(width)  $(SLFLAGS)

$(dfilter): $(dfilter_src)
	@$(ECHO) 'Compiling utility code dfilter...'
	@$(SFF) $(SFFLAGS) $(dfilter_src) -o $(dfilter)  $(SLFLAGS)

$(hfilter): $(hfilter_src)
	@$(ECHO) 'Compiling utility code hfilter...'
	@$(SFF) $(SFFLAGS) $(hfilter_src) -o $(hfilter)  $(SLFLAGS)

$(autos): $(autos_src) $(autos_header)
	@$(ECHO) 'Compiling autostructure...'
	@$(CP) $(autos_header) $(autos_param)
	@$(SFF) $(SFFLAGS) $(autos_src) -o $(autos) $(SLFLAGS)
	@$(RM) $(autos_param)

$(p_stg1): $(p_stg1_src) $(p_stg1_header)
	@$(ECHO) 'Compiling Inner region stage 1...'
	@$(CP) $(p_stg1_header) $(p_stg1_param)
	@$(PFF) $(PFFLAGS) $(p_stg1_src) -o $(p_stg1) $(PLFLAGS)
	@$(RM) $(p_stg1_param) *.mod

$(p_stglib): $(p_stglib_src) $(p_stglib_header)
	@$(ECHO) 'Compiling Inner region stage lib...'
	@$(CP) $(p_stglib_header) $(p_stglib_param)
	@$(PFF) $(PFFLAGS) -c $(p_stglib_src) -o $(p_stglib) $(PLFLAGS)
	@$(RM) $(p_stglib_param) *.o *.mod
	
$(p_stg2): $(p_stg2_src) $(p_stglib) $(p_stg2_header)
	@$(ECHO) 'Compiling Inner region stage 2...'
	@$(CP) $(p_stg2_header) $(p_stg2_param)
	@$(PFF) $(PFFLAGS) $(p_stg2_src) $(p_stglib) -o $(p_stg2) $(PLFLAGS)
	@$(RM) $(p_stg2_param) *.o *.mod

$(p_stg3): $(p_stg3_src) $(p_stglib) $(p_stg3_header)
	@$(ECHO) 'Compiling Inner region stage 3...'
	@$(CP) $(p_stg3_header) $(p_stg3_param)
	@$(PFF) $(PFFLAGS) $(p_stg3_src) -o $(p_stg3) $(PBFLAGS) $(PLFLAGS) 
	@$(RM) $(p_stg3_param) *.o *.mod

$(p_stg3_proto): $(p_stg3_proto_src) $(p_stglib) $(p_stg3_proto_header)
	@$(ECHO) 'Compiling Inner region stage 3 Prototype...'
	@$(CP) $(p_stg3_proto_header) $(p_stg3_proto_param)
	@$(PFF) $(PFFLAGS) $(p_stg3_proto_src) -o $(p_stg3_proto) $(PBFLAGS) $(PLFLAGS) 
	@$(RM) $(p_stg3_proto_param) *.o *.mod

$(pd_stg2): $(pd_stg2_src) $(p_stglib) $(pd_stg2_header)
	@$(ECHO) 'Compiling Inner region stage 2d..'
	@$(CP) $(pd_stg2_header) $(pd_stg2_param)
	@$(PFF) $(PFFLAGS) $(pd_stg2_src) $(p_stglib) -o $(pd_stg2) $(PLFLAGS)
	@$(RM) $(pd_stg2_param) *.o *.mod

$(pd_stg3): $(pd_stg3_src) $(p_stglib) $(pd_stg3_header)
	@$(ECHO) 'Compiling Inner region stage 3d..'
	@$(CP) $(pd_stg3_header) $(pd_stg3_param)
	@$(PFF) $(PFFLAGS) $(pd_stg3_src) -o $(pd_stg3) $(PBFLAGS) $(PLFLAGS)
	@$(RM) $(pd_stg3_param) *.o *.mod

$(pd_stgd): $(pd_stgd_src) $(p_stglib) $(pd_stgd_header)
	@$(ECHO) 'Compiling Inner region stage d..'
	@$(CP) $(pd_stgd_header) $(pd_stgd_param)
	@$(PFF) $(PFFLAGS) $(pd_stgd_src) -o $(pd_stgd) $(PBFLAGS) $(PLFLAGS)
	@$(RM) $(pd_stgd_param) *.o *.mod

$(p_stg2nx): $(p_stg2nx_src) $(p_stg2nx_header)
	@$(ECHO) 'Compiling Inner region stage 2 NX...'
	@$(CP) $(p_stg2nx_header) $(p_stg2nx_param)
	@$(PFF) $(PFFLAGS) $(p_stg2nx_src) -o $(p_stg2nx) $(PLFLAGS)
	@$(RM) $(p_stg2nx_param) *.o *.mod

$(p_stg3nx): $(p_stg3nx_src) $(p_stg3nx_header)
	@$(ECHO) 'Compiling Inner region stage 3 NX...'
	@$(CP) $(p_stg3nx_header) $(p_stg3nx_param)
	@$(PFF) $(PFFLAGS) $(p_stg3nx_src) -o $(p_stg3nx)  $(PBFLAGS) $(PLFLAGS)
	@$(RM) $(p_stg3nx_param) *.o *.mod

$(p_stgf): $(p_stgf_src) $(p_stgf_header)
	@$(ECHO) 'Compiling outer region stage F...'
	@$(CP) $(p_stgf_header) $(p_stgf_param)
	@$(PFF) $(PFFLAGS) $(p_stgf_src) -o $(p_stgf) $(PLFLAGS)
	@$(RM) $(p_stgf_param) *.mod

$(p_stgicf): $(p_stgicf_src) $(p_stgicf_header)
	@$(ECHO) 'Compiling outer region stage ICF...'
	@$(CP) $(p_stgicf_header) $(p_stgicf_param)
	@$(PFF) $(PFFLAGS) $(p_stgicf_src) -o $(p_stgicf) $(PLFLAGS)
	@$(RM) $(p_stgicf_param) *.mod

$(p_stgb): $(p_stgb_src) $(p_stgb_header)
	@$(ECHO) 'Compiling outer region stage b ...'
	@$(CP) $(p_stgb_header) $(p_stgb_param)
	@$(PFF) $(PFFLAGS) $(p_stgb_src) -o $(p_stgb) $(PLFLAGS)
	@$(RM) $(p_stgb_param) *.mod

$(p_stgfdamp): $(p_stgfdamp_src) $(p_stgfdamp_header)
	@$(ECHO) 'Compiling outer region stage Fdamp...'
	@$(CP) $(p_stgfdamp_header) $(p_stgfdamp_param)
	@$(PFF) $(PFFLAGS) $(p_stgfdamp_src) -o $(p_stgfdamp) $(PLFLAGS)
	@$(RM) $(p_stgfdamp_param) *.mod

$(p_stgicfdamp): $(p_stgicfdamp_src) $(p_stgicfdamp_header)
	@$(ECHO) 'Compiling outer region stage ICFdamp...'
	@$(CP) $(p_stgicfdamp_header) $(p_stgicfdamp_param)
	@$(PFF) $(PFFLAGS) $(p_stgicfdamp_src) -o $(p_stgicfdamp) $(PLFLAGS)
	@$(RM) $(p_stgicfdamp_param) *.mod

tidy:
	rm *.mod