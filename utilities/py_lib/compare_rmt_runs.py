## @namespace compare_rmt_runs
# @ingroup utilities
# @brief Utility for quickly comparing the output 
# from two RMT-runs
#
# @details  
#
# The utility should be invoked from the command line as 
# 
# >>> python3 ./compare_rmt_runs dir1 dir2
# 
# where dir1 and dir2 are two directories containing the output from different
# versions of RMT (or runs with different input files that should give
# equivalent results, for instance).
# 
# If the output is identical (agreement bewteen floating point numbers is measured
# at the 10^(-9) level) then the utility simply prints 'True' to screen.
# Otherwise, the names of the differing files will be output.
#
# In the case that some files are present in one directory and not in the other,
# a warning will be printed and the files which are present in both will be
# compared.

from __future__ import print_function
import filehand as fh
import glob
import os

## Check if output files are of same length
def same_length(x,y):
    if len(x) != len(y):
        return False
    else:
        return True

## Check if output files agree to within tol
def within_tol(xdat,ydat,tol=1e-9):
    for x,y in zip(xdat,ydat):
        if abs(x-y) > tol :
            return False

    return True

## Check if output files agree
def data_same(x1,x2,y1,y2):
    return  x1==x2 and same_length(y1,y2) and within_tol(y1,y2) 

## read in data from output files and check for agreement
def compare_files(file1,file2,sol_id=1):

    x1,y1=fh.get_raw_data(file1, sol_id)
    x2,y2=fh.get_raw_data(file2, sol_id)

    return data_same(x1,x2,y1,y2)

## find the rmt output files in the named directories
def get_output_filelist(dir_name):
    # Added sorting alphabetically for each sublist, to ensure
    # same results across operating systems - not essential at
    # the moment, but will prevent surprises upon future changes.
    flist=sorted(glob.glob(dir_name+'pop*'))
    flist=flist+sorted(glob.glob(dir_name+'expec_*'))
    flist=flist+sorted(glob.glob(dir_name+'data/pop*'))

    return flist

## check that the output files produced by each run have the same names.
def same_output_list(flist1,flist2,dir1,dir2):
    
    dummylist = [ f.replace(dir1,dir2) for f in flist1 ]
    return (flist2 == dummylist)

def resolve_flists(flist1,flist2,dir1,dir2):
#    if (len(flist1) > len(flist2)):
    dummylist = [ f.replace(dir1,dir2) for f in flist1 ]
#    else:
#        dummylist = [ f.replace(dir2,dir1) for f in flist2 ]

    resolvedlist=[]
    for ff in dummylist:
        if os.path.isfile(ff):
            resolvedlist.append(ff)
    return resolvedlist

## read in data files from rmt run directories and see if they agree
def compare_two_dirs(dirlist, sol_id=1):
    flist1=get_output_filelist(dirlist[0]+"/")
    flist2=get_output_filelist(dirlist[1]+"/")

    if not same_output_list(flist1,flist2,dirlist[0],dirlist[1]):
        print("NOTE: Different output files produced")
        flist2 = resolve_flists(flist1,flist2,dirlist[0],dirlist[1])

    faillist=[]
    for x,y in zip(flist1,flist2):
        if not compare_files(x,y, sol_id):
           faillist.append(x+" and " +y+ " differ")

    if len(faillist) == 0:
        return True
    else:
        for item in faillist:
            print(item)
        return False

options = fh.get_options()
solutions_chosen_is_range = True if options['solutions_end'] is not None else False
    if solutions_chosen_is_range:
        solutions = range(options['solutions_start'],options['solutions_end']+1)
    else:
        solutions = range(options['solutions_start'],options['solutions_start']+1)
for sol_id in solutions:
    print(compare_two_dirs(fh.get_filelist(), sol_id))

