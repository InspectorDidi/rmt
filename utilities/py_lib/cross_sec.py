## @namespace cross_sec
# @ingroup utilities
# Code to calculate cross sections or ionisation rates from RMT output files.
# Specifically, the code operates on the popL##.* files contained in the data
# directory after an RMT run. Hence, you should run the code in the data
# directory so that the appropriate files can be read.
#
from __future__ import print_function
import glob
from math import pi


# CALCULATION PARAMETERS
# You must specify the channels you wish to address
# by setting the channel_list variable. As the cross section is calculated
# differently for different numbers of photons, you should select the channels
# which correspond to the one/two photon ionisation and then set nphotons
# accordingly. 
# Multiple Solutions version - you must specify which solution you want to analyse
#
#
########################################

#channel_list = ['05','06','07','08']
channel_list = ['05']
solution_id = 1
nphotons=1

########################################

## To account for the fact that the time dependent calculation uses a laser pulse
# profile, you set the pulse parameters in order to calculate the effective
# number of cycles. The number of cycles of ramp on is ramp_cyclces, peak_cycles
# the number at peak intensity.
# Intensity is set in units of 10^14 Wcm-2 (to mirror RMT)
# Omega is set in atomic units

########################################

ramp_cycles=5.0
peak_cycles=10.0
intensity=0.01
omega= 15.00/27.212

########################################


# GLOBAL VARIABLES
I1=intensity*10**14
I0=3.509*10**16
a0=0.5291772083e-10
t0=2.4188843265e-17
alpha=0.007297

#derived constants
t2= (I0/I1)**(nphotons)
t3 = a0 ** ( 2*nphotons )
t4 = t0 ** (nphotons -1 )
coeff=t2*t3*t4

# Routines

## for all named channel files, read the final population therein, and add it to
# the total.
def read_yields(dirname):
    rtot = 0.0
    lineno = -1 # reads the population from the last line of the file. 

    for channel in channel_list:
        popL_channels =  glob.glob(dirname+"/popL"+channel+"*")
        if len(popL_channels) != 1:
            raise ValueError('Should have found one popL{}* file, found {} files'.format(channel,len(popL_channels)))
        else:
            read_file = popL_channels[0]

        with open(read_file, 'r') as rpf:
            lines = rpf.readlines()
        rtot = rtot + float(lines[lineno].split()[solution_id - 1])
        print(rtot)

    return rtot

## given the number of photons, and the pulse profile, calculate the effective
# number of cycles
def calc_tot_eff_cyc():
    if nphotons == 1 :
        eff_rat=0.3750
    elif nphotons == 2 : 
        eff_rat = 0.27343750

    return peak_cycles+2.0*ramp_cycles*eff_rat

## given the total population in the channels, the number of effective cycles and
# the frequency, calculate the ionisation rate
def calc_rate(omega,eff_cyc):
    return (tot_yield * omega) / (eff_cyc * 2 * pi) 

## given the ionisation rate, number of photons and frequency, calculate the
# cross section 
def calc_cs(omega,rate):     
    t1 = (8.0 * pi * alpha * omega)**nphotons
    return rate*coeff*t1

## you can adjust read_yields to read from a list of directories, hence the input
# variable to this function
tot_yield = read_yields('.')

eff_cyc = calc_tot_eff_cyc()
rate = calc_rate(omega,eff_cyc)
barn = 1e22

print(omega, barn*calc_cs(omega, rate))



