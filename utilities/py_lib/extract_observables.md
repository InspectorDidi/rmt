\page extracting_obs Extracting Observables

Post-processing
===============

*Please note that in the following instructions, "python3" can be substituted with
the appropriate python alias / executable on your system (often simply "python"),
and while Python 3.x is preferred these utilities are cross-compatible with Python 2.x*
*Additionally to Python's standard library, the 'Numpy' library needs to be installed in order to use these tools. You can check it is installed by running the following in your command line;*

    python3 -c 'import numpy'
    
*which will produce an error if it is not installed.*

Photo-electron density and momentum
-----------------------------------

Information can be found in \ref reform_wavefunction "/source/programs/reform.f90"

Channel populations
-------------------

Upon completion of an RMT run, the `/data` directory contains the population of
each electron emission channel as a function of time.  If RMT is executed with
`binary_data_files =.true.` then the channel population data is written to a
single binary output file `popchn.*` in the `/data` directory. The python
utility [data_recon.py](\ref data_recon) can be used to reconstruct the channel
populations named for their channel ID exactly as if they had been written by
the RMT code itself. The naming convention is explained in \ref comp_output2
"/source/modules/io_routines.f90"

Ionization cross sections
-------------------------

Cross-sections and ionisation rates can be determined from the `popL##.*` files
in the `data` directory using the utility [cross_sec.py](\ref cross_sec). Before
running `cross_sec.py` you must edit the script to specify the channels you wish
to address (the cross-section is calculated differently for different numbers of
photons), set the number of photons accordingly and also set the correct pulse
parameters (ramp cycles, peak cycles, intensity and photon energy).
`cross_sec.py` can then be used as follows: 

    cd data 
    
    python3 ./cross_sec.py 
    
Upon completion, the script will print the cross-section, in barns, to the screen.

Photoemission: High-Harmonic Generation (HHG) spectra
--------------------------------------

Fundamentally, light is produced by accelerated charges, and in the cases
considered by RMT, the charge in question is the dipole. 

Following [1](https://doi.org/10.1103/PhysRevA.86.053420), we may show that the
electric field produced by an oscillating dipole is 

\f{equation}{ E(t) \propto \mathbf{\ddot{d}}(t) =
  \frac{d^2}{dt^2}\langle\Psi(t)|\mathbf{z}|\Psi(t)\rangle \f}

where \f$ \ddot{d}(t) \f$ is the time-dependent expectation value of the dipole
acceleration, \f$ \mathbf{z} \f$ is the position operator and \f$\Psi(t)\f$ is
the wavefunction.  Then, the power spectrum of the emitted radiation is given,
up to a proportionality constant,  by \f$ |\mathbf{\ddot{d}}(\omega)|^2 \f$- the
Fourier transform of \f$ \mathbf{\ddot{d}}(t) \f$ squared.  The dipole
acceleration \f$ \mathbf{\ddot{d}} \f$ cannot however be computed easily (except
in simple cases such as atomic helium
[1](https://doi.org/10.1103/PhysRevA.86.053420)) as this quantity is
prohibitively sensitive to the description of atomic structure at very small
radial distances. Instead, the relationships between acceleration, velocity and
displacement can be exploited to express the harmonic spectrum in terms of the
the dipole velocity and/or length.

Thus in RMT calculations the harmonic spectrum is calculated from the
time-dependent expectation value of the dipole operator \f$\mathbf{D}\f$:

\f{equation}{ \mathbf{d}(t) =  \langle \Psi(t) | \mathbf{D} | \Psi(t)\rangle \f}

and of the dipole velocity operator \f$\mathbf{\dot{D}}\f$
\f{equation}{  \dot{\mathbf{d}} (t) =  \langle \Psi(t) | \mathbf{\dot{D}} | \Psi(t)\rangle \f}

The harmonic spectrum is then given by

\f{equation}{ S(\omega) \quad = \quad \omega^4 | \mathbf{d}(\omega)|^2
 \quad= \quad \omega^2 | \mathbf{\dot{d}}(\omega)|^2 \f}

where \f$\omega\f$ is the photon energy and \f$\mathbf{d}(\omega)\f$ and
\f$\mathbf{\dot{d}}(\omega)\f$ are the Fourier transforms of \f$\mathbf{d}(t)\f$
and \f$ \mathbf{\dot{d}(t)} \f$ respectively. Consistency between the length and
velocity form spectra is used a test of the accuracy of the RMT calculations. 

A python script is provided with the code, [gen_hhg.py](\ref gen_hhg), to
compute various forms of the harmonic spectrum. It can be invoked as follows:

    python3 ./gen_hhg.py [-xyz] <filelist>

or, in the current directory,

    python3 ./gen_hhg.py [-xyz] .


The options `-xyz` specify which component(s) of the dipole are to be processed-
by default only the z-component is evaluated.

`<filelist>` is a list of directories containing the `expec_*`
(dipole-expectation) files. `gen_hhy.py` will produce the new files (`Harm_*`)
containing the harmonic spectra for each requested component for the dipole
length, and the dipole velocity if available. It will also generate the
corresponding smoothed spectra (`Smooth_*`).

If either the `expec_z_all.*` or `expec_v_all.*` RMT output files are missing,
then the corresponding spectra will not be computed. If both are missing, or if
any errors are encountered, the code will print an error message.
 
The utility [harm_phase.py](\ref harm_phase) can be called in a similar way and
will provide the harmonic phase for length and velocity forms.

Photoabsorption (Transient absorption spectra)
---------------------------------------------------

Following Refs. [2](https://doi.org/10.1103/PhysRevA.83.013419) and
[3](https://doi.org/10.1088/0953-4075/49/6/062003), it can be shown that the
transient absorption spectrum \f$ \sigma(\omega) \f$ is given by

\f{equation}{ \sigma (\omega) = 4\pi\alpha\omega \; \mathrm{Im}
\bigg[\frac{\mathbf{d}(\omega)}{\mathbf{\epsilon}(\omega)}\bigg] \f}

where \f$ \alpha \f$ is the fine-structure constant, \f$ \omega \f$ is the
photon energy, and \f$ \mathbf{d}(\omega) \f$ and \f$ \mathbf{\epsilon}(\omega)
\f$ are the Fourier transforms of the time-dependent expectation value of the
dipole operator \f$ \mathbf{d}(t) \f$ and electric field of the driving laser
\f$ \mathbf{\epsilon}(t) \f$ respectively.

The transient absorption spectrum can therefore be calculated from the
time-dependent dipole expectation, generated in RMT as described above, and the
electric field. 

A python script is provided with the code, [gen_tas.py](\ref gen_tas), to
compute the absorption spectrum. Running


    python3 ./gen_tas.py [-xyz] <filelist>

where -xyz specifies the components of the dipole required and
`<filelist>` is a list of directories containing the RMT output
(specifically the `expec_z_all.*` and `EField.*` files) or

    python3 ./gen_tas.py [-xyz] .

from the directory containing these files will result in a new file- `TA_spect.len`
which contains the absorption spectrum as a function of photon energy (in eV).

If either the `expec_z_all.*` or `EField.*` RMT output files are missing, or if
any errors are encountered, the code will print an error message.

