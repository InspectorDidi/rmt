# -*- coding: utf-8 -*-
##@namespace datahand
#@ingroup utilities
# Common routines for data handling.


##Pad the given input array with enough zeros to increase the length by a
#factor of at least <factor> (8 is chosen to fit previous use-cases, but if
#you need higher/lower resolution in the spectra, use a higher/lower
#<factor>) . Output is of length exactly a power of two so that
#subsequent FFT routines run quickly.
from __future__ import print_function
import numpy as np

def pad_with_zeros(input_array,factor=8):
  
    nn=factor*len(input_array)
    pot=1024
    for ii in range(25):
        pot*=2
        if pot > nn:
            break
    num_of_zeros=pot-len(input_array)
    if not (num_of_zeros%2) : # if num_of_zeros is not even
        return np.concatenate([np.zeros(1+ num_of_zeros//2),np.transpose(input_array),np.zeros(num_of_zeros//2)])
    else:
        return np.concatenate([np.zeros(num_of_zeros//2),np.transpose(input_array),np.zeros(num_of_zeros//2)])

## Performs a moving average for the purposes of smoothing the given data
def moving_average(input_data, window_size):
    
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(input_data, window, 'same')


## Convolve input data with a blackman window to ensure smooth
# decay to zero as t--> infinity
def apply_blackman(input_array):

    ll=np.size(input_array)
    return np.blackman(ll)*input_array

## Jack's version of FFT to check FFD routine. Works by simple dot-product integration
def JackFFT(ydat):

    ll=np.size(ydat)
    outdat=np.zeros(2*ll+1)*(0.0+0.0j)

    for i in range(-ll,ll):
        frequency=(0.5*float(i)/float(ll))
        print(i,frequency)
        outdat[i]=JackFFTInt(ydat,frequency)
    return outdat

## Jack's version of FFT to check FFD routine. Works by simple dot-product integration
def JackFFTInt(ydat,freq):

    ll=np.size(ydat)
    outdat=0.0
    frequency=freq*2.0*np.pi
    
    int_sum=0.0+0.0*1j
    for ii in range(0,ll):
        x=float(ii)#/float(ll)
        wave_point=np.sin(x*frequency)-1j*np.cos(x*frequency)
        int_sum=int_sum+(wave_point*ydat[ii])#/float(ll))
    outdat=int_sum
    return outdat


## Apply the fourier transform to the input data.
def FFT(ydat):

    ll=np.size(ydat)
    return (np.fft.fft(ydat))[0:(ll//2)-1]

## Calculate the correct frequency axis (eV) for the fourier transformed data.
def get_frequency_axis(dt,input_array):
    
    ll=np.size(input_array)
    freqs=np.arange((ll//2)-1)*(((2.0*np.pi)/ll)/dt)
    return freqs 

##  Perform nits (default 4) iterations of the smoothing with moving average
def smooth_data(wdata,nits=4):

    out_data = wdata
    for ii in np.arange(nits):
        out_data=moving_average(out_data,30)
    return out_data


## Given the electric field profile, analyse it to find where
#the zero field crossings are. Return array of indices of field zeros.
def get_zero_field(field):
     
    z = [cntr for cntr,(ii,jj) in enumerate(zip(field[1:],field[:-1])) if ii*jj < 0]
    return(z)


## Set up mask function to window out long-trajectory harmonics. This is
#done by constructing Gaussian windows which tail off to zero at the zero
#field crossings (obtained from get_zero_field). 
def define_mask(z,data):

    len_data = float(len(data))
    dd = [x-y for x,y in zip(z[1:],z[:-1])]
    x = np.zeros(len(data))
    win_len_min = int(0.5*sum(dd)/(len(dd))) # not used in function, same with below
    win_len = int(sum(dd)/(len(dd)))
    shift = 0.25*float(dd[1])/float(len(data)) #shift by a quater of a cycle so get gaussian centered over short trajectories
    width = 0.00003 # width of gaussian to select trajectories. Found by trial and error - other values might suit certain cases better.

    tmpx = np.linspace(0,1,len(data))
    
    for ii in (range(len(z)-1)):
        zii = float(z[ii])
        x += np.array([np.exp(-((i - shift - (zii/len_data))**2)/width) for i in tmpx])
    return x


## Apply a windowing function to restrict the time dependent dipole response to
#short-trajectories only. i.e. window out electron return times which occur
#after the field zeros
def mask_data(data,field):
     
    zeros = get_zero_field(field)
    mask = define_mask(zeros,data)

    return [m*x for m,x in zip(mask,data)]


## Apply a windowing function to restrict the time dependent dipole response to
#long-trajectories only. i.e. window all electron return times which don't occur
#after the field zeros
def mask_datalong(data,field):
    
    zeros = get_zero_field(field)
    mask = define_mask(zeros,data)
    mask = [1-i for i in mask]
    return [m*x for m,x in zip(mask,data)]

                      
def make_phase_data(data,gauge,dt,stride=100, pad_factor=8):

    dt = dt*stride
    samp_data = data[::stride]
    data = []

    freqs,data = get_ftrans(dt,samp_data, pad_factor=pad_factor)
    freqs = 27.212 * freqs # rescales to eV

    # Either trans_data is just the dipole length (data)...
    if gauge=="z":
        trans_data = [x for x in data]
    #... or we build it from the dipole velocity
    elif gauge=="v":
        trans_data=[-1j*x for x in data]
    else:
        raise ValueError('gauge not recognised, should be "v" or "z"')

    # Ignore first point as it gives divide by zero
    phase=[np.angle(x) for x in trans_data[1:]]

    return freqs,phase

def get_ftrans(dt, data, pad_factor=8):
    """
    Given the raw input from RMT, calculate the harmonic spectrum and the
    smoothed harmonic spectrum in the given dipole form (gauge)
    """
    try:
        data = apply_blackman(data)
    except Exception as e:
        print('An error occured applying the blackman window: \n',e)
        raise e
    try:
        data = pad_with_zeros(data,factor=pad_factor)
    except Exception as e:
        print('An error occured padding with zeros: \n',e)
        raise e
    try:
        freqs = get_frequency_axis(dt,data)
    except Exception as e:
        print('An error occured getting the frequency axis: \n',e)
        raise e

    data = FFT(data)

    return freqs,data
    
def rescale_data(gauge,x,y):
    """                                                                                                            
    Scale the power spectrum by the appropriate power of the frequency                
    """
    # Note that the factor of 0.67 here is simply for consistency with old
    # scripts for calculating the HHG spectrum. The units are arbitrary so it
    # doesn't matter, but it can be removed if desired.
    if ( gauge == "z" ):
        return 0.67*1e-5*y*(x**4)
    elif (gauge == "v"):
        return 0.67*1e-5*y*(x*x)         
    else:
        raise ValueError('gauge not recognised, should be "v" or "z"')

def get_hhg_spect(dt,data,gauge, pad_factor=8):
    """                                                                                                                   
    Given the raw input from RMT, calculate the harmonic spectrum and the                   
    smoothed harmonic spectrum in the given dipole form (gauge)                                     
    """
    try:
        data = apply_blackman(data)
    except Exception as e:
        print('An error occured applying the blackman window: \n',e)
        raise e
    try:
        data = pad_with_zeros(data,factor=pad_factor)
    except Exception as e:
        print('An error occured padding with zeros: \n',e)
        raise e
    try:
        freqs = get_frequency_axis(dt,data)
    except Exception as e:
        print('An error occured getting the frequency axis: \n',e)
        raise e

    data = FFT(data)
    harm_data = abs(data)*abs(data)
    harm_data = rescale_data(gauge,freqs,harm_data)
    freqs = 27.212*freqs # rescales to eV
    smoothed = smooth_data(harm_data)
    
    return freqs,harm_data,smoothed
                                    

def filterbyomega(freqs,data,omega):

    df = freqs[1]-freqs[0]
    dataout = np.zeros(len(data))
    for ii in range(1,len(data)):
        ratio = (float(ii)*df)/(float(omega)*2)
        diff = -0.5 + ratio%1
        if np.abs(diff) < 0.1:
            dataout[ii] = data[ii]

    return dataout


