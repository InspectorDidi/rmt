#!/usr/bin/python
## @namespace gen_tas
# @ingroup utilities
#Utility for generating absorption spectra from RMT output. The utility should be
#invoked from the command line as
#
#>>> python3 ./gen_tas.py <filelist> 
#
#where <filelist> is a list of directories containing the RMT output
#(specifically the expec_z_all.* and EField.* files. Can be invoked for the
#current directory using
#
#>>> python3 ./gen_tas.py .
#
#The result will be a new file in the RMT directory: TA_spect_<conf>_len
#which contains the absorption spectrum as a function of photon energy (in eV).
#If either the
#expec_z_all.* or EField.* RMT output files are missing, or if any
#errors are encountered, the code will print an error message
#
#@author ACB 30/04/2018

from __future__ import print_function
import numpy as np
from multiprocessing import Pool, cpu_count
import filehand as fh
import datahand as dh
import os


##finds the upper limit index for some given photon energy. The default
#defined here is 3 a.u (~81 eV) i.e. the absorption spectrum is computed for
#photon energies between 0 and 81 eV. This limit is chosen to be high enough
#for most realistic absorption calculations, but can be changed to suit.
def get_ulim(freq_vec):
    
    imax=0
    for ii in freq_vec:
        imax=imax+1
        if (ii > 3):
            break
    return(imax)

##Use the formula (4.w.pi/c)*imag(D/E) to calculate the absorption spectrum.
#Here w is the photon energy, c is the speed of light, D is the time
#dependent expectation value of the dipole and E the electric field strength
def get_atas(edat,fdat,freq, dim_name, sol_id):
    
    ooc=1./137.036
    try:
        with np.errstate(all='raise'):
            rat=edat/fdat
    except (ZeroDivisionError, RuntimeWarning, FloatingPointError) as ze:
        print("An zero-division occurred while trying to process the {} dimension of configuration {}.\n\
This is probably due to the component specified being all zero for this configuration,\n\
the calculation will continue but one or more NaN values will appear in the corresponding\n\
TA_spect* file".format(dim_name, sol_id))
    finally:
        with np.errstate(all='ignore'):
            rat=edat/fdat
    return(4*np.pi*ooc*freq*rat.imag)

##Pre-pad the given input array with enough zeros to increase the length by a
#factor of at least <factor> (8 is chosen to fit previous use-cases, but if
#you need higher/lower resolution in the spectra, use a higher/lower
#<factor>) . Output is of length exactly a power of two so that
#subsequent FFT routines run quickly.
def pad_with_zeros(input_array,factor=8): # LUKE - duplicated in datahand; check / generalise
    
    nn=factor*len(input_array)
    pot=1024
    for ii in range(25):
        pot=pot*2
        if pot > nn:
            break
    num_of_zeros=pot-len(input_array)
    return np.concatenate([np.zeros(num_of_zeros),np.transpose(input_array)])

##for the given file name (expec or field) get the raw data, pad it with
#zeros, apply the blackman window and fourier transform. Returns the fourier
#transformed signal and the associated frequency axis.
def process_data(name,sol_id,colnum):

    fname = fh.get_fname(name)
    dt,data = fh.get_raw_data(fname,sol_id,colnum)
    data = pad_with_zeros(data)

    freqs = dh.get_frequency_axis(dt,data)

    data = dh.apply_blackman(data)
    data = dh.FFT(data)
    return(data,freqs)

##attempts to peform the calculation of the absorption spectrum for a given
#directory. If this fails, will exit gracefully and print an error message
#to screen. 
def do_conversion(dir_name): # LUKE - possibly duplicated code, check

    try:
        os.chdir(dir_name)
        
        options = fh.get_options()
        solutions_chosen_is_range = True if options['solutions_end'] is not None else False
        if solutions_chosen_is_range:
            solutions = range(options['solutions_start'],options['solutions_end']+1)
        else:
            solutions = range(options['solutions_start'],options['solutions_start']+1)
        
        for sol_id in solutions:
            for desired,dim_name,dim_id in zip(options["componentlist"],['x','y','z'],[1,2,3]):
                if desired:
                    try:
                        expec_data,freqs=process_data('expec',sol_id,dim_id) 
                        # confirm expec dimension should match field dimension
                        field_data,freqs=process_data('field',sol_id,dim_id)
                    except Exception as e:
                        print("Unable to process data for the {} dimension of configuration {}".format(dim_name), sol_id)
                        raise e

                    # trim the data to some maximum frequency
                    ulim = get_ulim(freqs)
                    atas = get_atas(expec_data[:ulim],field_data[:ulim],freqs[:ulim], dim_name, sol_id)
                    freqs=27.212*(freqs[:ulim]) #convert a.u to eV
                    try:
                        fh.write_xy_output(freqs,atas,'TA_spect_len_{0:04d}_{1}'.format(sol_id,dim_name))
                    except Exception as e:
                        print("An error occurred trying to write the output for config {}, {} dimension".format(sol_id, dim_name))
                        raise e
    except:
        raise Exception("could not convert " + dir_name)
        

################################################################################

filelist = fh.get_filelist()

p = Pool(cpu_count())
p.map(do_conversion,filelist)
