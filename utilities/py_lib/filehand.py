from __future__ import print_function
from argparse import ArgumentParser as AP
import sys, os
import numpy as np
import glob
import datahand as dh
import numpy as np
import pandas as pd
import inspect

def read_command_line(return_options=False, return_filepaths=False):
    """
    read the list of options from the command line and set the component array
    representing x/y/z output True of False accordingly. Also set mask to True
    or False.
    """


    parser = AP(epilog="Notes: ellipticity.py only operates on data invoking the x-y plane.")
    # components: 
    parser.add_argument('files', help='filepaths to process (current directory if none supplied, seperate by spaces)', nargs='*')
    parser.add_argument('-x','--x', help='transform the x component of the dipole (unsuitable for ellipticity)', action='store_true')
    parser.add_argument('-y','--y', help='transform the y component of the dipole (unsuitable for ellipticity)', action='store_true')
    parser.add_argument('-z','--z', help='transform the z component of the dipole', action='store_true')
    # options:
    parser.add_argument('--mask', help='mask dipole to screen long trajectories', action='store_true')
    parser.add_argument('--masklong', help='mask dipole to screen short trajectories', action='store_true')
    parser.add_argument('--writemask', help='write masks used to file', action='store_true')
    parser.add_argument('--setfieldz', help='all masks are calculated from the z-polarised field', action='store_true')
    parser.add_argument('--continuousphase', help='in the phase calculations, attempt to produce a continuous phase output',
                        action='store_true')

    parser.add_argument('--filterharmonicpeaks', type=float,
                        help='only produce results in the region around the expected peaks of the harmonic spectrum (as expected from\
                              a fundamental frequency omega)')

    parser.add_argument('--pad_factor', help='number of zeros to pad data with during calculations involving Fourier transforms \
                                              (default 8: i.e. pad signal up to length 2^8)', type=int, default=8)

    parser.add_argument('-s','--solutions_start', help='first solution to operate on. (use with -e will operate on solutions from -s to -e, \
                                                        -s on its own will operate on just the specified solution, and not specifying \
                                                        either will operate on the first solution only)', type=int, default=None)

    parser.add_argument('-e', '--solutions_end', help='last solution to operate on. (use only with -s, to specify range of solutions)',
                        type=int, default=None)
    
    parsed_args = vars(parser.parse_args())

    if parsed_args['solutions_end'] is not None:
        assert (parsed_args['solutions_start'] is not None), 'must specify a solution with -s (indexed from 1) or a range with -s & -e' 
        assert (parsed_args['solutions_end'] > parsed_args['solutions_start']),'If -e is used, -s must be used also, and end > start'
    
    if parsed_args['solutions_start'] is None:
        parsed_args['solutions_start'] = 1
    
    option_names = ['mask', 'masklong', 'writemask', 'setfieldz', 'continuousphase',
                    'filterharmonicpeaks', 'pad_factor', 'solutions_start', 'solutions_end']

    options = {o:parsed_args[o] for o in option_names}

    if options["filterharmonicpeaks"] is not None:
        options['omega'] = options['filterharmonicpeaks']
        options['filterharmonicpeaks'] = True
    else:
        options['omega'] = 0.0
        options['filterharmonicpeaks'] = False

    if sum([parsed_args[c] for c in ['x','y','z']]) == 0: # if no component(s) chosen, assume z only
        options['componentlist'] = [False, False, True]
    else:
        options['componentlist'] = [parsed_args[c] for c in ['x','y','z']]

    # Check that correct components are chosen if using ellipticity.py:
    # Note that input.stack() output is affected by python version, and multiprocessing.
    # Python version differences are handled here, but future changes to multiprocessing
    # style would require the selected elements of input.stack() to be updated.
 
    check_needed = False
    if __name__ != '__main__':
        if sys.version_info[0] >= 3: # inspect.stack() output varies between Python2 and 3
            for frame in inspect.stack()[1:]:
                if frame.filename[-14:] == 'ellipticity.py':
                    check_needed = True
                    break
        else:
            if (inspect.stack()[2][1][-14:] == 'ellipticity.py'):
                check_needed = True
        
    if check_needed:        
        if (options['componentlist'] != [False,False,True]):
            raise ValueError('ellipticity.py only operates on data in the xy plane\n(check you haven\'t used -x or -y as arguments)')
                

    rootdir = os.getcwd()
    if len(parsed_args['files'])>0:
        prefixes_to_handle = ('./','.') # For the moment, strip current-directory references.
                                        # Ideally, re-structure to allow referring to directories
                                        # above the level of the cwd
        for p in prefixes_to_handle:
            parsed_args['files'] = [f[(f.startswith(p) and len(p)):] for f in parsed_args['files']]
        filepaths = ["{}/{}".format(rootdir, fname) for fname in parsed_args['files']]
    else:
        filepaths = ['{}'.format(rootdir)]
        

    if not (return_filepaths^return_options): # ^ is XOR - if both desired, or none specified, return both
        return options, filepaths
    elif return_options:
        return options
    elif return_filepaths:
        return filepaths

def get_raw_data(fname, cnfg_id, dim_id=1):

    """
    Imports raw, time-dependent RMT data values from fname. Returns
    the time step used and a numpy array containing the time-dependent
    data.
    """

    """ NON-MS version:
    raw_data=[]
    times=[]

    if sys.version_info >= (3, 0):
        with open(fname,"r") as opened_file: # using 'open' within the 'with' context manager ensures closing of file
            mapping = str.maketrans({"(":None, ")":None})
            for line in opened_file.readlines():
                times.append(float((line.translate(mapping).split(None,1)[0])))
                raw_data.append(float((line.translate(mapping).split(None,1)[1]).split(",")[0].split()[col-1]))

    else:
        with open(fname,"r") as opened_file:
            for line in opened_file.readlines():
                times.append(float((line.translate(None,"()").split(None,1)[0])))
                raw_data.append(float((line.translate(None,"()").split(None,1)[1]).split(",")[0].split()[col-1]))

    return times[1]-times[0], np.array(raw_data)
    """
    try:
        df = pd.read_csv(fname,delim_whitespace=True)
    except FileNotFoundError as e:
        print('unable to read from file {}'.format(fname))
        raise e

    dims = ['x','y','z']
    col_title = '{0:04d}_{1}'.format(cnfg_id, dims[dim_id-1])
    # using dim_id-1 as the utility scripts index it from 1
    delta_t = df.Time[1]-df.Time[0]
    try:
        col_data = df[col_title].values
    except AttributeError as e1:
        col_data = df[col_title].to_numpy(copy=True)
    except Exception as e2:
        raise Exception("Not able to extract values from the table, error thrown: ", e2)

    return delta_t, col_data




def get_fname(ftype,gauge='z'):
    """ 
    Determines the name of the data file based on the type of data requested.
    ftype can take the values of expec (for dipole expectation value files) or
    field (for EField files), and the optional argument, gauge, determines
    whether it's the dipole length or velocity file requested.
    """
    
    if (ftype=="expec"):
        if (gauge=="z"):
            matches = glob.glob("expec_z_all*")
            if len(matches)!=1:
                raise NameError("wrong number of files matching 'expec_z_all*' found: expected 1, got {}".format(len(matches)))
            else:
                fname = matches[0]
        elif (gauge=="v"):
            matches = glob.glob("expec_v_all*")
            if len(matches)!=1:
                raise NameError("wrong number of files matching 'expec_v_all*' found: expected 1, got {}".format(len(matches)))
            else:
                fname = matches[0]
    elif (ftype=="field"):
        matches = glob.glob("EField*")
        if len(matches)!=1:
            raise NameError("wrong number of files matching 'expec_v_all*' found: expected 1, got {}".format(len(matches)))
        else:
            fname = matches[0]
    else:
        fname = None

    return fname

def get_data_and_field(gauge,dim_id,mask_field_id,cnfg_id):
    fname = get_fname("expec",gauge)
    dt,data = get_raw_data(fname, cnfg_id, dim_id)
    field_file = get_fname("field")
    dt,field = get_raw_data(field_file, cnfg_id, mask_field_id)
    return dt,data,field

def write_xy_output(xdat,ydat,fname,filehead=""):
    """
    writes the x-y data in two columns to the file fname. Optionally prepend the
    datafile with a header for plotting (e.g. in xmgrace)
    """
    try:
        with open(fname,'w') as opf:
            opf.write(filehead)
    #        for ii in np.arange(np.size(ydat)):
            for x,y in zip(xdat,ydat):
                opf.write("{}   {}\n".format(x,y))
    except Exception as e:
        print('unable to write to file {}'.format(fname))
        raise e

def write_xyz_output(xdat,ydat,zdat,fname,filehead=""):
    """
    writes the x-y data in two columns to the file fname. Optionally prepend the
    datafile with a header for plotting (e.g. in xmgrace)
    """
    try:
        with open(fname,'w') as opf:
            opf.write(filehead)
            for ix,x in enumerate(xdat):
                for iy,y in enumerate(ydat):
                    opf.write("{}   {}   {}\n".format(x, y, zdat[ix,iy]))
                opf.write("\n")

    except Exception as e:
        print('unable to write to file {}'.format(fname))
        raise e

def write_mask_file(gauge,dim_id,field_id,cnfg_id,prefix):

    dt,data,field = get_data_and_field(gauge, dim_id, field_id, cnfg_id)
    zeros = dh.get_zero_field(field)
    shortmask = dh.define_mask(zeros,data)
    longmask = [1-i for i in shortmask]
    write_xy_output(np.linspace(0,len(data)*dt,len(data)),shortmask,prefix+'_{0:04d}_'.format(cnfg_id)+"short")
    write_xy_output(np.linspace(0,len(data)*dt,len(data)),longmask,prefix+'_{0:04d}_'.format(cnfg_id)+"long")

def get_options(): # placeholder to maintain functionality during refactoring process, deprecated by updated read_command_line()
    return read_command_line(return_options=True)

def get_filelist(): # placeholder to maintain functionality during refactoring process, deprecated by updated read_command_line()
    return read_command_line(return_filepaths=True)
